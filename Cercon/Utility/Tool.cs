﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Web.Configuration;
using System.Web;
using System.Data;
using Excel;
using System.IO;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Globalization;

namespace Utility
{
    public class Tool
    {
        public static string GetMD5(string pwd)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bHash = md5.ComputeHash(Encoding.UTF8.GetBytes(pwd));
            StringBuilder sbHash = new StringBuilder();

            foreach (byte b in bHash)
            {
                sbHash.Append(String.Format("{0:x2}", b));
            }
            return sbHash.ToString();
        }

        public static string DataToJson(object source)
        {
            try
            {
                string json = JsonConvert.SerializeObject(source, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "dd/MM/yyyy HH:mm:ss" });
                return json;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static string GetParamAppConfig(string key)
        {
            try
            {
                return WebConfigurationManager.AppSettings[key];
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public static Dictionary<string, DataTable> ReadExcel(HttpPostedFileBase importFile, int sheetIndex = 0, bool header = true)
        {
            Dictionary<string, DataTable> dicResult = new Dictionary<string, DataTable>();

            try
            {
                IExcelDataReader reader;

                if (importFile == null || importFile.ContentLength <= 0)
                {
                    dicResult.Add("File không hợp lệ", null);
                    return dicResult;
                }

                if (importFile.ContentLength > (10 * 1024 * 1024))
                {
                    dicResult.Add("File quá lớn (> 10MB)", null);
                    return dicResult;
                }

                if (importFile.FileName.EndsWith(".xlsx"))
                    reader = ExcelReaderFactory.CreateOpenXmlReader(importFile.InputStream);
                else if (importFile.FileName.EndsWith(".xls"))
                    reader = ExcelReaderFactory.CreateBinaryReader(importFile.InputStream);
                else
                {
                    dicResult.Add("Định dạng File không hợp lệ. (*.xls|*.xlsx)", null);
                    return dicResult;
                }

                reader.IsFirstRowAsColumnNames = header;
                DataSet ds = reader.AsDataSet();
                reader.Close();

                if (ds.Tables.Count <= 0)
                {
                    dicResult.Add("Không đọc được dữ liệu trong File", null);
                    return dicResult;
                }

                DataTable dt = ds.Tables[sheetIndex];

                dicResult.Add("Thành công", dt);
                return dicResult;
            }
            catch (Exception ex)
            {
                dicResult.Add("Có lỗi khi đọc file. Hãy kiểm tra lại file", null);
                return dicResult;
            }
        }

        public static Dictionary<string, string> GetDicFromFile(string path)
        {
            try
            {
                List<string> list = GetListByReadLine(path);
                Dictionary<string, string> dic = new Dictionary<string, string>();

                foreach (var i in list)
                {
                    string[] parms = i.Split(new string[] { "|||" }, StringSplitOptions.None);
                    dic.Add(parms[0], parms[1]);
                }

                return dic;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<string> GetListByReadLine(string filePath)
        {
            filePath = HttpContext.Current.Server.MapPath(filePath);
            List<string> list = new List<string>();

            try
            {
                using (TextReader reader = File.OpenText(filePath))
                {
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        line = line.Replace("\\n", "\n");
                        line = line.Replace("\\r", "\r");
                        list.Add(line);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return list;
        }

        public static bool CheckIpReceived(string ip)
        {
            string[] listIpAccess = WebConfigurationManager.AppSettings["IpCheck"].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            if (listIpAccess.Contains(ip)) return true;
            return false;
        }

        public static string CreateXmlResponse(string phone, string message, int serviceNo = -1, int contentType = 0)
        {
            XElement xml = new XElement("ClientResponse");

            List<string> listMessage = new List<string>();
            int countMessage = (int)Math.Ceiling((double)message.Length / 160);
            for (int i = 1; i <= countMessage; i++)
            {
                try
                {
                    listMessage.Add(message.Substring((i - 1) * 160, 160));
                }
                catch
                {
                    listMessage.Add(message.Substring((i - 1) * 160));
                }
            }

            foreach (var i in listMessage)
            {
                xml.Add(
                    new XElement("Message",
                        new XElement("PhoneNumber", phone),
                        new XElement("Message", i),
                        new XElement("SMSID", Guid.NewGuid().ToString()),
                        new XElement("ServiceNo", serviceNo),
                        new XElement("ContentType", contentType)
                    )
                );
            }

            return xml.ToString();
        }

        public static DateTime? ParseStringToDateTime(string dateTime)
        {
            try
            {
                string[] pattern = new string[] 
                { 
                    "d/M/yyyy",
                    "d/M/yyyy HH:mm:ss",
                    "d/M/yyyy hh:mm:ss tt",
                    "dd/MM/yyyy",
                    "dd/MM/yyyy HH:mm:ss",
                    "dd/MM/yyyy hh:mm:ss tt",
                    "M/d/yyyy",
                    "M/d/yyyy HH:mm:ss",
                    "M/d/yyyy hh:mm:ss tt",
                    "MM/dd/yyyy",
                    "MM/dd/yyyy HH:mm:ss",
                    "MM/dd/yyyy hh:mm:ss tt",
                    "yyyy/M/d",
                    "yyyy/M/d HH:mm:ss",
                    "yyyy/M/d hh:mm:ss tt",
                    "yyyy/MM/dd",
                    "yyyy/MM/dd HH:mm:ss",
                    "yyyy/MM/dd hh:mm:ss tt"
                };

                string[] arrParms = dateTime.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (arrParms.Length > 0)
                {
                    arrParms[0] = arrParms[0].Replace("-", "/");
                    string rawDateTime = arrParms[0];

                    if (arrParms.Length > 1)
                    {
                        arrParms[1] = arrParms[1].Replace("/", ":");
                        arrParms[1] = arrParms[1].Replace("-", ":");
                        rawDateTime += " " + arrParms[1];

                        if (arrParms.Length > 2)
                        {
                            rawDateTime += " " + arrParms[2];
                        }
                    }

                    DateTime resultDateTime = DateTime.ParseExact(rawDateTime, pattern, CultureInfo.CurrentCulture, DateTimeStyles.None);
                    return resultDateTime;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string ConvertDataTabletoString(DataTable dt)
        {
            try
            {
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return JsonConvert.SerializeObject(rows);
            }
            catch(Exception)
            {
                return String.Empty;
            }
        }

        public static bool CheckPhoneNumber(string phoneNumber)
        {
            try
            {
                phoneNumber = phoneNumber.Replace(" ", "");
                if (phoneNumber.StartsWith("84"))
                    phoneNumber = phoneNumber.Remove(0, 2).Insert(0, "0");
                else if (phoneNumber.StartsWith("+84"))
                    phoneNumber = phoneNumber.Remove(0, 3).Insert(0, "0");

                if (phoneNumber.Length < 10)
                    return false;

                double phoneTmp = -1;
                if (!double.TryParse(phoneNumber, out phoneTmp))
                    return false;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
