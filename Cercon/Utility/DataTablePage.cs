﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public class DataTablePage
    {
        public int draw { set; get; }
        public int recordsTotal { set; get; }
        public int recordsFiltered { set; get; }
        public object data { set; get; }

        public DataTablePage()
        {

        }
    }
}
