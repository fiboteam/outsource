﻿function CreateDataTableJ(ajaxValue, arrColumns, searchField, contentAction, arrButtons) {
    var table = $('#example').DataTable({
        "select": true,
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "scrollY": "450px",
        "scrollCollapse": true,
        "pagingType": "full_numbers",
        "language": {
            "decimal": "",
            "emptyTable": "Không tải được dữ liệu",
            "info": "Từ _START_ đến _END_ (tổng: _TOTAL_ dòng)",
            "infoEmpty": "từ 0 dến 0 (tổng: 0 dòng)",
            "infoFiltered": "(Tìm trong _MAX_ tổng dòng)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Hiện _MENU_ dòng",
            "loadingRecords": "Loading...",
            "processing": "Processing...",
            "search": "Tìm kiếm (" + searchField + "): ",
            "zeroRecords": "Không tìm thấy",
            "paginate": {
                "first": "<<",
                "last": ">>",
                "next": ">",
                "previous": "<"
            },
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        },
        "dom": 'B<"row">fr<"clear">ltip',
        "buttons": arrButtons,
        "order": [[0, "desc"]],
        "columns": arrColumns,
        "ajax": ajaxValue,
        "columnDefs": contentAction
    });

    return table;
}