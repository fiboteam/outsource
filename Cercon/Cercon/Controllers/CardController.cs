﻿using Utility;
using DAL.EF;
using DAL.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Data;
using Excel;
using System.Globalization;
using System.IO;
using Cercon.Code;

namespace Cercon.Controllers
{
    public class CardController : BaseAdminController
    {
        CardEF ef = new CardEF();

        public CardController()
        {
            NoAutenticatedActions.AddRange(new string[] { "Interact" });
        }

        //
        // GET: /Card/

        public ActionResult Index(int typeCard = 1)
        {
            if (typeCard == 2)
            {
                ViewBag.TitleCard = "Zolid";
                ViewBag.TypeCard = 2;
            }
            else
            {
                ViewBag.TitleCard = "Cercon";
                ViewBag.TypeCard = 1;
            }

            return View();
        }

        public ActionResult GetFilter(int length, int typeCard)
        {
            //string lenght = Request.Params["lenght"];
            string draw = Request.Params["draw"];
            string start = Request.Params["start"];
            string searchValue = Request.Params["search[value]"];
            string orderColumn = Request.Params["order[0][column]"];
            string orderDir = Request.Params["order[0][dir]"];

            string json = ef.GetFilterDataTable(start, length, orderColumn, orderDir, draw, searchValue, typeCard);

            return Content(json, "application/json");
        }

        public ActionResult Add(int TypeCard)
        {
            Card model = new Card()
            {
                TypeCard = (TypeCard)Enum.Parse(typeof(TypeCard), TypeCard.ToString()),
                Valid = DateTime.Now,
            };
            return View("AddUpdate", model);
        }

        public ActionResult Update(long Id, int TypeCard)
        {
            var find = ef.FindByFunc(p => p.Id == Id);

            if (find == null)
            {
                return Redirect("/Card/Index");
            }

            return View("AddUpdate", find);
        }

        public ActionResult AddUpdate(Card model)
        {
            model.CardNo = model.CardNo.ToUpper();
            model.CreatedByUserId = MemberID;
            model.UpdatedByUserId = MemberID;
            var find = ef.FindByFunc(p => p.CardNo.ToUpper().Equals(model.CardNo) && p.TypeCard == model.TypeCard);
            if (find != null && find.Id != model.Id)
            {
                return Content("{\"Result\":false, \"Message\": \"Thẻ bệnh nhân này đã có. Mã thẻ: " + find.CardNo + "\" }", "application/json");
            }

            if (!ef.AddOrUpdate(model))
            {
                return Content("{\"Result\":false, \"Message\": \"Cập nhật không thành công\" }", "application/json");
            }

            return Content("{\"Result\":true, \"Message\": \"Cập nhật thành công\" }", "application/json");
        }

        public ActionResult Delete(long Id)
        {
            if (!ef.Delete(Id))
            {
                return Content("{\"Result\":false, \"Message\": \"Không xóa được\" }", "application/json");
            }

            return Content("{\"Result\":true, \"Message\": \"Xóa thành công\" }", "application/json");
        }

        public ActionResult DeleteMore(string arrId)
        {
            List<long> lstId = new List<long>();
            List<string> arrTmp = new List<string>(arrId.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));
            arrTmp.ForEach(p => lstId.Add(long.Parse(p)));

            if (!ef.Delete(lstId))
            {
                return Content("{\"Result\":false, \"Message\": \"Không xóa được\" }", "application/json");
            }

            return Content("{\"Result\":true, \"Message\": \"Xóa thành công\" }", "application/json");
        }

        public ActionResult DeleteAll(int TypeCard)
        {
            var items = ef.GetByFunc<object>(p => (int)p.TypeCard == TypeCard);
            
            if (!ef.Delete(items))
            {
                return Content("{\"Result\":false, \"Message\": \"Không xóa được\" }", "application/json");
            }

            return Content("{\"Result\":true, \"Message\": \"Xóa thành công\" }", "application/json");
        }

        public ActionResult ShowImportFile(int TypeCard)
        {
            return View("ImportFile", TypeCard);
        }

        public ActionResult ImportFile(HttpPostedFileBase importFile
            , int TypeCard
            , string iCardNo = "A"
            , string iCardName = "B"
            , string iDoctor = "C"
            , string iValid = "D"
            , string iLab = "E"
            , string iTopLeft = "F"
            , string iTopRight = "G"
            , string iBottomLeft = "H"
            , string iBottomRight = "I"
            , int sheetIndex = 0
            , bool header = false)
        {
            var readExcel = Tool.ReadExcel(importFile, sheetIndex, header);

            if (readExcel.ElementAt(0).Value == null)
            {
                return Content("{\"Result\":false, \"Message\": \"" + readExcel.ElementAt(0).Key + "\" }", "application/json");
            }

            string arrColumnsExcel = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            List<Card> listNewItem = new List<Card>();

            int hasCard = 0;
            int hasCardInFile = 0; 
            int errorCard = 0;
            int successCard = 0;

            DataTable dtError = readExcel.ElementAt(0).Value.Clone();
            dtError.Columns[arrColumnsExcel.IndexOf(iCardNo)].ColumnName = "CardNo";
            dtError.Columns[arrColumnsExcel.IndexOf(iCardName)].ColumnName = "CardName";
            dtError.Columns[arrColumnsExcel.IndexOf(iDoctor)].ColumnName = "Doctor";
            dtError.Columns[arrColumnsExcel.IndexOf(iValid)].ColumnName = "Valid";
            dtError.Columns[arrColumnsExcel.IndexOf(iLab)].ColumnName = "Lab";
            dtError.Columns[arrColumnsExcel.IndexOf(iTopLeft)].ColumnName = "TopLeft";
            dtError.Columns[arrColumnsExcel.IndexOf(iTopRight)].ColumnName = "TopRight";
            dtError.Columns[arrColumnsExcel.IndexOf(iBottomLeft)].ColumnName = "BottomLeft";
            dtError.Columns[arrColumnsExcel.IndexOf(iBottomRight)].ColumnName = "BottomRight";

            dtError.Columns.Add("NoteError", typeof(string));
            dtError.Columns.Add("TypeError", typeof(string));
            DataRow rowError = null;

            foreach (DataRow r in readExcel.ElementAt(0).Value.Rows)
            {
                try
                {
                    rowError = dtError.NewRow();
                    rowError.ItemArray = r.ItemArray;
                    
                    Card item = new Card();
                    item.TypeCard = (TypeCard)Enum.Parse(typeof(TypeCard), TypeCard.ToString());
                    item.CardNo = r[arrColumnsExcel.IndexOf(iCardNo)].ToString().ToUpper();

                    var findCardInFile = listNewItem.FirstOrDefault(p => p.CardNo.Equals(item.CardNo));
                    if (findCardInFile != null)
                    {
                        hasCardInFile++;
                        rowError.ItemArray = r.ItemArray;
                        rowError["TypeError"] = "Trùng trong File";
                        dtError.Rows.Add(rowError);
                        continue;
                    }

                    var findCard = ef.FindByFunc(p => p.CardNo.ToUpper().Equals(item.CardNo) && p.TypeCard == item.TypeCard);
                    if (findCard != null)
                    {
                        hasCard++;
                        rowError.ItemArray = r.ItemArray;
                        rowError["TypeError"] = "Trùng trên Server";
                        dtError.Rows.Add(rowError);
                        continue;
                    }

                    try
                    {
                        item.Valid = (DateTime)r[arrColumnsExcel.IndexOf(iValid.ToUpper())];
                    }
                    catch
                    {
                        item.Valid = Tool.ParseStringToDateTime(r[arrColumnsExcel.IndexOf(iValid.ToUpper())].ToString()).Value;
                    }

                    item.CardName = r[arrColumnsExcel.IndexOf(iCardName.ToUpper())].ToString();
                    item.Doctor = r[arrColumnsExcel.IndexOf(iDoctor.ToUpper())].ToString();
                    item.Lab = r[arrColumnsExcel.IndexOf(iLab.ToUpper())].ToString();
                    item.TopLeft = r[arrColumnsExcel.IndexOf(iTopLeft.ToUpper())].ToString();
                    item.TopRight = r[arrColumnsExcel.IndexOf(iTopRight.ToUpper())].ToString();
                    item.BottomLeft = r[arrColumnsExcel.IndexOf(iBottomLeft.ToUpper())].ToString();
                    item.BottomRight = r[arrColumnsExcel.IndexOf(iBottomRight.ToUpper())].ToString();
                    item.UpdatedByUserId = MemberID;
                    item.CreatedByUserId = MemberID;

                    listNewItem.Add(item);
                    successCard++;
                }
                catch (Exception ex)
                {
                    errorCard++;
                    rowError.ItemArray = r.ItemArray;
                    rowError["TypeError"] = "Lỗi định dạng";
                    rowError["NoteError"] = ex.Message;
                    dtError.Rows.Add(rowError);
                }
            }

            if (dtError.Rows.Count > 0)
            {
                Session["ImportError"] = dtError;
            }
            else
            {
                Session["ImportError"] = null;
            }

            string messageResult = "[Import] Thành công: " 
                + successCard + "/" + readExcel.ElementAt(0).Value.Rows.Count 
                + " - Trùng trên Server: " + hasCard
                + " - Trùng trong File: " + hasCardInFile
                + " - Số dòng lỗi: " + errorCard;

            if (ef.BulkInsert(listNewItem))
            {
                if (errorCard > 0 || hasCard > 0 || hasCardInFile > 0)
                    return Content("{\"Result\":false, \"Message\": \"" + messageResult + "\" }", "application/json");
                return Content("{\"Result\":true, \"Message\": \"" + messageResult + "\" }", "application/json");
            }

            return Content("{\"Result\":false, \"Message\": \"Lỗi không thể Import được\" }", "application/json");
        }

        public ActionResult ShowImportError(int TypeCard)
        {
            if (TypeCard == 2)
            {
                ViewBag.TitleCard = "Zolid";
                ViewBag.TypeCard = 2;
            }
            else
            {
                ViewBag.TitleCard = "Cercon";
                ViewBag.TypeCard = 1;
            }

            return View((DataTable)Session["ImportError"]);
        }

        public ActionResult ShowExportData(int TypeCard)
        {
            return View("ExportData", TypeCard);
        }

        public ActionResult ExportData(int TypeCard, DateTime? StartDate, DateTime? EndDate)
        {
            try
            {
                IQueryable<Card> list;

                if (StartDate == null && EndDate == null)
                {
                    list = ef.GetByFunc<object>(p => (int)p.TypeCard == TypeCard);
                }
                else
                {
                    if (StartDate == null && EndDate != null)
                    {
                        EndDate = EndDate.Value.AddHours(23).AddMinutes(59);
                        list = ef.GetByFunc<object>(p => DateTime.Compare(p.CreatedDate, EndDate.Value) <= 0 && (int)p.TypeCard == TypeCard);
                    }
                    else if (StartDate != null && EndDate == null)
                    {
                        list = ef.GetByFunc<object>(p => DateTime.Compare(p.CreatedDate, StartDate.Value) >= 0 && (int)p.TypeCard == TypeCard);
                    }
                    else
                    {
                        EndDate = EndDate.Value.AddHours(23).AddMinutes(59);
                        list = ef.GetByFunc<object>(p => DateTime.Compare(p.CreatedDate, StartDate.Value) >= 0
                                                        && DateTime.Compare(p.CreatedDate, EndDate.Value) <= 0
                                                        && (int)p.TypeCard == TypeCard);
                    }
                }

                if (list != null && list.Count() > 0)
                {
                    var listResult = list.OrderByDescending(p => p.Id).ToList();

                    DataTable dtResult = CreateExcelFile.ListToDataTable(listResult);

                    dtResult.Columns.Remove("Id");
                    dtResult.Columns.Remove("UpdatedByUserId");
                    dtResult.Columns.Remove("CreatedByUserId");
                    dtResult.Columns.Remove("TypeCard");

                    DataSet dsResult = new DataSet();
                    dsResult.Tables.Add(dtResult);

                    string pathFile = Server.MapPath("~/App_Data") + "\\Export-" + Enum.Parse(typeof(TypeCard), TypeCard.ToString()).ToString() + ".xlsx";

                    using (MemoryStream msFile = new MemoryStream())
                    {
                        if (CreateExcelFile.CreateExcelDocument(dsResult, msFile))
                        {
                            Session["ExportFile"] = msFile;
                            return Content("/Card/GetExportFile?typecard=" + TypeCard, System.Net.Mime.MediaTypeNames.Text.Plain);

                        }
                    };

                    return Content("{\"Result\":false, \"Message\": \"Không tạo được file Excel\" }", "application/json");
                }

                return Content("{\"Result\":false, \"Message\": \"Dữ liệu rỗng. Hoặc chọn lại ngày xuất file\" }", "application/json");
            }
            catch (Exception ex)
            {
                return Content("{\"Result\":false, \"Message\": \"Lỗi không thể xuất file. Ex: "+ ex.Message + "\" }", "application/json");
            }
        }

        public ActionResult GetExportFile(int TypeCard)
        {
            MemoryStream mFile = (MemoryStream)Session["ExportFile"];
            Session["ExportFile"] = null;
            return File(mFile.ToArray()
                , System.Net.Mime.MediaTypeNames.Application.Octet
                , "Export-" + Enum.Parse(typeof(TypeCard), TypeCard.ToString()).ToString() + DateTime.Now.ToString("-yyyy-MM-dd-HH-mm-ss") + ".xlsx");
        }

        public ActionResult Interact(string message = "", string phone = "", string service = "", string port = "", string main = "", string sub = "", string guid = "")
        {
            try
            {
                int serviceNo = -1;
                int.TryParse(Tool.GetParamAppConfig("ServiceNo"), out serviceNo);

                //Kiểm tra IP
                string ip = Request.ServerVariables["REMOTE_ADDR"];
                if (!Tool.CheckIpReceived(ip))
                {
                    return Content(Tool.CreateXmlResponse("", "Lỗi IP kết nối không hợp lệ"), "text/xml");
                }

                if (String.IsNullOrEmpty(phone))
                {
                    return Content(Tool.CreateXmlResponse("", "Số điện thoại rỗng"), "text/xml");
                }

                if (!Tool.CheckPhoneNumber(phone))
                {
                    return Content(Tool.CreateXmlResponse("", "Số điện không hợp lệ. SĐT: " + phone), "text/xml");
                }

                //Chuyển số điện thoại thành số 0 đầu
                phone = phone.Replace(" ", "");
                if (phone.StartsWith("84"))
                    phone = phone.Remove(0, 2).Insert(0, "0");
                else if (phone.StartsWith("+84"))
                    phone = phone.Remove(0, 3).Insert(0, "0");

                Dictionary<string, string> dicMessageResult = Tool.GetDicFromFile("~/App_Data/MessageResult.txt");
                if (dicMessageResult == null || dicMessageResult.Count <= 0)
                {
                    return Content(Tool.CreateXmlResponse("", "Lỗi không tìm thấy tin nhắn trả về"), "text/xml");
                }

                //Kiểm tra nội dung
                string[] parms = message.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (
                    String.IsNullOrEmpty(message)
                    || parms.Length != 2
                    || (!parms[0].ToLower().Equals(Tool.GetParamAppConfig("KeywordCERCON").ToLower())
                        && !parms[0].ToLower().Equals(Tool.GetParamAppConfig("KeywordZOLID").ToLower()))
                    )
                {
                    return Content(Tool.CreateXmlResponse(phone, dicMessageResult["MessageErrorSyntax"], serviceNo), "text/xml");
                }

                parms[0] = parms[0].ToLower();
                parms[0] = parms[0][0].ToString().ToUpper() + parms[0].Substring(1);

                TypeCard typeCard = (TypeCard)Enum.Parse(typeof(TypeCard), parms[0]);
                string cardNoTmp = parms[1].ToLower();

                var findCard = ef.FindByFunc(p => p.CardNo.ToLower().Equals(cardNoTmp) && p.TypeCard == typeCard);
                if (findCard == null)
                {
                    return Content(Tool.CreateXmlResponse(phone, dicMessageResult["MessageErrorCardNo"], serviceNo), "text/xml");
                }

                string successResult = Tool.GetParamAppConfig("MessageSuccess");
                successResult = successResult.Replace("[keyword]", typeCard.ToString());
                successResult = successResult.Replace("[cardno]", findCard.CardNo);
                successResult = successResult.Replace("[cardname]", findCard.CardName);
                successResult = successResult.Replace("[doctor]", findCard.Doctor);
                successResult = successResult.Replace("[lab]", findCard.Lab);
                successResult = successResult.Replace("[valid]", findCard.Valid == null ? "" : findCard.Valid.ToString("dd/MM/yyyy"));

                //Thành công
                string resultXml = Tool.CreateXmlResponse(phone, successResult, serviceNo);

                return Content(resultXml, "text/xml");
            }
            catch (Exception ex)
            {
                return Content(Tool.CreateXmlResponse("", "Đã có lỗi xảy ra trên hệ thông. Message: " + ex.Message), "text/xml");
            }
        }
    }
}
