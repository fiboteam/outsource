﻿using Cercon.Models;
using Cercon.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cercon.Controllers
{
    public class BaseAdminController : Controller
    {
        public static string SessionName = System.Configuration.ConfigurationManager.AppSettings["SessionName"].ToString();

        public List<string> NoAutenticatedActions = new List<string>();

        /// <summary>
        /// Save Member hiện tại xuống session
        /// </summary>
        /// <param name="member"></param>
        public void SaveSession(UserModel member)
        {

            Session[SessionName] = member;
        }

        /// <summary>
        /// Xóa session để logout
        /// </summary>
        public void ClearSession()
        {
            if (Session[SessionName] != null)
                Session.Remove(SessionName);

        }

        public long MemberID
        {
            get
            {
                if (Session != null)
                {
                    if (Session[SessionName] != null)
                    {
                        return ((UserModel)Session[SessionName]).Id;
                    }
                }
                return 0;
            }
        }

        protected string LoginName
        {
            get
            {
                if (Session != null)
                {
                    if (Session[SessionName] != null)
                    {
                        return ((UserModel)Session[SessionName]).UserName;
                    }
                }
                return string.Empty;
            }
        }

        public UserModel MemberLogin
        {
            get
            {
                if (Session != null)
                {
                    if (Session[SessionName] != null)
                    {
                        return ((UserModel)Session[SessionName]);
                    }
                }
                return null;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session[SessionName] == null)
            {
                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Account")
                    return;
                if (NoAutenticatedActions.Exists(element => element.ToLower() == filterContext.ActionDescriptor.ActionName.ToLower()))
                    return;
                filterContext.Result = new RedirectResult(Url.Action("Login", "Account"));
            }
        }
    }
}