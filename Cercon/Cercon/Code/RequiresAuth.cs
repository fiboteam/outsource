﻿using Cercon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cercon.Code
{
    public class RequiresAuth : ActionFilterAttribute
    {
        protected bool isAdminModule = true;

        public bool IsAdminModule
        {
            get { return isAdminModule; }
            set { isAdminModule = value; }
        }

        public static string SessionName = System.Configuration.ConfigurationManager.AppSettings["SessionName"].ToString();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!IsValidIP(HttpContext.Current.Request.UserHostAddress))
            {
                filterContext.Result = new EmptyResult();
                filterContext.Result = new RedirectResult("/Home/AccessDenied");
            }

            if (!IsAdminModule)
            {
                return;
            }

            long memberId = HttpContext.Current.Session[SessionName] != null ? ((UserModel)HttpContext.Current.Session[SessionName]).Id : 0;
            if (memberId == 0 &&
                (filterContext.ActionDescriptor.ActionName != "Login" && filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Account"))
            {
                string currentUrl = string.Empty;
                if (filterContext.RequestContext.HttpContext.Request.Url != null)
                    currentUrl = filterContext.RequestContext.HttpContext.Request.Url.PathAndQuery;
                filterContext.Result = new EmptyResult();

                filterContext.Result = new RedirectResult("/Account/Login?returnUrl=" + HttpUtility.UrlEncode(currentUrl));
            }
        }
        /// <summary>
        /// Check IP đăng nhập xem có hợp lệ hay k - đề phòng sẵn
        /// </summary>
        /// <param name="IP"></param>
        /// <returns></returns>
        public bool IsValidIP(string IP)
        {
            string WhiteListIP = System.Configuration.ConfigurationManager.AppSettings["WhiteListIP"].ToString();
            if (string.IsNullOrEmpty(WhiteListIP))
                return true;
            else
            {
                return WhiteListIP.Split(';').Any(t => t == IP);
            }
        }
    }
}