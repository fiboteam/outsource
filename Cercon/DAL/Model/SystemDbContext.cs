﻿using DAL.Object;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Model
{
    public class SystemDbContext: DbContext
    {
        public DbSet<Card> Cards { get; set; }
        public DbSet<User> Users { set; get; }

        public SystemDbContext()
            : base("DefaultConnection")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }
}
