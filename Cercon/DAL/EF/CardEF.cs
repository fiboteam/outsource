﻿using Model;
using Utility;
using DAL.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace DAL.EF
{
    public class CardEF: EFBase<Card>
    {
        public string GetFilterDataTable(string start, int length, string columnOrder, string orderType, string draw, string searchValue, int typeCard)
        {
            List<string> orderName = new List<string>(Tool.GetParamAppConfig("ColumnsCard").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));

            IQueryable<Card> source = GetDbSet.Where(p => (int)p.TypeCard == typeCard);

            int totalRecord = source.Count();

            if (!String.IsNullOrEmpty(searchValue))
            {
                source = source.Where(p => 
                                            p.CardNo.ToLower().Contains(searchValue.ToLower()) 
                                         || p.CardName.ToLower().Contains(searchValue.ToLower()) 
                                         || p.Doctor.ToLower().Contains(searchValue.ToLower())
                                        );
            }

            DataTablePage dataTablePage = new DataTablePage()
            {
                recordsTotal = totalRecord,
                draw = int.Parse(draw) + 1,
                recordsFiltered = source.Count()
            };

            List<Card> tmp = source.ToList();

            int intColumnsOrder = int.Parse(columnOrder);
            if(int.Parse(draw) > 1 && intColumnsOrder > 0) //Cốt thêm cột Detail ở đầu nên phải trừ 1
                intColumnsOrder--;

            if (orderType.ToLower().Equals("asc"))
                tmp = tmp.OrderBy(p => p.GetType().GetProperty(orderName[intColumnsOrder]).GetValue(p, null)).ToList();
            else
                tmp = tmp.OrderByDescending(p => p.GetType().GetProperty(orderName[intColumnsOrder]).GetValue(p, null)).ToList();

            dataTablePage.data = tmp.Skip(int.Parse(start)).Take(length);

            return Tool.DataToJson(dataTablePage);
        }
    }
}
