﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Object
{
    public class ObjectBase
    {
        public long Id { set; get; }
        public DateTime CreatedDate { set; get; }
        public DateTime UpdatedDate { set; get; }

        public ObjectBase()
        {
            Id = 0;
            CreatedDate = DateTime.Now;
            UpdatedDate = CreatedDate;
        }
    }
}
