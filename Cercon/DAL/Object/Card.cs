﻿using Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DAL.Object
{
    public class Card: ObjectBase
    {
        public string CardNo { set; get; }
        public string CardName { set; get; }
        public string Doctor { set; get; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Valid { set; get; }
        public string Lab { set; get; }
        public string TopLeft { set; get; }
        public string TopRight { set; get; }
        public string BottomLeft { set; get; }
        public string BottomRight { set; get; }
        public TypeCard TypeCard { set; get; }
        public long CreatedByUserId { set; get; }
        public long UpdatedByUserId { set; get; }

        public Card()
        {
            TopLeft = "";
            TopRight = "";
            BottomLeft = "";
            BottomRight = "";
            CreatedByUserId = 0;
            UpdatedByUserId = 0;
        }
    }
}
