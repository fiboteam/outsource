using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class SmsInteract : BusinessObject
    {
        public string Keyword { get; set; }
        public string ReplyMessage { get; set; }
        public SmsInteractStatus SmsInteractStatus { get; set; }
        public Int32 SmsID { set; get; }
        public TypeRelateID TypeRelateID { set; get; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public SmsInteract()
        {

        }
    }
}
