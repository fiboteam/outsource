using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class SurveyQuestion : BusinessObject
    {
        public SurveyQuestion()
        {
            
        }

        public string Question { get; set; }
        public string Answers1 { get; set; }
        public string Answers2 { get; set; }
        public string Answers3 { get; set; }
        public string Answers4 { get; set; }
        public Int64 SurveyID { get; set; }
        public Int64 QuestionContinueID { get; set; }
        public SurveyQuestionStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
