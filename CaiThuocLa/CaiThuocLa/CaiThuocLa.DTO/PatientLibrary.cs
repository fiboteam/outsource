using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class PatientLibrary : BusinessObject
    {
        public PatientLibrary()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
        public Int64 PatientID { get; set; } 
        public Int64 LibraryID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
