using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using CaiThuocLa.Utility;

namespace CaiThuocLa.DTO
{
    public class SendSMS : BusinessObject
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
        public string SMSGuidID { get; set; }
        public int ServiceType { get; set; }
        public SendSMSStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ShortCreatedDate { get; set; }

        public SendSMS()
        {

        }
    }
}
