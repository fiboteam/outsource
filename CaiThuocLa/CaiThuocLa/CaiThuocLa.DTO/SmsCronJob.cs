using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class SmsCronJob : BusinessObject
    {
        public Int64 LibraryID { get; set; } 
        public Int64 SmsID { get; set; } 
        public DateTime ExecutionTime { get; set; } 
        public Int16 SmsCronJobStatus { get; set; }
        public Int64 PatientID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
