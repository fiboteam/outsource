using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class SMS : BusinessObject
    {
        public SMS()
        {
            IsFirstMessage = 0;
        }
        public string Message { get; set; }
        public Int64 LibraryID { get; set; }
        public int ShortCreatedDate { get; set; }
        public Int16 SMSStatus { get; set; }
        public Int64 CreatedByID { get; set; }
        public Int64 UpdatedByID { get; set; }
        public int IsFirstMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
