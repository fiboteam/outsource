using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class Library : BusinessObject
    {
        public Library()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
        public string LibraryName { get; set; } 
        public Int64 NumberOfSMS { get; set; }
        public LibraryStatus_vi_VN LibraryStatus { get; set; }
        public LibraryType_vi_VN LibraryType { get; set; }
        public Int64 CreatedByID { get; set; } 
        public Int64 UpdatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
