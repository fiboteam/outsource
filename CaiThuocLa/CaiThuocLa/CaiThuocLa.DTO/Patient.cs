using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class Patient : BusinessObject
    {
             public string PatientName { get; set; } 

             public string PhoneNumber { get; set; } 

             public int Age { get; set; }

             public CaiThuocLa.Utility.Environment_vi_VN Environment { get; set; }

             public AddictionLevel_vi_VN AddictionLevel { get; set; }
             public PatientStatus_vi_VN PatientStatus { get; set; }

             public DateTime StartDate { get; set; } 

             public int ShortCreatedDate { get; set; } 

             public Int64 AccountManagerID { get; set; } 


    }
}
