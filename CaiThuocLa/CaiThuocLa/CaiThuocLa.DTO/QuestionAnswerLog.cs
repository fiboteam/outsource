using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class QuestionAnswerLog : BusinessObject
    {
        public QuestionAnswerLog()
        {
            
        }

        public Int64 SurveyQuestionID { get; set; }
        public Int16 AnswersIndex { get; set; }
        public string AnswersContent { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
