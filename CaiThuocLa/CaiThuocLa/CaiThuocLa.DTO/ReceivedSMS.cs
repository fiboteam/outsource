using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class ReceivedSMS : BusinessObject
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
        public string SMSGuidID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ShortCreatedDate { get; set; }

        public ReceivedSMS()
        {

        }
    }
}
