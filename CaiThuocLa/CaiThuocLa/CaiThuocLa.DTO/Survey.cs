using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class Survey : BusinessObject
    {
        public Survey()
        {
            
        }

        public string SurveyName { get; set; }
        public string SurveyKeyword { get; set; }
        public SurveyStatus_vi_VN Status { get; set; }
        public int TotalQuestion { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
