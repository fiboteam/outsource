using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class AssortPatient : BusinessObject
    {
        public string PhoneNumber { get; set; }
        public Int64 PatientID { get; set; }
        public Int64 SurveyID { get; set; }
        public AssortPatientType AssortType { get; set; }
        public AssortPatientStatus Status { get; set; }
        public Int16 AnswersCount { get; set; }
        public float Score { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public AssortPatient()
        {

        }
    }
}
