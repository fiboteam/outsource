using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace CaiThuocLa.DTO
{
    public class Account : BusinessObject
    {
        public Account():base()
        {
            LoginName = "";
            PassWord = "";
            PhoneNumber = "";
            EmailAddress = "";
            ParentId = -1;
        }

        /// <summary>
        /// User tài khoản
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// Mật khẩu đăng nhập
        /// </summary>
        public string PassWord { get; set; }

        public string FullName { get; set; }
        public long ParentId { get; set; }
        /// <summary>
        /// Số phone
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Email user
        /// </summary>
        public string EmailAddress { get; set; }


    }
}
