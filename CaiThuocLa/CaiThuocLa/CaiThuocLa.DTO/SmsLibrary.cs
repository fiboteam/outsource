﻿using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaiThuocLa.DTO
{
    public class SmsLibrary : BusinessObject
    {
        public Int64 SmsID { get; set; }
        public Int64 LibraryID { get; set; }
        public int SentDate { get; set; }
        public int SendingTime { get; set; }
        public int SentMinute { get; set; }
        public SmsLibraryStatus_vi_VN Status { get; set; }
    }
}
