﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;

using System.Configuration;
using System.Security.Cryptography;
using System.Text.RegularExpressions;


namespace CaiThuocLa.Utility
{
    public static class GFunction
    {
        /// <summary>
        /// Set value in XML config file
        /// </summary>
        /// <param name="filePath">The path to Config File, default is SystemConfig.xml</param>
        /// <param name="parameterName"></param>
        /// <param name="parameterValue"></param>
        public static void SetValueForSystemConfigParameter(string parameterName, string parameterValue, string filePath = "")
        {
            try
            {
                if (filePath.Length <= 0)
                    filePath = @"SystemConfig.xml";

                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.Load(filePath);

                XmlNode root = Xmldoc.DocumentElement;
                XmlNodeList listchildnode = root.ChildNodes;

                foreach (XmlElement element in listchildnode)
                {
                    if (element.Name.Equals(parameterName))
                    {
                        element.InnerText = parameterValue;
                        break;
                    }
                }

                Xmldoc.Save(filePath);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Get value of parameter from xml file config
        /// </summary>
        /// <param name="filePath">The path to Config File, default is SystemConfig.xml</param>
        /// <param name="parameterName"></param>
        /// <returns></returns>
        public static string GetSystemConfigParameter(string parameterName, string filePath = "")
        {
            try
            {
                if (filePath.Length <= 0)
                    filePath = @"SystemConfig.xml";

                XmlDocument Xmldoc = new XmlDocument();
                Xmldoc.Load(filePath);

                XmlNode root = Xmldoc.DocumentElement;
                XmlNodeList listchildnode = root.ChildNodes;

                foreach (XmlElement element in listchildnode)
                {
                    if (element.Name.Equals(parameterName))
                        return element.InnerText;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GetMD5(string pwd)
        {
            if (pwd != null)
            {
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] bHash = md5.ComputeHash(Encoding.UTF8.GetBytes(pwd));
                StringBuilder sbHash = new StringBuilder();

                foreach (byte b in bHash)
                {
                    sbHash.Append(String.Format("{0:x2}", b));
                }
                return sbHash.ToString();
            }
            return "";
        }

        //public static string Encrypt(string encryptString, string separator = "", string extension = "")
        //{
        //    string s = encryptString + separator + extension;
        //    return Crypto.ActionEncrypt(s);
        //}

        //public static string[] Decrypt(string encryptedString, string separator = "")
        //{
        //    string s = Crypto.ActionDecrypt(encryptedString);
        //    string[] separators = new string[] { separator };
        //    return s.Split(separators, StringSplitOptions.None);
        //}

        //public static string Decrypt(string encryptedString, string separator = "", int indexOfString = 0)
        //{
        //    string s = Crypto.ActionDecrypt(encryptedString);
        //    if (separator != null && separator.Length > 0)
        //        s = s.Substring(indexOfString, s.IndexOf(separator));

        //    return s;
        //}

        public static string ExtractDomainFromEmail(string email)
        {
            return email.Substring(email.IndexOf("@") + 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone">Phone list: 0909123123;090932433123</param>
        /// <param name="msg"></param>
        /// <returns></returns>
        //public static bool SendSMS(string phone, string msg)
        //{
        //    try
        //    {
        //        string[] phoneList = phone.Split(';');
        //        if (phoneList.Count() > 0)
        //        {
        //            Service smsService = new Service();
        //            string xml = "<Document>";
        //            foreach (string phoneNumber in phoneList)
        //            {
        //                if (phoneNumber.Length > 7 && phoneNumber.Length < 13)
        //                    xml += "<ListMobilePhone><PhoneNumber>" + phoneNumber + "</PhoneNumber><SMSGUID>FromELBSystem</SMSGUID><Message>" + msg + "</Message></ListMobilePhone>";
        //            }
        //            xml += "</Document>";
        //            string result = smsService.SendSMSToMultiMessage_WithoutSplit(ConfigurationManager.AppSettings.Get("SMS_CLIENT"), ConfigurationManager.AppSettings.Get("SMS_PASS"), "n/a", xml, ConfigurationManager.AppSettings.Get("SMS_SERVICE").AsInt());
        //            if (result.IndexOf("Sending...") > 0)
        //                return true;
        //        }

        //    }
        //    catch { }
        //    return false;
        //}

        /*
         * Author: hieu.lt@fibo.vn
         */
        public static string HashPassword(string password, string salt = "")
        {
            if (password != null)
            {
                return GetMD5(GetMD5(password) + salt);
            }

            return "";
        }

        /*
         * Author: hieu.lt@fibo.vn
         */
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        /*
         * Author: hieu.lt@fibo.vn
         */
        public static string GetExtensionDomain(string domain)
        {
            string extension;
            string[] explodeArray = domain.Trim().Trim('.').Split('.');
            List<string> explodeList = explodeArray.ToList();

            explodeList.RemoveAt(0);
            extension = String.Join(".", explodeList.ToArray()).ToLower();

            return extension.Trim();
        }

        /*
         * Author: hieu.lt@fibo.vn
         */
        public static string GetLastFirstName(string fullName, bool isGetLastName = false)
        {
            string lastName = "";
            string firstName = "";

            if (fullName.Trim() != "")
            {
                string[] fullNameSplited = fullName.Trim().Split(' ');
                lastName = fullNameSplited[0];
                List<string> fullNameList = fullNameSplited.ToList();
                fullNameList.RemoveAt(0);
                firstName = String.Join(" ", fullNameList.ToArray());
                firstName = (firstName.Trim() != "") ? firstName : lastName;
            }

            return isGetLastName ? lastName : firstName;
        }

        public static string ShowEnumOnComboBox(string typeName, string lang, string attr)
        {

            Type objtype = Type.GetType("FERP.Utility." + typeName + lang);
            // object entity = Activator.CreateInstance(objtype);
            //List<SelectListItem> liststatus = new List<SelectListItem>();
            /*
             @foreach (var type in memberGroupTypes)
             {
                 MemberGroupType _memberGroupType = (MemberGroupType)type;
                 string key = _memberGroupType.ToString();
                 int value = (int)_memberGroupType;
                 <option value="@value">@key</option>
             }*/
            string str = "<select " + attr + ">";
            var memberGroupTypes = Enum.GetValues(objtype);
            foreach (var type in Enum.GetValues(objtype))
            {

                string key = type.ToString();
                int value = (byte)type;
                str += "<option value='" + value + "'>" + key + "</option>";
            }
            str += "</select>";
            return str;
        }

        public static DateTime ConvertToMMddyyyy(string datetime)
        {
            if (!string.IsNullOrEmpty(datetime))
            {
                string dateconvert = DateTime.ParseExact(datetime.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture)
                            .ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
                return DateTime.Parse(dateconvert);
            }
            return DateTime.Parse("1900-01-01");

        }

        public static string ConvertToDateByShortDate(int shortDate)
        {
            string year = shortDate.ToString().Substring(0,4);
            string month = shortDate.ToString().Substring(4, 2);
            string day = shortDate.ToString().Substring(6, 2);
            return day + "-" + month + "-" + year;
        }

        public static List<long> ConvertStringListToLongList(List<string> lStr)
        {
            List<long> IDList = new List<long>();
            for (int i = 0; i < lStr.Count(); i++)
            {
                long id = Convert.ToInt64(lStr[i]);
                if (long.TryParse(lStr[i], out id))
                {
                    IDList.Add(id);
                }
            }
            return IDList;
        }

        public static string SubStringServerName(string str, int numberChar)
        {
            int index = str.Length;
            if (str.Length > numberChar)
            {
                for (int i = 15; i < str.Length; i++)
                {
                    if (Char.IsDigit(str[i - 1]) && str[i] == '_' && Char.IsLetter(str[i + 1]))
                    {
                        index = i;
                        break;
                    }
                }
            }
            if (index != str.Length)
            {
                string str1 = str.Substring(0, index);
                string str2 = str.Substring(index);
                str = str1 + "\n" + str2;
            }
            return str;
        }

        public static string SubString(string str, int length)
        {
            int index = str.Length;
            if (str.Length > length)
            {
                return str.Substring(0, length) + " ...";
            }
            return str;
        }

        public static string GetMobileStandardFibo(string mobile)
        {
            string str = "";
            string[] arr = { "-", ".", " " };
            foreach (string s in arr)
            {
                str = mobile.Replace(s, "");
            }
            if (str != "")
            {
                str = "(+84) " + str.Remove(0, 1);
            }

            return str;
        }

        public static string GetPhoneStandardFibo(string phone)
        {
            string str = "";

            if (phone != null && phone != "")
            {
                str = phone.Replace(".", "-");
            }

            return str;
        }

        /// <summary>
        /// phong.nd@fibo.vn
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }

        public static string[] specialCharacters = new string[] { "~","`","@","#","$","%","^","&","*","(",")","_","+","=","{","[","}","]"
            ,"\\","|",";",":","'","\"","/","?",",","<",">"," ","\n","\r"};

        /// <summary>
        /// phong.nd@fibo.vn
        /// </summary>
        /// <param name="domainName"></param>
        /// <returns></returns>
        public static bool CheckValidDomain(string domainName)
        {
            if (domainName == null || domainName.Trim() == "")
                return false;
            if (domainName.IndexOf(".") == 0 || domainName.EndsWith("."))
                return false;

            if (domainName.StartsWith("-") || domainName.EndsWith("-"))
                return false;

            if (!domainName.Contains("."))
            {
                return false;
            }

            string titleName = domainName.Split('.')[0];
            if (titleName.EndsWith("-"))
                return false;

            foreach (string invalid in specialCharacters)
            {
                if (domainName.Contains(invalid))
                    return false;
            }
            return true;
        }

        public static E ConvertEnum<T, E>(T value)
        {
            short temp = Convert.ToInt16(Convert.ChangeType(value, typeof(short)));
            E rs = (E)Convert.ChangeType(temp, typeof(E));
            return rs;
        }

        public static bool IsDigit(string str)
        {
            int outNumber = 0;
            bool result = Int32.TryParse(str, out outNumber);
            return result;
        }

        public static string DayOfWeekTypeHTML(int index, string lang)
        {
            string outPut = "";
            if (lang == "vi-VN")
            {
                switch (index)
                {
                    case 1:
                        outPut = "<span style=\"color:#F00\">Chủ nhật</span>";
                        break;
                    case 2:
                        outPut = "Thứ hai";
                        break;
                    case 3:
                        outPut = "Thứ ba";
                        break;
                    case 4:
                        outPut = "Thứ tư";
                        break;
                    case 5:
                        outPut = "Thứ năm";
                        break;
                    case 6:
                        outPut = "Thứ sáu";
                        break;
                    case 7:
                        outPut = "Thứ bảy";
                        break;
                }
            }
            else
            {
                switch (index)
                {
                    case 1:
                        outPut = "<span style=\"color:#F00\">Sunday</span>";
                        break;
                    case 2:
                        outPut = "Monday";
                        break;
                    case 3:
                        outPut = "Tuesday";
                        break;
                    case 4:
                        outPut = "Wednesday";
                        break;
                    case 5:
                        outPut = "Thursday";
                        break;
                    case 6:
                        outPut = "Friday";
                        break;
                    case 7:
                        outPut = "Saturday";
                        break;
                }
            }
            return outPut;
        }

        public static Type GetTypeLang(string fullName, string lang)
        {
            if (lang == "en-US")
            {
                lang = "";
            }
            lang = lang.Replace("-", "_");
            lang = lang != "" ? "_" + lang : "";

            /*
             * 1 : Lấy giá trị enum tropng file defince với ngôn ngữ tương ứng
             * 2 : Nếu không có định nghĩa trong file defile tiếp tục tìm trong file controller
             * 3 : Nếu cả trong file define và controller không có giá trị. Lấy giá trị default trong file define
             * 4 : Nếu trong file define không có thì tìm trong file controller.
             */
            Type type = Type.GetType(fullName + lang + ",CaiThuocLa.Utility");
            if (type == null)
                type = Type.GetType(fullName + lang + ",CaiThuocLa");
            if (type == null)
                type = Type.GetType(fullName + ",CaiThuocLa.Utility");
            if (type == null)
                type = Type.GetType(fullName + ",CaiThuocLa");
            return type;
        }

        public static string EnumDisplay(string enumFullName, object enumValue, string lang = "")
        {
            string strDisplay = "";
            try
            {
                Type type = GetTypeLang(enumFullName, lang);
                foreach (var value in Enum.GetValues(type))
                {
                    if ((short)value == (short)enumValue)
                    {
                        strDisplay = Enum.GetName(type, value).Replace("_", " ");
                        break;
                    }
                }
                strDisplay = strDisplay.ToString().Replace("_", " ");
            }
            catch
            {

            }
            return strDisplay;
        }

        private static string FiboAmountText(string gNumber)
        {
            string result = "";
            switch (gNumber)
            {
                case "0":
                    result = "không";
                    break;
                case "1":
                    result = "một";
                    break;
                case "2":
                    result = "hai";
                    break;
                case "3":
                    result = "ba";
                    break;
                case "4":
                    result = "bốn";
                    break;
                case "5":
                    result = "năm";
                    break;
                case "6":
                    result = "sáu";
                    break;
                case "7":
                    result = "bảy";
                    break;
                case "8":
                    result = "tám";
                    break;
                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }
        private static string Donvi(string so)
        {
            string Kdonvi = "";

            if (so.Equals("1"))
                Kdonvi = "";
            if (so.Equals("2"))
                Kdonvi = "nghìn";
            if (so.Equals("3"))
                Kdonvi = "triệu";
            if (so.Equals("4"))
                Kdonvi = "tỷ";
            if (so.Equals("5"))
                Kdonvi = "nghìn tỷ";
            if (so.Equals("6"))
                Kdonvi = "triệu tỷ";
            if (so.Equals("7"))
                Kdonvi = "tỷ tỷ";

            return Kdonvi;
        }
        private static string FiboAmountSplit(string tach3)
        {
            string Ktach = "";
            if (tach3.Equals("000"))
                return "";
            if (tach3.Length == 3)
            {
                string tr = tach3.Trim().Substring(0, 1).ToString().Trim();
                string ch = tach3.Trim().Substring(1, 1).ToString().Trim();
                string dv = tach3.Trim().Substring(2, 1).ToString().Trim();
                if (tr.Equals("0") && ch.Equals("0"))
                    Ktach = " không trăm lẻ " + FiboAmountText(dv.ToString().Trim()) + " ";
                if (!tr.Equals("0") && ch.Equals("0") && dv.Equals("0"))
                    Ktach = FiboAmountText(tr.ToString().Trim()).Trim() + " trăm ";
                if (!tr.Equals("0") && ch.Equals("0") && !dv.Equals("0"))
                    Ktach = FiboAmountText(tr.ToString().Trim()).Trim() + " trăm lẻ " + FiboAmountText(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm " + FiboAmountText(ch.Trim()).Trim() + " mươi " + FiboAmountText(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = " không trăm " + FiboAmountText(ch.Trim()).Trim() + " mươi ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = " không trăm " + FiboAmountText(ch.Trim()).Trim() + " mươi lăm ";
                if (tr.Equals("0") && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm mười " + FiboAmountText(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("0"))
                    Ktach = " không trăm mười ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("5"))
                    Ktach = " không trăm mười lăm ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = FiboAmountText(tr.Trim()).Trim() + " trăm " + FiboAmountText(ch.Trim()).Trim() + " mươi " + FiboAmountText(dv.Trim()).Trim() + " ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = FiboAmountText(tr.Trim()).Trim() + " trăm " + FiboAmountText(ch.Trim()).Trim() + " mươi ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = FiboAmountText(tr.Trim()).Trim() + " trăm " + FiboAmountText(ch.Trim()).Trim() + " mươi lăm ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = FiboAmountText(tr.Trim()).Trim() + " trăm mười " + FiboAmountText(dv.Trim()).Trim() + " ";

                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("0"))
                    Ktach = FiboAmountText(tr.Trim()).Trim() + " trăm mười ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("5"))
                    Ktach = FiboAmountText(tr.Trim()).Trim() + " trăm mười lăm ";

            }
            return Ktach;

        }
        public static string ConvertAmountNumberToAmountText(double gNum)
        {
            if (gNum == 0)
                return "Không đồng";

            string lso_chu = "";
            string tach_mod = "";
            string tach_conlai = "";
            double Num = Math.Round(gNum, 0);
            string gN = Convert.ToString(Num);
            int m = Convert.ToInt32(gN.Length / 3);
            int mod = gN.Length - m * 3;
            string dau = "[+]";

            // Dau [+ , - ]
            if (gNum < 0)
                dau = "[-]";
            dau = "";

            // Tach hang lon nhat
            if (mod.Equals(1))
                tach_mod = "00" + Convert.ToString(Num.ToString().Trim().Substring(0, 1)).Trim();
            if (mod.Equals(2))
                tach_mod = "0" + Convert.ToString(Num.ToString().Trim().Substring(0, 2)).Trim();
            if (mod.Equals(0))
                tach_mod = "000";
            // Tach hang con lai sau mod :
            if (Num.ToString().Length > 2)
                tach_conlai = Convert.ToString(Num.ToString().Trim().Substring(mod, Num.ToString().Length - mod)).Trim();

            ///don vi hang mod 
            int im = m + 1;
            if (mod > 0)
                lso_chu = FiboAmountSplit(tach_mod).ToString().Trim() + " " + Donvi(im.ToString().Trim());
            /// Tach 3 trong tach_conlai

            int i = m;
            int _m = m;
            int j = 1;
            string tach3 = "";
            string tach3_ = "";

            while (i > 0)
            {
                tach3 = tach_conlai.Trim().Substring(0, 3).Trim();
                tach3_ = tach3;
                lso_chu = lso_chu.Trim() + " " + FiboAmountSplit(tach3.Trim()).Trim();
                m = _m + 1 - j;
                if (!tach3_.Equals("000"))
                    lso_chu = lso_chu.Trim() + " " + Donvi(m.ToString().Trim()).Trim();
                tach_conlai = tach_conlai.Trim().Substring(3, tach_conlai.Trim().Length - 3);

                i = i - 1;
                j = j + 1;
            }
            if (lso_chu.Trim().Substring(0, 1).Equals("k"))
                lso_chu = lso_chu.Trim().Substring(10, lso_chu.Trim().Length - 10).Trim();
            if (lso_chu.Trim().Substring(0, 1).Equals("l"))
                lso_chu = lso_chu.Trim().Substring(2, lso_chu.Trim().Length - 2).Trim();
            if (lso_chu.Trim().Length > 0)
                lso_chu = dau.Trim() + " " + lso_chu.Trim().Substring(0, 1).Trim().ToUpper() + lso_chu.Trim().Substring(1, lso_chu.Trim().Length - 1).Trim() + " đồng";

            return lso_chu.ToString().Trim();

        }

        public static List<byte> GetListOfEnumValue(string enumFullName,string lang = "")
        {
            List<byte> listVal = new List<byte>();
            try
            {
                Type type = GetTypeLang(enumFullName, lang);
                foreach (var value in Enum.GetValues(type))
                {
                    listVal.Add((byte)value);
                }
            }
            catch
            {}
            return listVal;
        }

        public static string GetTimeFromLastPost(DateTime endDate)
        {
            string strResult = "";
            DateTime currentDate = DateTime.Now;
            if (Convert.ToInt32((currentDate - endDate).TotalDays) >= 1)
            {
                strResult = Convert.ToInt32((currentDate - endDate).TotalDays) + " ngày";
            }
            else if (Convert.ToInt32((currentDate - endDate).TotalHours) >= 1)
            {
                strResult = Convert.ToInt32((currentDate - endDate).TotalHours) + " giờ";
            }
            else if (Convert.ToInt32((currentDate - endDate).TotalMinutes) >= 1)
            {
                strResult = Convert.ToInt32((currentDate - endDate).TotalMinutes) + " phút";
            }
            else if (Convert.ToInt32((currentDate - endDate).TotalSeconds) >= 1)
            {
                strResult = Convert.ToInt32((currentDate - endDate).TotalSeconds) + " giây";
            }
            else
            {
                strResult = "";
            }
            return strResult;
        }


		

		public static string GetEnumLang(string enumFullName,long value,string lang)
		{
			if (lang == "en-US")
			{
				lang = "";
			}
			lang = lang.Replace("-", "_");
			lang = lang != "" ? "_" + lang : "";

			/*
			 * 1 : Lấy giá trị enum tropng file defince với ngôn ngữ tương ứng
			 * 2 : Nếu không có định nghĩa trong file defile tiếp tục tìm trong file controller
			 * 3 : Nếu cả trong file define và controller không có giá trị. Lấy giá trị default trong file define
			 * 4 : Nếu trong file define không có thì tìm trong file controller.
			 */
			Type type = Type.GetType(enumFullName + lang + ",Utility");
			if (type == null)
				type = Type.GetType(enumFullName + lang + ",FERP.Web");
			if (type == null)
				type = Type.GetType(enumFullName + ",Utility");
			if (type == null)
				type = Type.GetType(enumFullName + ",FERP.Web");

			foreach (byte val in Enum.GetValues(type))
			{
				if ((long)value == (long)val)
				{
					string name = Enum.GetName(type, val);
					return FirstCharToUpper(name.Replace("_", " "));
					
				}
			}

			return "";
			
		}

		public static string FirstCharToUpper(string str)
		{

			if(str == null)
				return str;

			char c = str[0];
			str = str.Remove(0,1);
			return c.ToString().ToUpper()+str.ToLower();
		}
		/// <summary>
		/// input: @params string
		/// return datetime type
		/// @str = '01-17-2014' MM-dd-yyyy or MM/dd/yyy
		/// </summary>
		/// <param name="str"></param>
		public static DateTime ConvertToDateTime(string str)
		{
			DateTime dt = new DateTime();
			List<string> arrStr = new List<string>();
			if (str == null || str == "")
				return dt;
			try
			{
				if (str.IndexOf('-') > 0)
				{
					arrStr = str.Split('-').ToList();
				}
				if (str.IndexOf('/') > 0)
				{
					arrStr = str.Split('/').ToList();
				}
				if (arrStr.Count == 3)
					dt = Convert.ToDateTime(arrStr[2] + "/" + arrStr[1] + "/" + arrStr[0]);
			}
			catch {
				dt = DateTime.Parse("1900-01-01");
			}
			return dt;
		}

        private static readonly string[] VietnameseSigns = new string[]
        {

            "aAeEoOuUiIdDyY",

            "áàạảãâấầậẩẫăắằặẳẵ",

            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

            "éèẹẻẽêếềệểễ",

            "ÉÈẸẺẼÊẾỀỆỂỄ",

            "óòọỏõôốồộổỗơớờợởỡ",

            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

            "úùụủũưứừựửữ",

            "ÚÙỤỦŨƯỨỪỰỬỮ",

            "íìịỉĩ",

            "ÍÌỊỈĨ",

            "đ",

            "Đ",

            "ýỳỵỷỹ",

            "ÝỲỴỶỸ"

        };

        public static string TrimUnicode(string str)
        {

            for (int i = 1; i < VietnameseSigns.Length; i++)
            {

                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }

            return str;

        }
    }
}