﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaiThuocLa.Utility
{

    public enum TypeRelateID : short
    {
        Không_liên_kết = 0,
        Tin_nhắn = 1,
        Từ_khóa = 2
    }

    public enum ExcelSheetAt : short
    {
        Sheet1 = 0,
        Sheet2 = 1,
        Sheet3 = 2,
        Sheet4 = 3
    }

    public enum ExcelColunmAt : short
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        F = 5
    }

    public enum Environment_vi_VN : short
    {
        /// <summary>
        /// Đô thị
        /// </summary>
        Đô_thị = 1,
        /// <summary>
        /// Ven đô
        /// </summary>
        Ven_đô = 2,
        /// <summary>
        /// Nông thôn
        /// </summary>
        Nông_thôn = 3
    }

    public enum AddictionLevel_vi_VN : short
    {
        /// <summary>
        /// Nặng
        /// </summary>
        Nặng = 1,
        /// <summary>
        /// Vừa
        /// </summary>
        Vừa = 2,
        /// <summary>
        /// Nhẹ
        /// </summary>
        Nhẹ = 3
    }

    public enum SMSStatus_vi_VN:short
    {
        Kích_hoạt=1,
        Ngưng_kích_hoạt = 2
    }

    public enum PatientStatus_vi_VN: short
    {
        Vừa_nhập = 1,
        Đang_cai_nghiện = 2,
        Tạm_dừng_nhận_tin = 3,
        Đã_xóa = 100
    }

    public enum SMSCronJobStatus_vi_VN:short
    {
        Chờ_gửi = 1,
        Đã_gửi = 2,
        Tạm_dừng = 3,
        Đã_xóa = 100
    }
    
    public enum TimeUnit_vi_VN : short
    {
        Giờ = 1,
        Ngày = 2,
        Tuần = 3,
        Tháng = 4,
        Năm = 5
    }

    public enum SmsLibraryStatus_vi_VN : short
    {
        Kích_hoạt = 1,
        Ngưng_kích_hoạt = 2 
    }

    public enum LibraryStatus_vi_VN : short
    {
        Chưa_Kích_hoạt = 1,
        Đã_kích_hoạt = 2,
        Đã_xóa = 100
    }

    public enum ReportType_vi_VN : short
    {
        Môi_trường_sống = 1,
        Mức_độ_nghiện = 2
    }

    public enum SurveyStatus_vi_VN: short
    {
        Kích_hoạt = 1,
        Ngưng_kích_hoạt = 2,
        Đã_xóa = 100
    }

    public enum LibraryType_vi_VN : short
    {
        Thư_viện_có_thời_gian_cố_định = 1,
        Thư_viện_thời_gian_thực_của_từng_bệnh_nhân = 2
    }

    public enum SurveyQuestionStatus: short
    {
        Kích_hoạt = 1,
        Ngưng_kích_hoạt = 2,
        Đã_xóa = 100
    }

    public enum AssortPatientType: short
    {
        Nhẹ = 1,
        Trung_bình = 2,
        Nặng = 3
        
    }

    public enum AssortPatientStatus : short
    {
        Đã_Hoàn_Thành = 1,
        Chưa_Hoàn_Thành = 2,
        Hủy = 3,
        Đã_Xóa = 100
    }

    public enum SmsInteractStatus : short
    {
        Kích_Hoạt = 1,
        Ngừng_Kích_Hoạt = 2,
        Đã_Xóa = 100
    }

    public enum SendSMSStatus : short
    {
        Đang_Gửi = 1,
        Thành_Công = 2,
        Thất_Bại = 3
    }
}
