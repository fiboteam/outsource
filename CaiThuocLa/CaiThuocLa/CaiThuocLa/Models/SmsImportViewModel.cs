﻿using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SmsImportViewModel
    {
        public SmsImportViewModel()
        {
        }

        public ExcelSheetAt ExcelSheetAt { get; set; }
        public ExcelColunmAt ExcelColunmMessage { get; set; }
        public ExcelColunmAt ExcelColunmSentDate { get; set; }
        public ExcelColunmAt ExcelColunmSendingTime { get; set; }
        public ExcelColunmAt ExcelColunmSentMinute { get; set; }
        public long LibraryID { get; set; }
    }
}