﻿using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class PatientReportModel : Model
    {
        public PatientReportModel()
        {
            FromDate = DateTime.Now.AddDays(-365);
            ToDate = DateTime.Now.AddDays(365);
            ReportType = ReportType_vi_VN.Môi_trường_sống;
        }
        public ReportType_vi_VN ReportType { get; set; }

        public int NumEnvironmentUrban {get;set;}
        public int NumEnvironmentSubUrban {get;set;}
        public int NumEnvironmentCountrySide {get;set;}
        public int NumAddictionLevelHeavy {get;set;}
        public int NumAddictionLevelMedium {get;set;}
        public int NumAddictionLevelMild {get;set;}
    }
}