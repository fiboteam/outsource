﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Nhập mật khẩu hiện tại")]
        [Display(Name = "Mật khẩu hiện tại")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Nhập mật khẩu mới")]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Nhập lại mật khẩu mới")]
        [Display(Name = "Mật khẩu hiện tại")]
        public string ConfirmNewPassword { get; set; }
    }
}