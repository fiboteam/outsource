﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SMSModel:Model
    {
        public string Message { get; set; }
        public Int64 LibraryID { get; set; }
        public int ShortCreatedDate { get; set; }
        public CaiThuocLa.Utility.SMSStatus_vi_VN SMSStatus { get; set; }
        public Int64 CreatedByID { get; set; }
        public Int64 UpdateByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        /// <summary>
        /// Lấy trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Danh sách người cai nghiện show index
        /// </summary>
        public List<SMS> DataList { get; set; }
        /// <summary>
        /// Class phân trang
        /// </summary>
        public Paging Page { get; set; }

        public SMSModel()
        {
            DataList = new List<SMS>();
            Page = new Paging() { PageSize = 100 };
            CreatedDate = DateTime.Now;
            UpdateDate = DateTime.Now;
            Message = "";

        }
    }
}