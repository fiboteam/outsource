﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class PatientModel : Model
    {
        public PatientModel()
        {
            AccountManagerID = -1;
            Page = new Paging() { PageSize = 100 };
            PatientName = "";
            PhoneNumber = "";
            Age = 0;
            DataList = new List<Patient>();
            //Environment = CaiThuocLa.Utility.Environment.Urban;
            FromDate = DateTime.Now.AddDays(-365);
            ToDate = DateTime.Now.AddDays(365);
            StartDate = DateTime.Now;
        }

        public string PatientName { get; set; }
        public string PhoneNumber { get; set; }
        public int Age { get; set; }
        public CaiThuocLa.Utility.Environment_vi_VN Environment { get; set; }
        public CaiThuocLa.Utility.AddictionLevel_vi_VN AddictionLevel { get; set; }
        public DateTime StartDate { get; set; }
        public int ShortCreatedDate { get; set; }
        public Int64 AccountManagerID { get; set; }
        /// <summary>
        /// Lấy trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Danh sách người cai nghiện show index
        /// </summary>
        public List<Patient> DataList { get; set; }
        /// <summary>
        /// Class phân trang
        /// </summary>
        public long LibraryID { get; set; }
        public Paging Page { get; set; }
    }
}