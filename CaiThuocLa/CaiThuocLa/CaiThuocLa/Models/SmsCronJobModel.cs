﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SmsCronJobModel : Model
    {
        public Int64 LibraryID { get; set; }
        public Int64 SmsID { get; set; }
        public int ExecutionTime { get; set; }
        public SMSCronJobStatus_vi_VN SmsCronJobStatus { get; set; }
        public Int64 PatientID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        /// <summary>
        /// Lấy trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// Danh sách người cai nghiện show index
        /// </summary>
        public List<SmsCronJob> DataList { get; set; }
        /// <summary>
        /// Class phân trang
        /// </summary>
        public Paging Page { get; set; }

        public SmsCronJobModel()
        {
            Page = new Paging() { PageSize = 100 };
            DataList = new List<SmsCronJob>();
        }
    }
}