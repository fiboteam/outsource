﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class AccountModel
    {
        [Required(ErrorMessage = "Nhập tài khoản của bạn")]
        [Display(Name = "Tài khoản")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Nhập mật khẩu của bạn")]
        [Display(Name = "Mật khẩu")]
        public string PassWord { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}