﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SurveyQuestionModel: Model
    {
        public string Question { get; set; }
        public string Answers1 { get; set; }
        public string Answers2 { get; set; }
        public string Answers3 { get; set; }
        public string Answers4 { get; set; }
        public Int64 SurveyID { get; set; }
        public Int64 QuestionContinueID { get; set; }
        public SurveyQuestionStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int CurrentPage { get; set; }
        public List<SurveyQuestion> DataList { get; set; }
        public string AnswersSearch { set; get; }
        public string returnUrl { set; get; }

        public SurveyQuestionModel()
        {
            Question = "";
            Answers1 = "";
            Answers2 = "";
            Answers3 = "";
            Answers4 = "";
            AnswersSearch = "";
            returnUrl = "";
            SurveyID =-1;
            QuestionContinueID = -1;
            Status = 0;
            CurrentPage = 0;
            DataList = new List<SurveyQuestion>();
            Paging = new Paging() { PageSize = 100 };
        }

        public SurveyQuestionModel(Int64 surveyID)
        {
            Question = "";
            Answers1 = "";
            Answers2 = "";
            Answers3 = "";
            Answers4 = "";
            AnswersSearch = "";
            SurveyID = surveyID;
            QuestionContinueID = -1;
            Status = 0;
            CurrentPage = 0;
            DataList = new List<SurveyQuestion>();
            Paging = new Paging() { PageSize = 100 };
        }
    }
}