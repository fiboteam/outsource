﻿using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SurveyExtenalModel
    {
        public string PhoneNumber { set; get; }
        public Int64 SurveyID { set; get; }
        public string SurveyName { set; get; }
        public List<SurveyQuestion> QuestionList { set; get; }

        public SurveyExtenalModel()
        {
            QuestionList = new List<SurveyQuestion>();
        }
    }
}