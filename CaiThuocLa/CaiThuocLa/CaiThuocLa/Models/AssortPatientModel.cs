﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class AssortPatientModel : Model
    {
        public string PhoneNumber { get; set; }
        public Int64 SurveyID { get; set; }
        public AssortPatientType AssortType { get; set; }
        public AssortPatientStatus Status { get; set; }
        public Int16 AnswersCount { get; set; }
        public float Score { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int CurrentPage { get; set; }
        public List<AssortPatient> DataList { get; set; }

        public AssortPatientModel()
        {
            PhoneNumber = "";
            SurveyID = -1;
            AssortType = 0;
            Status = 0;
            AnswersCount = 0;
            Score = 0;
            CurrentPage = 0;
            DataList = new List<AssortPatient>();
            Paging = new Paging() { PageSize = 100 };
        }
    }
}