﻿using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SendSMSModel : Model
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
        public string SMSGuidID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ShortCreatedDate { get; set; }
        public int CurrentPage { get; set; }
        public SendSMSStatus Status { set; get; }
        public List<SendSMS> DataList { get; set; }

        public SendSMSModel()
        {
            CurrentPage = 0;
            DataList = new List<SendSMS>();
            Paging = new Code.Paging() { PageSize = 100 };
        }
    }
}