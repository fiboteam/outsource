﻿using CaiThuocLa.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CaiThuocLa.Models
{
    public class Model
    {
        public long ID { get; set; }
        public string ErrorMessage { get; set; }
        public string SuccessMessage { get; set; }
        public Paging Paging { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public void AssignProperties(object source) //Vu.L Lưu ý vẫn cần phải set lại CreatedDate và UpdateDate mới đúng
        {
            foreach (var i in this.GetType().GetProperties())
            {
                PropertyInfo pInfo = source.GetType().GetProperty(i.Name);
                if (pInfo != null)
                {
                    i.SetValue(this, pInfo.GetValue(source, null), null);
                }
            }
        }
    }
}