﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SmsInteractModel: Model
    {
        public string Keyword { get; set; }
        public string ReplyMessage { get; set; }
        public SmsInteractStatus SmsInteractStatus { get; set; }
        public Int32 SmsID { set; get; }
        public string ContentSMS { set; get; }
        public TypeRelateID TypeRelateID { set; get; }
        public List<SmsInteract> DataList { get; set; }
        public int CurrentPage { get; set; }

        public SmsInteractModel()
        {
            Keyword = "";
            ReplyMessage = "";
            SmsInteractStatus = 0;
            ContentSMS = "";
            SmsID = 0;
            CurrentPage = 0;
            TypeRelateID = 0;
            DataList = new List<SmsInteract>();
            Paging = new Code.Paging() { PageSize = 100 };
        }
    }
}