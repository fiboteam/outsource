﻿using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class ReceivedSMSModel: Model
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
        public string SMSGuidID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ShortCreatedDate { get; set; }
        public int CurrentPage { get; set; }
        public List<ReceivedSMS> DataList { get; set; }

        public ReceivedSMSModel()
        {
            CurrentPage = 0;
            DataList = new List<ReceivedSMS>();
            Paging = new Code.Paging() { PageSize = 100 };
        }
    }
}