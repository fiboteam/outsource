﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SurveyModel : Model
    {
        public int CurrentPage { get; set; }
        public List<Survey> DataList { get; set; }
        public string SurveyName { get; set; }
        public string SurveyKeyword { get; set; }
        public SurveyStatus_vi_VN Status { get; set; }
        public int TotalQuestion { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public SurveyModel()
        {
            CurrentPage = 0;
            DataList = new List<Survey>();
            Paging = new Code.Paging() { PageSize = 100 };
            SurveyName = "";
            SurveyKeyword = "";
            Status = 0;
            TotalQuestion = 0;
        }
    }
}