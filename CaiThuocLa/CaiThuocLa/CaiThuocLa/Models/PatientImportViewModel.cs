﻿using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class PatientImportViewModel
    {
        public PatientImportViewModel()
        {
        }

        public ExcelSheetAt ExcelSheetAt { get; set; }
        public ExcelColunmAt ExcelColunmPatientName { get; set; }
        public ExcelColunmAt ExcelColunmPhoneNumber { get; set; }
        public ExcelColunmAt ExcelColunmAge { get; set; }
        public ExcelColunmAt ExcelColunmEnvironment { get; set; }
        public ExcelColunmAt ExcelColunmAddictionLevel { get; set; }
        public ExcelColunmAt ExcelColunmStartDate { get; set; }
        public long LibraryID { get; set; }
    }
}