﻿using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    
    public class SmsLibraryModel
    {
        public SmsLibraryModel()
        {
            SendingTime = 0;
        }
        public long SmsID { get; set; }
        public long LibraryID { get; set; }
        public int SendingTime {get;set;}
        public int SentDate { get; set; }
        public TimeUnit_vi_VN TimeUnit { get; set; }
        public List<SmsLibrary> DataList { get; set; }
    }
}