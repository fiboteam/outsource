﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class LibraryModel : Model
    {

        public LibraryModel()
        {
            Page = new Paging() { PageSize = 100 };
            LibraryName = "";
            LibraryType = 0;
            DataList = new List<Library>();
            //Environment = CaiThuocLa.Utility.Environment.Urban;
        }

        public string LibraryName { get; set; }
        public LibraryStatus_vi_VN LibraryStatus { get; set; }
        public LibraryType_vi_VN LibraryType { get; set; }
        public long CreatedById { get; set; }
        public long UpdatedById { get; set; }
        public int CurrentPage { get; set; }
        /// <summary>
        /// Danh sách người cai nghiện show index
        /// </summary>
        public List<Library> DataList { get; set; }
        /// <summary>
        /// Class phân trang
        /// </summary>
        public Paging Page { get; set; }
    }
}