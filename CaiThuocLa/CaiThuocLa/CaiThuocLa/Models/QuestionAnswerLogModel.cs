﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class QuestionAnswerLogModel: Model
    {
        public Int64 SurveyQuestionID { get; set; }
        public Int16 AnswersIndex { get; set; }
        public string AnswersContent { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int CurrentPage { get; set; }
        public List<QuestionAnswerLog> DataList { get; set; }
        public string Question { set; get; }

        public QuestionAnswerLogModel()
        {
            SurveyQuestionID = -1;
            AnswersIndex = -1;
            AnswersContent = "";
            PhoneNumber = "";
            CurrentPage = 0;
            DataList = new List<QuestionAnswerLog>();
            Paging = new Paging() { PageSize = 100 };
            Question = "";
        }
    }
}