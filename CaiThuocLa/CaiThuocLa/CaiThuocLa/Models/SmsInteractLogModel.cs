﻿using CaiThuocLa.Code;
using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Models
{
    public class SmsInteractLogModel: Model
    {
        public Int64 SmsInteractID { get; set; }
        public string PhoneNumber { get; set; }
        public List<SmsInteractLog> DataList { get; set; }
        public int CurrentPage { get; set; }
        public string Keyword { set; get; }

        public SmsInteractLogModel()
        {
            Keyword = "";
            SmsInteractID = -1;
            PhoneNumber = "";
            CurrentPage = 0;
            DataList = new List<SmsInteractLog>();
            Paging = new Code.Paging() { PageSize = 100 };
        }
    }
}