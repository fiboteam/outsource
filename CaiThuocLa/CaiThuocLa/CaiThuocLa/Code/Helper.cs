﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Expressions;

namespace CaiThuocLa.Code
{
    public static class Helper
    {
        #region EnumDropDownList

        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "", Value = "" } };
        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, TEnum selectedValue)
        {
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem
                                                {
                                                    Text = value.ToString(),
                                                    Value = value.ToString(),
                                                    Selected = (value.Equals(selectedValue))
                                                };

            return System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlHelper, name, items);
        }

        //<%= Html.EnumDropDownList("Person.FavoriteColor", Model.Person.FavoriteColor) %>
        public static MvcHtmlString EnumDropDownList(this HtmlHelper htmlHelper, string name, Type type, object selected,
            IDictionary<string, string> enumExtension)
        {
            var enums = new List<SelectListItem>();

            if (enumExtension != null)
            {
                enums.AddRange(enumExtension.Select(value => new SelectListItem
                {
                    Value = value.Value,
                    Text = value.Key
                }));
            }

            foreach (short value in Enum.GetValues(type))
            {
                var item = new SelectListItem
                {
                    Value = value.ToString(),
                    Text = Enum.GetName(type, value)
                };

                if (selected != null)
                    item.Selected = Convert.ToInt16(selected) == value;

                enums.Add(item);
            }

            return System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlHelper, name, enums.AsEnumerable());
        }
        public static Type GetTypeLang(string fullName, string lang)
        {
            if (lang == "en-US")
            {
                lang = "";
            }
            lang = lang.Replace("-", "_");
            lang = lang != "" ? "_" + lang : "";

            /*
             * 1 : Lấy giá trị enum tropng file defince với ngôn ngữ tương ứng
             * 2 : Nếu không có định nghĩa trong file defile tiếp tục tìm trong file controller
             * 3 : Nếu cả trong file define và controller không có giá trị. Lấy giá trị default trong file define
             * 4 : Nếu trong file define không có thì tìm trong file controller.
             */
            Type type = Type.GetType(fullName + lang + ",CaiThuocLa.Utility");
            if (type == null)
                type = Type.GetType(fullName + ",CaiThuocLa.Utility");

            return type;
        }
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IDictionary<string, string> enumExtension = null, bool getAll = true, string lang = "", object htmlAttributes = null, IList<string> exceptedValue = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            List<SelectListItem> enumLangs = new List<SelectListItem>();

            try
            {
                Type enumTypeLang = GetTypeLang(enumType.FullName, lang);

                // lấy giá trị của enum ngôn ngữ tương ứng
                foreach (short value in Enum.GetValues(enumTypeLang))
                {
                    if (exceptedValue == null || !exceptedValue.Contains(value.ToString()))
                    {
                        SelectListItem item = new SelectListItem();
                        item.Value = value.ToString();
                        item.Text = Enum.GetName(enumTypeLang, value).Replace("_", " ");
                        item.Selected = value.Equals(metadata.Model);
                        enumLangs.Add(item);
                    }
                }

                if (getAll)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = "0";
                    item.Text = "Toàn bộ";
                    enumLangs.Insert(0, item);
                }

                // thêm extensions
                if (enumExtension != null)
                {
                    foreach (KeyValuePair<string, string> value in enumExtension)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Value = value.Value;
                        item.Text = value.Key;
                        enumLangs.Insert(0, item);
                    }
                }

                if (metadata.IsNullableValueType)
                {
                    enumLangs = SingleEmptyItem.Concat(enumLangs).ToList();
                }
            }
            catch
            {
                SelectListItem seItem = new SelectListItem();
                seItem.Text = "Error. Not loaded enum";
                seItem.Value = 0.ToString();
                enumLangs.Add(seItem);
            }

            return System.Web.Mvc.Html.SelectExtensions.DropDownListFor(htmlHelper, expression, enumLangs, htmlAttributes);
        }

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }


        #endregion EnumDropDownList

    }
}