﻿using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using CaiThuocLa.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace CaiThuocLa.Code
{
    public static class Tools
    {
        public static string ConvertSurveyQuestionToString(SurveyQuestion source)
        {
            StringBuilder content = new StringBuilder();
            content.AppendLine(source.Question);
            content.AppendLine(source.Answers1);
            content.AppendLine(source.Answers2);
            content.AppendLine(source.Answers3);
            content.AppendLine(source.Answers4);
            return content.ToString();
        }

        public static bool AddReceivedSMS(string message, string phoneNumber, string guid)
        {
            SqlReceivedSMSDao _receivedSMS = new SqlReceivedSMSDao();
            DateTime timeNow = DateTime.Now;

            ReceivedSMS item = new ReceivedSMS()
            {
                Message = message,
                PhoneNumber =phoneNumber,
                SMSGuidID = guid,
                CreatedDate = timeNow,
                UpdatedDate = timeNow,
                ShortCreatedDate = int.Parse(timeNow.ToString("yyyyMMdd"))
            };

            return _receivedSMS.Insert(item);
        }

        public static int SendSMSToUser(string clientPhoneNumber, string message, string guid)
        {
            int code = 500;
            try
            {
                string clientNo = System.Configuration.ConfigurationManager.AppSettings["ClientNo"];
                string clientPass = System.Configuration.ConfigurationManager.AppSettings["ClientPass"];
                string serviceType = System.Configuration.ConfigurationManager.AppSettings["ServiceTypeSms2WayId"];
                SMSServiceReference.ServiceSoapClient client = new SMSServiceReference.ServiceSoapClient();
                string result = client.SendSMS(clientNo, clientPass, clientPhoneNumber, message, guid, Convert.ToInt32(serviceType));
                code = Convert.ToInt32(Regex.Match(result, "<Code>(.*?)</Code>").Groups[1].Value);
                //code = 200;

                if (code == 200)
                {
                    DateTime timeNow = DateTime.Now;
                    SendSMS item = new SendSMS()
                    {
                        Message = message,
                        PhoneNumber = clientPhoneNumber,
                        SMSGuidID = guid,
                        ServiceType = int.Parse(serviceType),
                        Status = SendSMSStatus.Đang_Gửi,
                        CreatedDate = timeNow,
                        UpdatedDate = timeNow,
                        ShortCreatedDate = int.Parse(timeNow.ToString("yyyyMMdd"))
                    };

                    SqlSendSMSDao _sendSMS = new SqlSendSMSDao();
                    if(!_sendSMS.Insert(item))
                    {
                        return 500;
                    }
                }
            }
            catch (Exception ex)
            {
                return 500;
            }
            return code;
        }
    }
}