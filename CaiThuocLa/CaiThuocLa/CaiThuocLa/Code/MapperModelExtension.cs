﻿using CaiThuocLa.Models;
using System;
using System.ComponentModel;
using System.Reflection;
using CaiThuocLa.DTO;

namespace CaiThuocLa.Code
{
    
    public static class MapperModelExtension
    {

        /// <summary>
        /// Lấy dữ liệu cho object tự động từ model. Tự động cập nhật UpdatedDate 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="businessobj">BusinessObject cần lấy dữ liệu</param>
        /// <param name="viewModel">Model dùng để lấy dữ liệu</param>
        /// <returns></returns>
        public static BusinessObject GetDataFromModel<T> (this BusinessObject businessobj, T viewModel)
        {
            foreach (PropertyInfo infoobj in businessobj.GetType().GetProperties())
            {
                PropertyInfo infomodel = Array.Find(viewModel.GetType().GetProperties(), t => t.Name == infoobj.Name);
                if (infomodel != null)
                {
                    object value = infomodel.GetValue(viewModel, null);
                    if (value != null)
                    {
                        infoobj.SetValue(businessobj, TypeDescriptor.GetConverter(infomodel.PropertyType).ConvertFrom(value.ToString()), null);
                    }
                }
                if(infoobj.Name=="UpdatedDate")
                {
                    infoobj.SetValue(businessobj, DateTime.Now, null);
                }
            }
            return businessobj;
        }

        /// <summary>
        /// Convert Model thành BussinessObject.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static T ConvertToBusinessObject<T>(this Model viewModel)
        {
            Type typeobj = typeof(T);
            Type typemodel = viewModel.GetType();
            T entity = (T)Activator.CreateInstance(typeobj);
            foreach (PropertyInfo infomodel in typemodel.GetProperties())
            {
                PropertyInfo infoobj = Array.Find(typeobj.GetProperties(), t => t.Name == infomodel.Name);
                if (infoobj != null)
                {
                    object value = infomodel.GetValue(viewModel, null);
                    if (value != null)
                    {
                        try
                        {
                            infoobj.SetValue(entity, TypeDescriptor.GetConverter(infomodel.PropertyType).ConvertFrom(value.ToString()), null);
                        }
                        catch (Exception)
                        {
                        }
                        
                    }
                }
            }
            return entity;
        }


        public static T ConvertToViewModel<T>(this BusinessObject businessobj)
        {
            Type typemodel = typeof(T);
            Type typeobj = businessobj.GetType();
            T entity = (T)Activator.CreateInstance(typemodel);
            foreach (PropertyInfo infoobj in typeobj.GetProperties())
            {
                PropertyInfo infomodel = Array.Find(typemodel.GetProperties(), t => t.Name == infoobj.Name);
                if (infomodel != null )
                {
                    object value = infoobj.GetValue(businessobj, null);
                    if (value != null)
                    {
                        infomodel.SetValue(entity, TypeDescriptor.GetConverter(infoobj.PropertyType).ConvertFrom(value.ToString()), null);
                    }
                }
            }
            return entity;
        }
    }

}


