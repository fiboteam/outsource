using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaiThuocLa.Code
{
    public class PagingModel
    {
        /// <summary>
        /// Gets or sets the total records.
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        /// Gets or sets the current page.
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Gets or sets the number of record per page.
        /// </summary>
        public int PageSize { get; set; }

        public int PageBlock { get; set; }
    }
}