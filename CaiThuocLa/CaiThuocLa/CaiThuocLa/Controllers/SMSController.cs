﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class SMSController : BaseController
    {
        //
        // GET: /SMS/
        public SqlSMSDao _smsDAO = new SqlSMSDao();
        public SqlLibraryDao _libraryDAO = new SqlLibraryDao();
        public SqlSmsLibraryDao _slDAO = new SqlSmsLibraryDao();

        public ActionResult Index(SMSModel model)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;

            string message = ConvertSignToUnSign(model.Message);

            //Load SMS
            model.DataList = _smsDAO.GetWithFilter(
                model.Page.PageSize,
                model.Page.CurrentPage,
                message,
                model.LibraryID,
                (short)model.SMSStatus
                );

            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Page.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Page.TotalRecords = 0;

            }

            List<Library> libraryList = _libraryDAO.GetList();

            Library all = new Library();
            all.ID = 0;
            all.LibraryName = "Tất cả";
            libraryList.Insert(0, all);

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            //ViewBag.SuccessMessage = (Session["SuccessMessage"] != null) ? Session["SuccessMessage"] : "";
            //ViewBag.ErrorMessage = (Session["ErrorMessage"] != null) ? Session["ErrorMessage"] : "";
            //Session["SuccessMessage"] = "";
            //Session["ErrorMessage"] = "";

            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";

            return View(model);
        }

        public ActionResult Add()
        {
            SMSModel model = new SMSModel();

            List<Library> libraryList = _libraryDAO.GetList();

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");

            return View();
        }

        [HttpPost]
        public ActionResult Add(SMSModel model)
        {
            string message = ConvertSignToUnSign(model.Message);
            //if (message == "error")
            //{
            //    ErrorMessage = "Nội dung tin nhắn không hợp lệ";
            //}
            //else
            //{
            SMS sms = new SMS();
            sms.ID = 0;
            sms.Message = message;
            sms.LibraryID = model.LibraryID;
            //sms.SMSStatus = (short)model.SMSStatus;
            sms.SMSStatus = (short)SMSStatus_vi_VN.Kích_hoạt;
            sms.CreatedByID = SAccount.ID;
            sms.UpdatedByID = SAccount.ID;
            sms.CreatedDate = DateTime.Now;
            sms.UpdatedDate = DateTime.Now;

            if (_smsDAO.Insert(sms))
            {
                SuccessMessage = "Đã thêm một tin nhắn thành công.";
            }
            else
            {
                ErrorMessage = "Thêm thất bại";
            }
            //}
            return Redirect("/SMS");
        }

        public ActionResult Edit(Int64 smsid)
        {
            SMSModel model = new SMSModel();
            SMS sms = new SMS();
            sms = _smsDAO.GetSingle(smsid);
            if (sms != null)
            {
                model.ID = sms.ID;
                model.Message = sms.Message;
                model.LibraryID = sms.LibraryID;
                model.SMSStatus = (SMSStatus_vi_VN)sms.SMSStatus;
                model.CreatedByID = sms.CreatedByID;
                model.UpdateByID = sms.UpdatedByID;
                model.CreatedDate = sms.CreatedDate;
                model.UpdateDate = sms.UpdatedDate;
                model.ShortCreatedDate = sms.ShortCreatedDate;
            }
            /*
            * Load combobox library search box
           */
            List<Library> libraryList = _libraryDAO.GetList();
            //Library all = new Library();
            //all.ID = 0;
            //all.LibraryName = "Tất cả";
            //libraryList.Insert(0, all);

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(SMSModel model)
        {
            string message = ConvertSignToUnSign(model.Message);

            //if (message == "error")
            //{
            //    ErrorMessage = "Nội dung tin nhắn không hợp lệ";
            //}
            //else
            //{
            SMS sms = new SMS();
            sms.ID = model.ID;
            sms.Message = message;
            sms.LibraryID = model.LibraryID;
            sms.CreatedDate = model.CreatedDate;
            sms.UpdatedDate = DateTime.Now;
            sms.CreatedByID = model.CreatedByID;
            sms.UpdatedByID = SAccount.ID;
            sms.SMSStatus = (short)model.SMSStatus;

            if (_smsDAO.Update(sms))
            {
                SuccessMessage = "Đã cập nhật tin nhắn thành công.";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            //}

            return Redirect("/SMS");
        }

        public ActionResult Delete(Int64 smsID)
        {
            SMS sms = _smsDAO.GetSingle(smsID);
            if (sms == null)
            {
                ErrorMessage = "chức năng này chưa làm";
            }
            else
            {
                if (_smsDAO.Delete(sms))
                {
                    SuccessMessage = "Xóa tin nhắn thành công";
                }
                else
                {
                    ErrorMessage = "Xóa tin nhắn thất bại";
                }
            }

            return Redirect("/SMS");
        }


        public ActionResult SMSImport()
        {
            List<Library> libraryList = _libraryDAO.GetList();
            //Library all = new Library();
            //all.ID = 0;
            //all.LibraryName = "Chưa xác định";
            //libraryList.Insert(0, all);
            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            return View();
        }

        [HttpPost]
        public ActionResult Import(SmsImportViewModel model, HttpPostedFileBase fileImport = null)
        {
            //string path = "/Userfile/FileImport";
            //string returnPath = path + "/" + UploadFile(fileImport, path, null);
            //ExecImport(model, returnPath);

            ExecImportNew(model, fileImport);
            return Redirect("/Sms");
        }

        public void ExecImportNew(SmsImportViewModel model, HttpPostedFileBase fileImport)
        {
            try
            {
                List<SMS> smsFails = new List<SMS>();
                List<SMS> smsSuccess = new List<SMS>();

                HSSFWorkbook hssfworkbook;
                hssfworkbook = new HSSFWorkbook(fileImport.InputStream);

                ISheet sheet = hssfworkbook.GetSheetAt((int)model.ExcelSheetAt);
                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                bool isHeader = true;
                while (rows.MoveNext())
                {
                    try
                    {
                        if (!isHeader)
                        {
                            IRow row = (HSSFRow)rows.Current;
                            ICell cellMessage = row.GetCell((int)model.ExcelColunmMessage);
                            ICell cellSentDate = row.GetCell((int)model.ExcelColunmSentDate);
                            ICell cellSendingTime = row.GetCell((int)model.ExcelColunmSendingTime);
                            ICell cellSentMinute = row.GetCell((int)model.ExcelColunmSentMinute);

                            SMS sms = new SMS();
                            sms.CreatedByID = SAccount.ID;
                            sms.Message = (cellMessage != null) ? cellMessage.StringCellValue : "";
                            sms.SMSStatus = (short)SMSStatus_vi_VN.Kích_hoạt;
                            sms.ShortCreatedDate = 0;
                            sms.UpdatedByID = SAccount.ID;
                            sms.LibraryID = model.LibraryID;
                            sms.IsFirstMessage = 0;
                            sms.CreatedDate = DateTime.Now;
                            sms.UpdatedDate = DateTime.Now;

                            if (_smsDAO.Insert(sms))
                            {
                                if (model.LibraryID > 0)
                                {
                                    SmsLibrary sl = new SmsLibrary();
                                    sl.LibraryID = model.LibraryID;
                                    sl.SmsID = sms.ID;
                                    sl.Status = SmsLibraryStatus_vi_VN.Kích_hoạt;
                                    sl.SentDate = (cellSentDate != null) ? (int)cellSentDate.NumericCellValue : -1;
                                    sl.SendingTime = (cellSendingTime != null) ? (int)cellSendingTime.NumericCellValue : 0;
                                    sl.SentMinute = (cellSentMinute != null) ? (int)cellSentMinute.NumericCellValue : 0;
                                    _slDAO.Insert(sl);
                                }
                                smsSuccess.Add(sms);
                            }
                            else
                            {
                                smsFails.Add(sms);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    isHeader = false;
                }
                if (smsFails.Count() > 0)
                {
                    Session["FailList"] = smsFails;
                    Session["DeplicatedMessage"] = "Tổng số sms bị trùng hoặc lỗi định dạng : " + smsFails.Count();
                }

                SuccessMessage = "Tổng số sms được nhập thành công : " + smsSuccess.Count();
                return;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Có lỗi phát sinh trong quá trình upload hoặc chọn data";
            }
        }

        public void ExecImport(SmsImportViewModel model, string filePath)
        {
            try
            {
                List<SMS> smsFails = new List<SMS>();
                List<SMS> smsSuccess = new List<SMS>();
                var path = Path.Combine(Server.MapPath(filePath));
                HSSFWorkbook hssfworkbook;
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    hssfworkbook = new HSSFWorkbook(file);
                }
                ISheet sheet = hssfworkbook.GetSheetAt((int)model.ExcelSheetAt);
                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                bool isHeader = true;
                while (rows.MoveNext())
                {
                    try
                    {
                        if (!isHeader)
                        {
                            IRow row = (HSSFRow)rows.Current;
                            ICell cellMessage = row.GetCell((int)model.ExcelColunmMessage);
                            ICell cellSentDate = row.GetCell((int)model.ExcelColunmSentDate);
                            ICell cellSendingTime = row.GetCell((int)model.ExcelColunmSendingTime);
                            ICell cellSentMinute = row.GetCell((int)model.ExcelColunmSentMinute);

                            SMS sms = new SMS();
                            sms.CreatedByID = SAccount.ID;
                            sms.Message = (cellMessage != null) ? cellMessage.StringCellValue : "";
                            sms.SMSStatus = (short)SMSStatus_vi_VN.Kích_hoạt;
                            sms.ShortCreatedDate = 0;
                            sms.UpdatedByID = SAccount.ID;
                            sms.LibraryID = model.LibraryID;
                            sms.IsFirstMessage = 0;
                            sms.CreatedDate = DateTime.Now;
                            sms.UpdatedDate = DateTime.Now;

                            if (_smsDAO.Insert(sms))
                            {
                                if (model.LibraryID > 0)
                                {
                                    SmsLibrary sl = new SmsLibrary();
                                    sl.LibraryID = model.LibraryID;
                                    sl.SmsID = sms.ID;
                                    sl.Status = SmsLibraryStatus_vi_VN.Kích_hoạt;
                                    sl.SentDate = (cellSentDate != null) ? (int)cellSentDate.NumericCellValue : -1;
                                    sl.SendingTime = (cellSendingTime != null) ? (int)cellSendingTime.NumericCellValue : 0;
                                    sl.SentMinute = (cellSentMinute != null) ? (int)cellSentMinute.NumericCellValue : 0;
                                    _slDAO.Insert(sl);
                                }
                                smsSuccess.Add(sms);
                            }
                            else
                            {
                                smsFails.Add(sms);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    isHeader = false;
                }
                if (smsFails.Count() > 0)
                {
                    Session["FailList"] = smsFails;
                    Session["DeplicatedMessage"] = "Tổng số sms bị trùng hoặc lỗi định dạng : " + smsFails.Count();
                }

                SuccessMessage = "Tổng số sms được nhập thành công : " + smsSuccess.Count();
                return;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Có lỗi phát sinh trong quá trình upload hoặc chọn data";
            }
        }

        MemoryStream GetExcelStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<Patient> PatientList = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("Danh sách người cai nghiện");
            IRow rowheader = sheet1.CreateRow(1);
            rowheader.CreateCell(0).SetCellValue("Họ tên");
            rowheader.CreateCell(1).SetCellValue("Điện thoại");
            rowheader.CreateCell(2).SetCellValue("Tuổi");
            rowheader.CreateCell(3).SetCellValue("Môi trường");
            rowheader.CreateCell(4).SetCellValue("Mức độ");
            rowheader.CreateCell(5).SetCellValue("Trạng thái");

            int i = 2;
            if (PatientList == null)
                return;
            foreach (Patient patient in PatientList)
            {
                int j = 0;
                IRow row = sheet1.CreateRow(i);
                row.CreateCell(j).SetCellValue(patient.PatientName);
                j++;
                row.CreateCell(j).SetCellValue(patient.PhoneNumber);
                j++;
                row.CreateCell(j).SetCellValue(patient.Age);
                j++;
                row.CreateCell(j).SetCellValue(patient.Environment.ToString());
                j++;
                row.CreateCell(j).SetCellValue(patient.PatientStatus.ToString());
                j++;
                i++;
            }
        }

        void InitializeWorkbook(HSSFWorkbook hssfworkbook)
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        //public ActionResult ProcessExport(PatientModel model)
        //{
        //    List<Patient> patientList = _patientDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.PatientName, model.PhoneNumber, model.Age, (int)model.Environment, (int)model.AddictionLevel, model.AccountManagerID, model.FromDate, model.ToDate, model.ShortCreatedDate);
        //    string fileName = "patientlist_" + Guid.NewGuid().ToString() + ".xls";
        //    ExportToExcel(patientList, fileName);
        //    return Redirect("/PinCode");
        //}

        public void ExportToExcel(List<Patient> patientList = null, string fileName = "")
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, patientList);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }

        private ICell GetCell(IRow row, int column)
        {
            ICell cell = row.GetCell(column);
            if (cell == null)
                return row.CreateCell(column);
            return cell;
        }

        //private string ConvertSignToUnSign(string sign)
        //{
        //    try
        //    {
        //        Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
        //        string temp = sign.Normalize(NormalizationForm.FormD);
        //        return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        //    }
        //    catch (Exception)
        //    {
        //        return "error";
        //    }

        //}

        //Xem tin nhan cho SmsInteract
        public ActionResult _ViewSMS(Int32 id)
        {
            var sms = _smsDAO.GetByID(id);
            return View(sms);
        }

        //Search Box
        public JsonResult SearchSMS()
        {
            List<SMS> list = new List<SMS>(_smsDAO.GetAll());
            List<string> tmp = new List<string>();
            foreach (var i in list)
            {
                tmp.Add(i.Message);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _ViewLinkSMS(Int32 SmsID, Int32 SmsInteractID)
        {
            var model = _smsDAO.GetSMSAll();
            ViewBag.SmsID = SmsID;
            ViewBag.SmsInteractID = SmsInteractID;
            return View(model);
        }
    }
}
