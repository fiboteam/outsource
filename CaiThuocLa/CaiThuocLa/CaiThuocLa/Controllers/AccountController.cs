﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using CaiThuocLa.Filters;
using CaiThuocLa.Models;
using CaiThuocLa.DTO;
using CaiThuocLa.DAO;
using CaiThuocLa.Utility;

namespace CaiThuocLa.Controllers
{
    
    public class AccountController : Controller
    {
        public SqlAccountDao _accountDao = new SqlAccountDao();
        public ActionResult Login()
        {
            return View();
        }
       public ActionResult ShowLoginInfo()
        {
            ViewBag.AccountLogin = new Account();
            return View();
        }

       [HttpPost]
       public ActionResult Login(AccountModel model)
       {
           if (ModelState.IsValid)
           {
               Account userLogin = _accountDao.CheckLogin(model.UserName, model.PassWord);
               Session["AccountLogin"] = userLogin;
               return Redirect("/Home");
           }
           ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
           return View(model);
       }

        public ActionResult Logout()
       {
           Session["AccountLogin"] = null;
           return Redirect("/Account/Login");
       }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult ChangePassword()
        {
            if (Session["AccountLogin"] == null)
            {
                return Redirect("/Account/Logout");
            }
            ViewBag.EmployeeLogin = (Account)Session["AccountLogin"];
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            Account accountLogin = null;
            if (Session["AccountLogin"] != null)
            {
                accountLogin = (Account)Session["AccountLogin"];
                if (GFunction.GetMD5(model.CurrentPassword) == accountLogin.PassWord)
                {
                    if (model.NewPassword == model.ConfirmNewPassword)
                    {
                        if (_accountDao.ChangePassword(accountLogin.LoginName, GFunction.GetMD5(model.NewPassword)) > 0)
                        {
                            ViewBag.SuccessMessage = "Mật khẩu đã được cập nhật";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "Đổi mật khẩu thất bại";
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Mật khẩu mới không trùng khớp";
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Mật khẩu hiện tại không trùng khớp";
                }
            }
            ViewBag.EmployeeLogin = accountLogin;
            return View();
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        //internal class ExternalLoginResult : ActionResult
        //{
        //    public ExternalLoginResult(string provider, string returnUrl)
        //    {
        //        Provider = provider;
        //        ReturnUrl = returnUrl;
        //    }

        //    public string Provider { get; private set; }
        //    public string ReturnUrl { get; private set; }

        //    //public override void ExecuteResult(ControllerContext context)
        //    //{
        //    //    OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
        //    //}
        //}

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
