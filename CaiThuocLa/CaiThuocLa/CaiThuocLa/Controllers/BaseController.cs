﻿using CaiThuocLa.DTO;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;

namespace CaiThuocLa.Controllers
{

    public class ExcelResult : ActionResult
    {
        public enum ExportExcellType
        {
            FromFile = 1,
            FromGridView = 2,
            FromWorkbook = 3,
        }

        public string FileName { get; set; }
        public string Path { get; set; }
        public System.Web.UI.WebControls.GridView ExcelGridView { get; set; }
        public HSSFWorkbook Workbook { get; set; }
        private ExportExcellType ExportType { set; get; }

        public ExcelResult(string fileName, string path)
        {
            FileName = fileName;
            Path = path;
            ExportType = ExportExcellType.FromFile;
        }

        public ExcelResult(string fileName, System.Web.UI.WebControls.GridView gridView)
        {
            FileName = fileName;
            ExcelGridView = gridView;
            ExportType = ExportExcellType.FromGridView;
        }

        public ExcelResult(string fileName, HSSFWorkbook workbook)
        {
            FileName = fileName;
            Workbook = workbook;
            ExportType = ExportExcellType.FromWorkbook;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Buffer = true;
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
            context.HttpContext.Response.Charset = System.Text.Encoding.UTF8.BodyName;
            context.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.HttpContext.Response.ContentType = "application/vnd.ms-excel";
            context.HttpContext.Response.ContentEncoding = System.Text.Encoding.UTF8;

            if (ExportType == ExportExcellType.FromFile)
            {
                context.HttpContext.Response.WriteFile(context.HttpContext.Server.MapPath(Path));
            }
            else if (ExportType == ExportExcellType.FromGridView)
            {
                StringWriter sw = new StringWriter();
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                ExcelGridView.RenderControl(htw);
                context.HttpContext.Response.Write(sw.ToString());
            }
            else
            {
                MemoryStream file = new MemoryStream();
                Workbook.Write(file);
                context.HttpContext.Response.BinaryWrite(file.GetBuffer());
            }
            context.HttpContext.Response.End();
        }
    }

    public class BaseController : Controller
    {
        public List<string> NoAutenticatedActions = new List<string>();

        public Account SAccount
        {
            get
            {
                if (Session != null)
                {
                    return (Account)Session["AccountLogin"];
                }
                return null;
            }
            set
            {
                if (value == null)
                    Session.Remove("AccountLogin");
                else
                    Session["AccountLogin"] = value;
            }
        }

        public string SuccessMessage
        {
            get
            {
                if (Session != null)
                {
                    return (string)Session["SuccessMessage"];
                }
                return null;
            }
            set
            {
                if (value == null)
                    Session.Remove("SuccessMessage");
                else
                    Session["SuccessMessage"] = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                if (Session != null)
                {
                    return (string)Session["ErrorMessage"];
                }
                return null;
            }
            set
            {
                if (value == null)
                    Session.Remove("ErrorMessage");
                else
                    Session["ErrorMessage"] = value;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["AccountLogin"] == null)
            {
                if (NoAutenticatedActions.Exists(element => element.ToLower() == filterContext.ActionDescriptor.ActionName.ToLower()))
                    return;
                filterContext.Result = new RedirectResult(Url.Action("Login", "Account"));
            }
        }

        public string ViewContent(string view)
        {
            return ViewContent(view, (string)null);
        }

        public string ViewContent(string view, string controller)
        {
            return ViewContent(view, controller, (object)null);
        }

        public string ViewContent(string view, object model)
        {
            return ViewContent(view, (string)null, model);
        }

        public string ViewContent(string view, string controller, object model)
        {
            if (String.IsNullOrWhiteSpace(controller))
                controller = ControllerContext.RouteData.GetRequiredString("controller");
            ViewDataDictionary viewData = new ViewDataDictionary(ViewData) { Model = model };
            return ViewContent(view, controller, viewData);
        }

        public string ViewContent(string view, string controller, ViewDataDictionary viewData)
        {
            var routeData = new System.Web.Routing.RouteData(RouteData.Route, RouteData.RouteHandler);
            foreach (var item in RouteData.Values)
            {
                routeData.Values.Add(item.Key, item.Value);
            }
            //then overwrite the controller - not the action, though
            routeData.Values["controller"] = controller;

            var controllerContext = new ControllerContext(
              HttpContext,
              routeData,
              this);

            ViewEngineResult result = ViewEngines.Engines.FindView(
              controllerContext,
              view,
              null);

            string content = null;
            if (result.View != null)
            {
                using (StringWriter output = new StringWriter())
                {
                    ViewContext viewContext = new ViewContext(controllerContext, result.View, viewData, TempData, output);

                    viewContext.View.Render(viewContext, output);
                    result.ViewEngine.ReleaseView(ControllerContext, viewContext.View);
                    content = output.ToString();
                }
                return content;
            }
            else
                throw new InvalidOperationException("Can't find view");
        }

        protected string UploadFile(HttpPostedFileBase file, string uploadPath = null, string[] allowExtention = null, long maxSize = 5242880)
        {
            string strMemberID = (SAccount != null) ? SAccount.ID.ToString() : "";
            string strUploads = (uploadPath == null || uploadPath == "") ? "~/Userfile" : "";
            string strName = "";
            if (file != null)
            {
                if (allowExtention == null)
                {
                    allowExtention = new[] { "xls" };
                }
                allowExtention = allowExtention.Where(item => item != null).Select(item => item.ToLower()).ToArray();
                var fileExt = System.IO.Path.GetExtension(file.FileName.ToLower()).Substring(1);
                if (!allowExtention.Contains(fileExt))
                {
                    return "";
                }

                if (file.ContentLength > maxSize)
                {
                    return "";
                }

                if (uploadPath == null)
                {
                    if (allowExtention.Contains(fileExt))
                    {
                        strUploads += "/Image";
                    }
                    else
                    {
                        strUploads += "/File";
                    }
                }
                else
                {
                    strUploads += "/" + uploadPath.Trim().Trim('/');
                }

                if (file.ContentLength > 0)
                {
                    string[] fileName = Path.GetFileName(file.FileName).ToString().Split('.').ToArray();
                    string name = "";
                    for (int i = 0; i < fileName.Count() - 1; i++)
                    {
                        name += fileName[i] + ".";
                    }
                    name = name.Trim('.');
                    int k = 0;
                    string newName = strMemberID + "_" + name;
                    var checkFIleExist = Path.Combine(Server.MapPath(strUploads), newName + "." + fileExt.ToLower());
                    do
                    {
                        if (System.IO.File.Exists(checkFIleExist))
                        {
                            k++;
                            newName = strMemberID + "_" + name + "(" + k + ")";
                            checkFIleExist = Path.Combine(Server.MapPath(strUploads), newName + "." + fileExt.ToLower());
                        }
                    } while (System.IO.File.Exists(checkFIleExist));
                    var path = Path.Combine(Server.MapPath(strUploads), newName + "." + fileExt.ToLower());
                    file.SaveAs(path);
                    strName = newName + "." + fileExt.ToLower();
                }
            }
            return strName;
        }

        protected NameValueCollection CurrentQueryString()
        {
            var request = this.ControllerContext.RequestContext.HttpContext.Request;
            NameValueCollection decodeParameters = HttpUtility.ParseQueryString(request.Url.Query);
            if (request.Form.AllKeys.Count() > 0)
                decodeParameters.Add(request.Form);
            return decodeParameters;
        }

        protected string ConvertSignToUnSign(string sign)
        {
            try
            {
                Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                string temp = sign.Normalize(NormalizationForm.FormD);
                return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            }
            catch (Exception)
            {
                return "-1";
            }

        }

        /// <summary>
        /// Check acc nhan sms
        /// </summary>
        /// <param name="userIn"></param>
        /// <param name="pwdIn"></param>
        /// <returns></returns>
        protected bool CheckReceivedSMS(string userIn, string pwdIn)
        {
            string user = WebConfigurationManager.AppSettings["ReceivedUser"];
            string pwd = WebConfigurationManager.AppSettings["ReceivedPwd"];
            if (String.IsNullOrEmpty(user) || String.IsNullOrEmpty(pwd))
                return false;
            if (user.Equals(userIn) && pwd.Equals(pwdIn))
                return true;
            return false;
        }
    }
}
