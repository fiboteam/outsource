﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class SmsCronJobController : BaseController
    {
        //
        // GET: /SmsCronJob/
        private SqlSmsCronJobDao _sqlSmsCronJobDAO = new SqlSmsCronJobDao();
        public SqlLibraryDao _libraryDAO = new SqlLibraryDao();

        public ActionResult Index(SmsCronJobModel model, string message, string phonenumber)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;

            if (message == null)
            {
                message = "";
            }

            if(phonenumber==null)
            {
                phonenumber = "";
            }
            string mess = ConvertSignToUnSign(message);

            model.DataList = _sqlSmsCronJobDAO.GetWithFilter(
                    model.Page.PageSize,
                    model.Page.CurrentPage,
                    model.LibraryID,
                    (short)model.SmsCronJobStatus,
                    mess,
                    phonenumber
                );

            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Page.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Page.TotalRecords = 0;

            }

            List<Library> libraryList = _libraryDAO.GetList();
            /*
             * Load combobox library search box
            */
            Library all = new Library();
            all.ID = 0;
            all.LibraryName = "Tất cả";
            libraryList.Insert(0, all);

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

    }
}
