﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class LibraryController : BaseController
    {
        //
        // GET: /Library/
        public SqlLibraryDao _libraryDao = new SqlLibraryDao();
        public SqlSMSDao _smsDao = new SqlSMSDao();
        public SqlSmsLibraryDao _slDao = new SqlSmsLibraryDao();

        public ActionResult Index(LibraryModel model)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;
            /*
             * Load list patient with filter
            */
            model.DataList = _libraryDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.LibraryName, (int)model.LibraryStatus,(int)model.LibraryType);

            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Page.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Page.TotalRecords = 0;

            }
            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(LibraryModel model)
        {
            Library lib = new Library();
            lib.LibraryName = model.LibraryName;
            lib.LibraryStatus = Utility.LibraryStatus_vi_VN.Đã_kích_hoạt;
            lib.LibraryType = model.LibraryType;
            lib.NumberOfSMS = 0;
            lib.CreatedByID = SAccount.ID;
            lib.UpdatedByID = SAccount.ID;
            lib.CreatedDate = DateTime.Now;
            lib.UpdatedDate = DateTime.Now;
            if(_libraryDao.Insert(lib))
            {
                /* Thêm 2 tin nhắn đầu tiên vào  */
                string message1 = WebConfigurationManager.AppSettings["FirstWelcomeMessage"];
                string message2 = WebConfigurationManager.AppSettings["SecondWelcomeMessage"];
                string keyword1 = WebConfigurationManager.AppSettings["Keyword1"];
                string keyword2 = WebConfigurationManager.AppSettings["Keyword2"];
                string keyword3 = WebConfigurationManager.AppSettings["Keyword3"];
                string replyMessage1 = WebConfigurationManager.AppSettings["FirstReplyMessage1"];
                string replyMessage2 = WebConfigurationManager.AppSettings["FirstReplyMessage2"];
                string replyMessage3 = WebConfigurationManager.AppSettings["FirstReplyMessage3"];
                if (_smsDao.AddFirstSms(lib.ID,SAccount.ID,message1,message2,keyword1,keyword2,keyword3,replyMessage1,replyMessage2,replyMessage3) > 0)
                {
                    SuccessMessage = "Đã thêm thành công";
                }
            }
            else
            {
                ErrorMessage = "Thêm thất bại";
            }
            return Redirect("/Library");
        }

        public ActionResult Edit(long lid)
        {
            Library lib = new Library();
            lib = _libraryDao.GetSingle(lid);
            LibraryModel model = new LibraryModel();
            model.LibraryName = lib.LibraryName;
            model.LibraryStatus = lib.LibraryStatus;
            model.ID = lib.ID; 
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(LibraryModel model)
        {
            Library lib = new Library();
            lib.LibraryName = model.LibraryName;
            lib.LibraryStatus = model.LibraryStatus;
            lib.LibraryType = model.LibraryType;
            lib.ID = model.ID;
            if (_libraryDao.Update(lib))
            {
                SuccessMessage = "Đã cập nhật thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            return Redirect("/Library");
        }

        public ActionResult Delete(long lid)
        {
            if(_libraryDao.UpdateStatus(lid,(int)LibraryStatus_vi_VN.Đã_xóa) > 0)
            {
                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/Library");
        }

        public ActionResult AddSms(long lid)
        {
            SmsLibraryModel model = new SmsLibraryModel();
            Library lib = new Library();
            lib = _libraryDao.GetSingle(lid);
            List<SMS> smsList = _smsDao.GetListNotExistsInLibrary(lid);
            ViewBag.SmsList = new SelectList(smsList, "ID", "Message");
            ViewBag.Library = lib;
            model.LibraryID = lid;
            return View(model);
        }

        [HttpPost]
        public ActionResult AjaxAddSms(long smsId,long libraryid,int sentDate, int sendingTime,int sentMinute)
        {
            try
            {
                List<SmsLibrary> slExists = _slDao.GetListInLibrary(libraryid);
                if(slExists.Where(m => m.SmsID == smsId).FirstOrDefault() != null)
                {
                    return Content("Tin nhắn đã tồn tại trong thư viện");
                }
                if(sendingTime < 0 || sendingTime > 23)
                {
                    return Content("Giờ gửi không hợp lệ");
                }
                if(sentDate < 0)
                {
                    return Content("Ngày giờ gửi không hợp lệ");
                }
                if(sendingTime <= 0 || sendingTime > 24)
                {
                    return Content("Ngày giờ gửi không hợp lệ");
                }
                if(sentMinute <= 0 || sentMinute > 60)
                {
                    return Content("Ngày giờ gửi không hợp lệ");
                }
                SmsLibrary sl = new SmsLibrary();
                sl.SmsID = smsId;
                sl.LibraryID = libraryid;
                sl.SentDate = sentDate;
                sl.SendingTime = sendingTime;
                sl.SentMinute = sentMinute;
                sl.Status = SmsLibraryStatus_vi_VN.Kích_hoạt;
                if (_slDao.Insert(sl))
                    return Content("Đã thêm thành công");
                return Content("Thêm thất bại");
            }
            catch(Exception ex){ }
            return Content("Lỗi Ajax");
        }
        
        public ActionResult ViewSmsInLibrary(long lid)
        {
            SmsLibraryModel model = new SmsLibraryModel();
            model.DataList = _slDao.GetListInLibrary(lid);
            return View(model);
        }

        [HttpPost]
        public ActionResult AjaxDeleteSms(long slid)
        {
            if (_slDao.DeleteSms(slid) > 0)
                return Content("Đã xóa thành công");
            return Content("Xóa thất bại");
        }

        #region Local Function
        public int CountSendingTime(int value,string timeunit)
        {
            int returnVal = value;
            switch(timeunit)
            {
                case "Ngày":
                    returnVal = value * 24; /* 24h / day */
                    break;
                case "Giờ":
                    returnVal = value;
                    break;
                case "Tháng":
                    returnVal = value * 24 * 30;
                    break;
                case "Tuần":
                    returnVal = value * 24 * 7;
                    break;
                case "Năm":
                    returnVal = value * 24 * 365;
                    break;
                default:
                    returnVal = value;
                    break;
            }
            return returnVal;
        }
        #endregion

        public ActionResult ImportSms(long lid)
        {
            SmsImportViewModel model = new SmsImportViewModel();
            model.LibraryID = lid;
            return View(model);
        }
        public ActionResult Active(long lid)
        {
            if(_libraryDao.ActiveLibrary(lid) > 0)
            {
                SuccessMessage = "Thư viện đã được kích hoạt. Vui lòng xem chi tiết trong lịch gửi tin";
            }
            else
            {
                ErrorMessage = "Kích hoạt thất bại";
            }
            return Redirect("/SmsCronJob/?libraryid=" + lid);
        }
    }
}
