﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using CaiThuocLa.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using Excel;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;

namespace CaiThuocLa.Controllers
{
    public class SmsInteractController : BaseController
    {
        public SmsInteractController()
        {
            NoAutenticatedActions.Add("ReceivedSMS");
        }


        SqlSMSDao _smsDao = new SqlSMSDao();
        SqlSmsCronJobDao _smsCronJob = new SqlSmsCronJobDao();
        SqlSmsInteractDao _smsInteract = new SqlSmsInteractDao();
        SqlSmsInteractLogDao _smsInteractLog = new SqlSmsInteractLogDao();
        SqlPatientDao _patient = new SqlPatientDao();

        //
        // GET: /SmsInteract/

        public ActionResult Index(SmsInteractModel model)
        {
            if (model.CurrentPage != 0)
                model.Paging.CurrentPage = model.CurrentPage;
            else
                model.Paging.CurrentPage = 1;

            model.DataList = _smsInteract.GetWithFilter(
                                (int)model.Paging.PageSize,
                                (int)model.Paging.CurrentPage,
                                model.Keyword,
                                model.ReplyMessage,
                                (short)model.SmsInteractStatus,
                                model.ContentSMS
                                );

            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Paging.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Paging.TotalRecords = 0;
            }

            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(SmsInteractModel model)
        {
            SmsInteract item = new SmsInteract();
            item.AssignProperties(model);
            item.CreatedDate = DateTime.Now;
            item.UpdatedDate = item.CreatedDate;

            if (_smsInteract.Insert(item))
            {
                SuccessMessage = "Đã nhập thông tin khảo sát thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            return Redirect("/SmsInteract");
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            SmsInteract item = _smsInteract.GetByID(id);
            SmsInteractModel model = new SmsInteractModel();
            model.AssignProperties(item);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(SmsInteractModel model)
        {
            SmsInteract item = _smsInteract.GetByID(model.ID);
            item.AssignProperties(model);
            item.UpdatedDate = DateTime.Now;

            if (_smsInteract.Update(item))
            {
                SuccessMessage = "Đã cập nhật thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }

            return Redirect("/SmsInteract");
        }

        public ActionResult Delete(Int64 id)
        {
            SmsInteract item = _smsInteract.GetByID(id);
            item.SmsInteractStatus = SmsInteractStatus.Đã_Xóa;
            if (_smsInteract.Update(item))
            {
                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/SmsInteract");
        }

        public ActionResult ReceivedSMS(string userName, string password, string phoneNumber, string content, string guid)
       {
            try
            {
                if (!CheckReceivedSMS(userName, password)) return Content("Fail");
                if (String.IsNullOrEmpty(guid) || String.IsNullOrEmpty(phoneNumber)) return Content("Fail");

                string result = String.Empty;

                if (!String.IsNullOrEmpty(content))
                {
                    var byteContent = Convert.FromBase64String(content);
                    content = Encoding.UTF8.GetString(byteContent);
                }

                Tools.AddReceivedSMS(content, phoneNumber, guid);

                if (Regex.IsMatch(phoneNumber.Trim(), @"^[\+0-9]{1}[0-9]{8,20}$") && (phoneNumber.StartsWith("84") || phoneNumber.StartsWith("0")))
                {
                    //Nếu có nhiều Keyword trùng. Thì tìm trong bảng SmsCronJob để lấy được SmsID vừa gửi cho Patitent
                    //Từ đó suy ra được Patitent đang nhắn Keyword nào
                    if (phoneNumber.StartsWith("84"))
                        phoneNumber = "0" + phoneNumber.Substring(2);

                    //Kiểm tra xem có phải thành viên không
                    var patient = _patient.GetByColumnName("PhoneNumber", phoneNumber).FirstOrDefault(p => p.PatientStatus != PatientStatus_vi_VN.Đã_xóa);

                    if (patient == null) //Kiểm tra lại số điện thoại của Patient đã đăng ký
                    {
                        result = WebConfigurationManager.AppSettings["PatientNotFound"];
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(content))
                        {
                            result = WebConfigurationManager.AppSettings["ErrorSyntax"];
                        }
                        else
                        {
                            var smsInteract = new List<SmsInteract>(_smsInteract.GetByColumnName("Keyword", content)).Where(p => p.SmsInteractStatus == SmsInteractStatus.Kích_Hoạt).ToList();

                            if (smsInteract != null && smsInteract.Count > 0)
                            {
                                SmsInteract item = null;
                                if (smsInteract.Count == 1) //Nếu từ khóa là duy nhất thì không cần duyệt trong bảng khác
                                    item = smsInteract[0];
                                else
                                {

                                    long relateID = 0;
                                    DateTime smsReceivedDate = new DateTime(0);
                                    TypeRelateID typeRelate = TypeRelateID.Không_liên_kết;

                                    var listSmsInteractSent = _smsInteract.GetSmsInteractID(phoneNumber);
                                    var listSmsCronJob = _smsCronJob.GetSmsByPatitent(phoneNumber);

                                    SmsInteract itemSmsInteract = null;
                                    SmsCronJob itemSmsCronJob = null;

                                    if (listSmsInteractSent != null && listSmsInteractSent.Count > 0)
                                    {
                                        itemSmsInteract = listSmsInteractSent[0];
                                    }

                                    if (listSmsCronJob != null && listSmsCronJob.Count > 0)
                                    {
                                        itemSmsCronJob = listSmsCronJob[0];
                                    }

                                    //So sánh 2 bảng nếu bảng nào có keyword đó và ngày mới nhất
                                    if (itemSmsInteract == null && itemSmsCronJob == null)
                                    {
                                        result = WebConfigurationManager.AppSettings["CronJobNotFound"];
                                    }
                                    else if (itemSmsInteract == null)
                                    {
                                        relateID = itemSmsCronJob.SmsID;
                                        smsReceivedDate = itemSmsCronJob.ExecutionTime;
                                        typeRelate = TypeRelateID.Tin_nhắn;
                                    }
                                    else if (itemSmsCronJob == null)
                                    {
                                        relateID = itemSmsInteract.ID;
                                        smsReceivedDate = DateTime.Parse(itemSmsInteract.ExtentionProperty["LogReceivedDate"].ToString());
                                        typeRelate = TypeRelateID.Từ_khóa;
                                    }
                                    else
                                    {
                                        DateTime tmpDateTime = DateTime.Parse(itemSmsInteract.ExtentionProperty["LogReceivedDate"].ToString());
                                        if (tmpDateTime.CompareTo(itemSmsCronJob.ExecutionTime) >= 0)
                                        {
                                            relateID = itemSmsInteract.ID;
                                            smsReceivedDate = tmpDateTime;
                                            typeRelate = TypeRelateID.Từ_khóa;
                                        }
                                        else
                                        {
                                            relateID = itemSmsCronJob.SmsID;
                                            smsReceivedDate = itemSmsCronJob.ExecutionTime;
                                            typeRelate = TypeRelateID.Tin_nhắn;
                                        }
                                    }

                                    if (relateID > 0)
                                    {
                                        TimeSpan timeReal = DateTime.Now - smsReceivedDate;
                                        int timeValid = 1;
                                        int.TryParse(WebConfigurationManager.AppSettings["TimeAnswerSms"], out timeValid);

                                        if (timeReal.TotalHours <= timeValid)
                                        {
                                            item = smsInteract.FirstOrDefault(p => p.SmsID == relateID && p.TypeRelateID == typeRelate);

                                            if (item == null) //Không tìm thấy Keyword với SmsID này
                                            {
                                                result = WebConfigurationManager.AppSettings["KeywordAnswerWrong"];
                                            }
                                        }
                                        else
                                        {
                                            result = WebConfigurationManager.AppSettings["InvalidTime"];
                                        }
                                    }
                                    else
                                    {
                                        result = WebConfigurationManager.AppSettings["CronJobNotFound"];
                                    }
                                }

                                if (item != null)
                                {
                                    if (item.SmsInteractStatus != SmsInteractStatus.Kích_Hoạt)
                                    {
                                        result = WebConfigurationManager.AppSettings["InteractDeleted"];
                                    }
                                    else
                                    {
                                        result = item.ReplyMessage;
                                        DateTime timeNow = DateTime.Now;
                                        SmsInteractLog smsInteractLog = new SmsInteractLog()
                                        {
                                            SmsInteractID = smsInteract[0].ID,
                                            PhoneNumber = phoneNumber,
                                            CreatedDate = timeNow,
                                            UpdatedDate = timeNow,
                                            ShortCreatedDate = int.Parse(timeNow.ToString("yyyyMMdd"))
                                        };
                                        _smsInteractLog.Insert(smsInteractLog);

                                        //Kiểm tra dừng nhận tin nhắn tự động
                                        string[] StopKey = WebConfigurationManager.AppSettings["StopReceivedSMS"].Split('|');

                                        for (int i = 0; i < StopKey.Length; i++)
                                        {
                                            StopKey[i] = StopKey[i].ToLower();
                                        }

                                        if (StopKey.Contains(item.Keyword.ToLower()))
                                        {
                                            if (patient != null)
                                            {
                                                if (_patient.UpdateStatus(patient.ID, (int)PatientStatus_vi_VN.Tạm_dừng_nhận_tin) <= 0)
                                                {
                                                    result = WebConfigurationManager.AppSettings["StopReceivedSMSFail"];
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(result))
                                        result = WebConfigurationManager.AppSettings["NotFound"];
                                }
                            }
                            else
                            {
                                result = WebConfigurationManager.AppSettings["NotFound"];
                            }
                        }
                    }

                    int code = Tools.SendSMSToUser(phoneNumber, result, Guid.NewGuid().ToString());
                    if (code == 500) return Content("Fail");
                }
            }
            catch(Exception ex)
            {
                return Content("Message: " + ex.Message + " --- " + ex.ToString());
            }

            return Content("OK");
        }

        [HttpGet]
        public ActionResult ImportFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ImportFile(HttpPostedFileBase importFile, string keywordIndex = "A", string replyMessageIndex = "B", string smsIndex = "D", string typeRelateIDIndex = "E", int sheetIndex = 0, bool firstRowIsColumnNames = false)
        {
            ReadExcel(importFile, keywordIndex, replyMessageIndex, smsIndex, typeRelateIDIndex, sheetIndex, firstRowIsColumnNames);
            return Redirect("/SmsInteract");
        }

        private void ReadExcel(HttpPostedFileBase importFile, string keywordIndex, string replyMessageIndex, string smsIndex, string typeRelateIDIndex, int sheetIndex, bool firstRowIsColumnNames)
        {
            try
            {
                int success = 0;
                //int hasItem = 0;
                int failItem = 0;

                IExcelDataReader reader;

                if (importFile == null || importFile.ContentLength <= 0)
                {
                    ErrorMessage = "File nhập vào không hợp lệ";
                    return;
                }

                if(importFile.ContentLength > (10 * 1024 * 1024))
                {
                    ErrorMessage = "File vượt quá 10MB";
                    return;
                }

                if (importFile.FileName.EndsWith(".xlsx"))
                    reader = ExcelReaderFactory.CreateOpenXmlReader(importFile.InputStream);
                else if (importFile.FileName.EndsWith(".xls"))
                    reader = ExcelReaderFactory.CreateBinaryReader(importFile.InputStream);
                else
                {
                    ErrorMessage = "Định dạng File không hợp lệ. (*.xls|*.xlsx)";
                    return;
                }

                reader.IsFirstRowAsColumnNames = firstRowIsColumnNames;
                DataSet ds = reader.AsDataSet();
                reader.Close();

                if (ds.Tables.Count <= 0)
                {
                    ErrorMessage = "Không đọc được dữ liệu trong File";
                    return;
                }

                string arrCol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                DataTable dt = ds.Tables[sheetIndex];

                keywordIndex = keywordIndex.ToUpper();
                replyMessageIndex = replyMessageIndex.ToUpper();
                smsIndex = smsIndex.ToUpper();

                foreach (DataRow i in dt.Rows)
                {
                    try
                    {
                        string key = i[arrCol.IndexOf(keywordIndex)].ToString();
                        string message = i[arrCol.IndexOf(replyMessageIndex)].ToString();
                        int status = 1;

                        int smsid = 0;
                        int typeRelateID = 0;

                        try
                        {
                            int.TryParse(i[arrCol.IndexOf(smsIndex)].ToString(), out smsid);
                            int.TryParse(i[arrCol.IndexOf(typeRelateIDIndex)].ToString(), out typeRelateID);
                        }
                        catch { }

                        //var check = new List<SmsInteract>(_smsInteract.GetByColumnName("Keyword", key));
                        //if (check != null && check.Count > 0)
                        //{
                        //    hasItem++;
                        //    continue;
                        //}

                        DateTime timeNow = DateTime.Now;
                        SmsInteract item = new SmsInteract()
                        {
                            Keyword = key,
                            SmsInteractStatus = (SmsInteractStatus)Enum.Parse(typeof(SmsInteractStatus), status.ToString()),
                            ReplyMessage = message,
                            SmsID = smsid,
                            TypeRelateID = (TypeRelateID)Enum.Parse(typeof(TypeRelateID), typeRelateID.ToString()),
                            CreatedDate = timeNow,
                            UpdatedDate = timeNow
                        };

                        if (_smsInteract.Insert(item))
                            success++;
                        else
                            failItem++;
                    }
                    catch
                    {
                        failItem++;
                    }
                }

                SuccessMessage = "Nhập dữ liệu từ File: Tổng: " + dt.Rows.Count.ToString()
                                + " - Thành công: " + success.ToString()
                                + " - Thất bại: " + failItem.ToString();
                                //+ " - Trùng: " + hasItem.ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = "Nhập dữ liệu từ File không thành công";
            }
        }

        [HttpPost]
        public ActionResult UpdateSmsID(Int32 SmsID, Int32 SmsInteractID)
        {
            var sms = _smsDao.GetByID(SmsID);
            var smsInteract = _smsInteract.GetByID(SmsInteractID);

            if (sms != null && smsInteract !=null)
            {
                smsInteract.SmsID = (Int32)sms.ID;
                if (_smsInteract.Update(smsInteract))
                {
                    SuccessMessage = "Cập nhật Câu hỏi liên kết thành công";
                    return Redirect("/SmsInteract");
                }
            }
            ErrorMessage = "Cập nhật câu hỏi liên kết thất bại";
            return Redirect("/SmsInteract");
        }

        //Xem tin nhan cho SmsInteract
        public ActionResult _ViewSmsInteractRelate(Int32 id)
        {
            var sms = _smsInteract.GetByID(id);
            return View(sms);
        }
    }
}
