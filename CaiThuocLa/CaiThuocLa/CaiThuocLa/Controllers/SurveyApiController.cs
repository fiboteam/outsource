﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CaiThuocLa.Controllers
{
    public class SurveyApiController : ApiController
    {
        SqlPatientDao _patient = new SqlPatientDao();

        [HttpGet]
        public ExtenalModel CheckForSendQuestion(string phoneNumber)
        {
            ExtenalModel model = new ExtenalModel();
            List<Patient> patienList = new List<Patient>(_patient.GetByColumnName("PhoneNumber", phoneNumber));
            if (patienList.Count > 0)
            {
                model.Allow = true;
                model.Exception = String.Empty;
            }
            else
            {
                model.Allow = false;
                model.Exception = "Số điện thoại này không có trong dang sách bệnh nhân.";
            }
            return model;
        }
    }
}
