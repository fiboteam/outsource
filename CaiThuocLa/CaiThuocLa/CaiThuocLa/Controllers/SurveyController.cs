﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using CaiThuocLa.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Text.RegularExpressions;
using System.Text;

namespace CaiThuocLa.Controllers
{
    public class SurveyController : BaseController
    {
        public SurveyController()
        {
            NoAutenticatedActions.Add("ReceivedSMS");
        }

        public SqlSurveyDao _survey = new SqlSurveyDao();
        public SqlSurveyQuestionDao _surveyQuestion = new SqlSurveyQuestionDao();
        SqlPatientDao _patient = new SqlPatientDao();

        //
        // GET: /Survey/

        public ActionResult Index(SurveyModel model)
        {
            if (model.CurrentPage != 0)
                model.Paging.CurrentPage = model.CurrentPage;
            else
                model.Paging.CurrentPage = 1;

            model.DataList = _survey.GetWithFilter(
                                (int)model.Paging.PageSize,
                                (int)model.Paging.CurrentPage,
                                model.SurveyName,
                                model.SurveyKeyword,
                                (short)model.Status
                                );
            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Paging.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Paging.TotalRecords = 0;

            }

            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(SurveyModel model)
        {
            Survey item = new Survey();
            item.AssignProperties(model);
            item.CreatedDate = DateTime.Now;
            item.UpdatedDate = item.CreatedDate;

            if (_survey.Insert(item))
            {
                SuccessMessage = "Đã nhập thông tin khảo sát thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            return Redirect("/Survey");
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            Survey item = _survey.GetByID(id);
            SurveyModel model = new SurveyModel();
            model.AssignProperties(item);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(SurveyModel model)
        {
            Survey item = _survey.GetByID(model.ID);
            item.AssignProperties(model);
            item.UpdatedDate = DateTime.Now;

            if (_survey.Update(item))
            {
                SuccessMessage = "Đã cập nhật thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }

            return Redirect("/Survey");
        }

        public ActionResult Delete(Int64 id)
        {
            Survey item = _survey.GetByID(id);
            item.Status = SurveyStatus_vi_VN.Đã_xóa;
            if (_survey.Update(item))
            {
                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/Survey");
        }

        [HttpGet]
        public ActionResult SendQuestion(Int64 id, string surveyName)
        {
            SurveyExtenalModel model = new SurveyExtenalModel();
            model.SurveyID = id;
            model.SurveyName = surveyName;
            model.QuestionList = new List<SurveyQuestion>(_surveyQuestion.GetByColumnName("SurveyID", model.SurveyID).Where(p => p.Status == SurveyQuestionStatus.Kích_hoạt));
            return View(model);
        }

        [HttpPost]
        public ActionResult SendQuestion(SurveyExtenalModel model)
        {
            return Redirect("/Survey");
        }

        /// <summary>
        /// Nhận SMS
        /// </summary>
        /// <returns></returns>
        public ActionResult ReceivedSMS(string userName, string password, string phoneNumber, string content, string guid)
        {
            try
            {
                if (!CheckReceivedSMS(userName, password)) return Content("Fail");
                if (String.IsNullOrEmpty(guid) || String.IsNullOrEmpty(phoneNumber)) return Content("Fail");

                string result = String.Empty;
                string[] arrResult;

                if (!String.IsNullOrEmpty(content))
                {
                    var byteContent = Convert.FromBase64String(content);
                    content = Encoding.UTF8.GetString(byteContent);
                }

                Tools.AddReceivedSMS(content, phoneNumber, guid);

                content = content.ToUpper();

                if (Regex.IsMatch(phoneNumber.Trim(), @"^[\+0-9]{1}[0-9]{8,20}$") && (phoneNumber.StartsWith("84") || phoneNumber.StartsWith("0")))
                {

                    if (phoneNumber.StartsWith("84"))
                        phoneNumber = "0" + phoneNumber.Substring(2);

                    //Kiểm tra xem có phải thành viên không
                    var patient = 1; //_patient.GetPatientByPhone(phoneNumber);

                    if (patient == null) //Kiểm tra lại số điện thoại của Patient đã đăng ký
                    {
                        result = WebConfigurationManager.AppSettings["PatientNotFound"];
                    }
                    else
                    {
                        List<string> parms = content.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        short timeOut = 24;
                        short.TryParse(WebConfigurationManager.AppSettings["TimeOutReceived"], out timeOut);

                        if (parms.Count > 1)
                        {
                            string[] SurveySyntax = WebConfigurationManager.AppSettings["SurveySyntax"].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                            if (SurveySyntax.Contains(parms[0]))
                            {
                                var survey = new List<Survey>(_survey.GetByColumnName("SurveyKeyword", parms[1]));
                                if (survey != null && survey.Count == 1)
                                {
                                    arrResult = _survey.v2_CheckSurveyNew(survey[0].ID, phoneNumber, timeOut).Split('|');
                                    if (arrResult[0].Equals("true"))
                                    {
                                        if (arrResult.Length > 1)
                                        {
                                            //Tools.SendSMSToUser(phoneNumber, arrResult[1], Guid.NewGuid().ToString());
                                        }

                                        var surveyQuestion = new List<SurveyQuestion>(_surveyQuestion.GetByColumnName("SurveyID", survey[0].ID));
                                        int countQuestion = 0;
                                        if (surveyQuestion != null)
                                            countQuestion = surveyQuestion.Count(p => p.Status == SurveyQuestionStatus.Kích_hoạt);

                                        string messageJoinSurvey = WebConfigurationManager.AppSettings["JoinSurvey"]
                                                                    .Replace("@Name", survey[0].SurveyKeyword)
                                                                    .Replace("@Count", countQuestion.ToString());
                                        Tools.SendSMSToUser(phoneNumber, messageJoinSurvey, Guid.NewGuid().ToString());
                                        result = _survey.v2_GetContinueQuestion(survey[0].ID, phoneNumber, true, 0);
                                    }
                                    else if (arrResult[0].Equals("false"))
                                    {
                                        result = arrResult[1];
                                    }
                                }
                                else
                                {
                                    string answers = "";
                                    for (int i = 1; i < parms.Count; i++)
                                    {
                                        answers += parms[i] + " ";
                                    }

                                    answers = answers.Trim();

                                    arrResult = _survey.v2_AnswersQuestion(phoneNumber, answers, timeOut).Split('|');
                                    if (arrResult[0].Equals("false"))
                                    {
                                        result = arrResult[1];
                                    }
                                    else if (arrResult[0].Equals("true"))
                                    {
                                        result = _survey.v2_GetContinueQuestion(0, phoneNumber, false, long.Parse(arrResult[1]));
                                    }
                                }
                            }
                            else
                            {
                                result = WebConfigurationManager.AppSettings["NotFound"];
                            }
                        }
                        else
                        {
                            result = WebConfigurationManager.AppSettings["ErrorSyntax"];
                        }
                    }

                    int codeFinish = Tools.SendSMSToUser(phoneNumber, result, Guid.NewGuid().ToString());
                    if (codeFinish == 500) return Content("Fail");
                }
            }
            catch(Exception ex)
            {
                return Content("Message: " + ex.Message + " --- " + ex.ToString());
            }

            return Content("OK");
        }
    }
}
