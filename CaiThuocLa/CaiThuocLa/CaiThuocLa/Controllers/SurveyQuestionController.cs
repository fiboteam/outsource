﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class SurveyQuestionController : BaseController
    {
        public SqlSurveyQuestionDao _surveyQuestion = new SqlSurveyQuestionDao();
        public SqlSurveyDao _survey = new SqlSurveyDao();

        //
        // GET: /Survey/

        public ActionResult Index(SurveyQuestionModel model)
        {
            if (model.CurrentPage != 0)
                model.Paging.CurrentPage = model.CurrentPage;
            else
                model.Paging.CurrentPage = 1;

            model.DataList = _surveyQuestion.GetWithFilter(
                                (int)model.Paging.PageSize,
                                (int)model.Paging.CurrentPage,
                                model.Question,
                                model.AnswersSearch,
                                model.SurveyID,
                                (short)model.Status
                                );
            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Paging.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Paging.TotalRecords = 0;

            }

            List<Survey> surveyList = new List<Survey>(_survey.GetAll());
            Survey surveyAll = new Survey();
            surveyAll.ID = -1;
            surveyAll.SurveyName = "Tất cả";
            surveyList.Add(surveyAll);
            ViewBag.SurveyList = new SelectList(surveyList, "ID", "SurveyName");

            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        public ActionResult _SurveyQuestionPatial(Int64 pid)
        {
            var list = new List<SurveyQuestion>(_surveyQuestion.GetBySurveyID(pid));
            return View(list);
        }

        [HttpGet]
        public ActionResult Add()
        {
            SurveyQuestionModel model = new SurveyQuestionModel();
            List<Survey> surveyList = new List<Survey>(_survey.GetAll());
            ViewBag.SurveyList = new SelectList(surveyList, "ID", "SurveyName");
            return View(model);
        }

        [HttpGet]
        public ActionResult AddPatial(Int64 pid)
        {
            SurveyQuestionModel model = new SurveyQuestionModel();
            model.SurveyID = pid;
            model.returnUrl = "/Survey";
            List<Survey> surveyList = new List<Survey>(_survey.GetAll());
            ViewBag.SurveyList = new SelectList(surveyList, "ID", "SurveyName");
            return View("Add", model);
        }

        [HttpPost]
        public ActionResult Add(SurveyQuestionModel model)
        {
            SurveyQuestion item = new SurveyQuestion();
            item.AssignProperties(model);
            item.CreatedDate = DateTime.Now;
            item.UpdatedDate = item.CreatedDate;

            if (_surveyQuestion.Insert(item))
            {
                SuccessMessage = "Đã nhập thông tin khảo sát thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            if (String.IsNullOrEmpty(model.returnUrl))
                return Redirect("/SurveyQuestion");
            return Redirect(model.returnUrl);
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            SurveyQuestion item = _surveyQuestion.GetByID(id);
            SurveyQuestionModel model = new SurveyQuestionModel();
            model.AssignProperties(item);
            List<Survey> surveyList = new List<Survey>(_survey.GetAll());
            ViewBag.SurveyList = new SelectList(surveyList, "ID", "SurveyName");
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(SurveyQuestionModel model)
        {
            SurveyQuestion item = new SurveyQuestion();
            item.AssignProperties(model);
            item.UpdatedDate = DateTime.Now;

            if (_surveyQuestion.Update(item))
            {
                SuccessMessage = "Đã cập nhật thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }

            if (String.IsNullOrEmpty(model.returnUrl))
                return Redirect("/SurveyQuestion");
            return Redirect(model.returnUrl);
        }

        [HttpGet]
        public ActionResult EditPatial(Int64 id)
        {
            SurveyQuestion item = _surveyQuestion.GetByID(id);
            SurveyQuestionModel model = new SurveyQuestionModel();
            model.returnUrl = "/Survey";
            model.AssignProperties(item);
            List<Survey> surveyList = new List<Survey>(_survey.GetAll());
            ViewBag.SurveyList = new SelectList(surveyList, "ID", "SurveyName");
            return View("Edit", model);
        }

        public ActionResult Delete(Int64 id)
        {
            SurveyQuestion item = _surveyQuestion.GetByID(id);
            item.Status = SurveyQuestionStatus.Đã_xóa;

            if (_surveyQuestion.Update(item))
            {
                Survey survey = _survey.GetByID(item.SurveyID);
                survey.TotalQuestion--;
                _survey.Update(survey);

                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/SurveyQuestion");
        }

        public ActionResult DeletePatial(Int64 id)
        {
            SurveyQuestion item = _surveyQuestion.GetByID(id);
            item.Status = SurveyQuestionStatus.Đã_xóa;
            if (_surveyQuestion.Update(item))
            {
                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/Survey");
        }
    }
}
