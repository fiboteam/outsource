﻿using CaiThuocLa.DAO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class HomeController : BaseController
    {
        private SqlSendSMSDao _sendSmsDao = new SqlSendSMSDao();
        private SqlPatientDao _patientDao = new SqlPatientDao();

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CountAllSmsSent()
        {
            int totalsms = _sendSmsDao.CountAllSmsSent();
            return Content(totalsms.ToString());
        }

        public ActionResult CountAllPatient()
        {
            int totalpatient = _patientDao.CountAllPatient();
            return Content(totalpatient.ToString());
        }
        //public ActionResult TestEncript()
        //{
        //    string Password = "F1qRyNF1R4h0b7GxW_e3";
        //    string encript = GFunction.En
        //}
    }
}
