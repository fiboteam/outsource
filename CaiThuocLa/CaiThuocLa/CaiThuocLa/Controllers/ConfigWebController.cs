﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Configuration;
using System.Xml;
using System.Text;

namespace CaiThuocLa.Controllers
{
    public class ConfigWebController : BaseController
    {
        //
        // GET: /ConfigWeb/

        public ActionResult Index()
        {
            Dictionary<string[], string> model = new Dictionary<string[], string>();
            try
            {
                string[] listEdit = WebConfigurationManager.AppSettings["ListEditConfig"].Split('|');

                foreach (var i in listEdit)
                {
                    string[] parms = i.Split(':');
                    model.Add(new string[] { parms[0].Trim(), WebConfigurationManager.AppSettings[parms[0].Trim()] }, parms[1]);
                }

                return View(model);
            }
            catch (Exception)
            {
                ErrorMessage = "Lỗi: Không lấy được danh sách cấu hình";
                model = new Dictionary<string[], string>();
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult _Edit(string Type)
        {
            ViewBag.Content = WebConfigurationManager.AppSettings[Type];
            object model = Type;
            return View("_Edit", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _Edit(string Type, string Content)
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("/");
                string oldValue = config.AppSettings.Settings[Type].Value;
                config.AppSettings.Settings[Type].Value = Content;
                config.Save(ConfigurationSaveMode.Modified);
                SuccessMessage = "Lưu cấu hình thành công";
            }
            catch (Exception ex)
            {
                ErrorMessage = "Lưu cấu hình thất bại";
            }
            return Redirect("/ConfigWeb");
        }
    }
}
