﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class AssortPatientController : BaseController
    {
        public SqlAssortPatientDao _assortPatient = new SqlAssortPatientDao();
        public SqlSurveyDao _survey = new SqlSurveyDao();
        public SqlQuestionAnswerLogDao _questionAnswerLogDao = new SqlQuestionAnswerLogDao();

        //
        // GET: /Survey/

        public ActionResult Index(AssortPatientModel model)
        {
            if (model.CurrentPage != 0)
                model.Paging.CurrentPage = model.CurrentPage;
            else
                model.Paging.CurrentPage = 1;

            model.DataList = _assortPatient.GetWithFilter(
                                (int)model.Paging.PageSize,
                                (int)model.Paging.CurrentPage,
                                model.PhoneNumber,
                                model.SurveyID,
                                (short)model.AssortType,
                                model.Score,
                                (short)model.Status
                                );
            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Paging.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Paging.TotalRecords = 0;

            }

            List<Survey> surveyList = new List<Survey>(_survey.GetAll());
            Survey surveyAll = new Survey();
            surveyAll.ID = -1;
            surveyAll.SurveyName = "Tất cả";
            surveyList.Add(surveyAll);

            ViewBag.SurveyList = new SelectList(surveyList, "ID", "SurveyName");
            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        [HttpGet]
        public ActionResult Export()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Export(DateTime? StartDate, DateTime? EndDate)
        {
            var listData = _assortPatient.GetExport(StartDate, EndDate);
            ExportToExcel(listData);
            return Redirect("/AssortPatient/Index");
        }

        public void ExportToExcel(List<AssortPatient> listassortPatient = null, string fileName = "")
        {
            MemoryStream ms;

            HSSFWorkbook hssfworkbook = new HSSFWorkbook();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, listassortPatient);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<AssortPatient> listassortPatient = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("Danh sách tham gia khảo sát");
            IRow rowheader = sheet1.CreateRow(1);
            rowheader.CreateCell(0).SetCellValue("Tên khảo sát");
            rowheader.CreateCell(1).SetCellValue("Điện thoại");
            rowheader.CreateCell(2).SetCellValue("Trạng thái");
            rowheader.CreateCell(3).SetCellValue("Ngày khảo sát");
            rowheader.CreateCell(4).SetCellValue("Câu hỏi/Kết quả trả lời");

            int maxCountAnswers = 1;

            IRow rowAnswers = sheet1.CreateRow(2);
            rowAnswers.CreateCell(0).SetCellValue("");
            rowAnswers.CreateCell(1).SetCellValue("");
            rowAnswers.CreateCell(2).SetCellValue("");
            rowAnswers.CreateCell(3).SetCellValue("");
            rowAnswers.CreateCell(4).SetCellValue("1");

            int i = 2;
            if (listassortPatient == null)
                return;

            foreach(var item in listassortPatient)
            {
                int j = 0;
                IRow row = sheet1.CreateRow(++i);
                row.CreateCell(j++).SetCellValue(item.ExtentionProperty["SurveyKeyword"].ToString());
                row.CreateCell(j++).SetCellValue(item.PhoneNumber);
                row.CreateCell(j++).SetCellValue(item.Status.ToString().Replace("_", " "));
                row.CreateCell(j++).SetCellValue(item.UpdatedDate.ToString("dd/MM/yyyy"));

                if (item.Status == AssortPatientStatus.Đã_Hoàn_Thành)
                {
                    var listTmp = _questionAnswerLogDao.GetAnswersComplete(item.SurveyID, item.PhoneNumber);
                    if (listTmp == null)
                        continue;

                    if (listTmp.Count > maxCountAnswers)
                    {
                        for (int c = maxCountAnswers + 1; c <= listTmp.Count; c++)
                        {
                            rowAnswers.CreateCell(c + 3).SetCellValue(c.ToString());
                            maxCountAnswers++;
                        }
                    }

                    foreach (var iTmp in listTmp)
                    {
                        try
                        {
                            object value = iTmp.ExtentionProperty["Answers"];
                            row.CreateCell(j++).SetCellValue(value == null ? "" : value.ToString());
                        }
                        catch(Exception)
                        {

                        }
                    }
                }
            }

            var cra = new NPOI.SS.Util.CellRangeAddress(1, 1, 4, maxCountAnswers + 3);
            sheet1.AddMergedRegion(cra);
        }

        void InitializeWorkbook(HSSFWorkbook hssfworkbook)
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        MemoryStream GetExcelStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }
    }
}