﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Web.Configuration;

namespace CaiThuocLa.Controllers
{
    public class SendSMSController : BaseController
    {
        SqlSendSMSDao _sendSMS = new SqlSendSMSDao();

        public SendSMSController()
        {
            NoAutenticatedActions.Add("PushStatus");
        }

        //
        // GET: /QuestionAnswerLog/

        public ActionResult Index(SendSMSModel model)
        {
            if (model.CurrentPage != 0)
                model.Paging.CurrentPage = model.CurrentPage;
            else
                model.Paging.CurrentPage = 1;

            model.DataList = _sendSMS.GetWithFilter(
                                (int)model.Paging.PageSize,
                                (int)model.Paging.CurrentPage,
                                model.Message,
                                model.PhoneNumber,
                                model.SMSGuidID,
                                (short)model.Status
                                );
            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Paging.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Paging.TotalRecords = 0;

            }

            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        public ActionResult PushStatus(string SMSGUID = "", string SMSStatus = "", string SecureCode = "")
        {
            int statusResult = 1;
            if (String.IsNullOrEmpty(SMSGUID) || (String.IsNullOrEmpty(SMSStatus) || String.IsNullOrEmpty(SecureCode)))
                statusResult = 1;
            else
            {
                if (SecureCode.Equals(WebConfigurationManager.AppSettings["CodePushStatus"]))
                {
                    var item = new List<SendSMS>(_sendSMS.GetByColumnName("SMSGuidID", SMSGUID));
                    if (item != null && item.Count == 1)
                    {
                        if (_sendSMS.UpdateStatus(short.Parse(SMSStatus), item[0].SMSGuidID))
                            statusResult = 2;
                        else
                            statusResult = 1;
                    }
                }
            }

            XElement xml = new XElement("Status", statusResult);
            return Content(xml.ToString(), "text/xml");
        }
    }
}
