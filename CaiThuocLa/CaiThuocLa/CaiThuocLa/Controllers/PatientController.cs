﻿using CaiThuocLa.Code;
using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class PatientController : BaseController
    {
        public SqlPatientDao _patientDao = new SqlPatientDao();
        public SqlLibraryDao _libraryDao = new SqlLibraryDao();
        public SqlPatientLibraryDao _patientLibrary = new SqlPatientLibraryDao();
        public SqlSMSDao _smsDao = new SqlSMSDao();
        public SqlSmsCronJobDao _smsCronJobDao = new SqlSmsCronJobDao();
        //
        // GET: /Patient/

        public ActionResult Index(PatientModel model)
        {
            if (model.CurrentPage != 0)
                model.Page.CurrentPage = model.CurrentPage;
            else
                model.Page.CurrentPage = 1;
            /*
             * Load list patient with filter
            */
            model.DataList = _patientDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.PatientName, model.PhoneNumber, model.Age,model.LibraryID, (int)model.Environment, (int)model.AddictionLevel, model.AccountManagerID, model.FromDate, model.ToDate, model.ShortCreatedDate);

            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Page.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Page.TotalRecords = 0;

            }
            List<Library> libraryList = _libraryDao.GetList();
            /*
             * Load combobox library search box
            */
            Library all = new Library();
            all.ID = 0;
            all.LibraryName = "Tất cả";
            libraryList.Insert(0, all);

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }

        public ActionResult Add()
        {
            PatientModel model = new PatientModel();
            List<Library> libraryList = _libraryDao.GetList();
            /*
             * Load combobox library search box
            */
            //Library all = new Library();
            //all.ID = 0;
            //all.LibraryName = "Tất cả";
            //libraryList.Insert(0, all);

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            return View();
        }

        [HttpPost]
        public ActionResult Add(PatientModel model)
        {
            var checkPatient = _patientDao.GetByColumnName("PhoneNumber", model.PhoneNumber).FirstOrDefault(p => p.PatientStatus != PatientStatus_vi_VN.Đã_xóa);

            if (checkPatient != null)
            {
                ErrorMessage = "Số điện thoại này đã được đăng ký bởi: " + checkPatient.PatientName;
                return Redirect("/Patient");
            }

            Patient patient = new Patient();
            patient.PatientName = model.PatientName;
            patient.PhoneNumber = model.PhoneNumber;
            patient.StartDate = GFunction.ConvertToMMddyyyy(Request.Form["StartDate"].ToString());
            patient.Age = model.Age;
            patient.AddictionLevel = model.AddictionLevel;
            patient.AccountManagerID = SAccount.ID;
            patient.Environment = model.Environment;
            if (_patientDao.Insert(patient))
            {
                if (model.LibraryID > 0)
                {
                    PatientLibrary pl = new PatientLibrary();
                    pl.LibraryID = model.LibraryID;
                    pl.PatientID = patient.ID;
                    if (_patientLibrary.Insert(pl))
                    {
                        /* Create CronJob for first message */
                        _smsCronJobDao.AddCronJobForFirstMessage(pl.LibraryID, pl.PatientID);
                        SuccessMessage = "Đã nhập thông tin bệnh nhân thành công";
                    }
                    else
                    {
                        ErrorMessage = "Cập nhật thất bại";
                    }
                }
                else
                {
                    ErrorMessage = "Cập nhật thất bại";
                }
            }
            return Redirect("/Patient");
        }

        public ActionResult Edit(long pid)
        {
            PatientModel model = new PatientModel();
            Patient pt = new Patient();
            pt = _patientDao.GetPatient(pid);
            if (pt != null)
            {
                model.PatientName = pt.PatientName;
                model.PhoneNumber = pt.PhoneNumber;
                model.StartDate = pt.StartDate;
                model.Age = pt.Age;
                model.AddictionLevel = pt.AddictionLevel;
                model.Environment = pt.Environment;
                model.ID = pt.ID;
            }
            /*
            * Load combobox library search box
           */
            List<Library> libraryList = _libraryDao.GetList();
            Library all = new Library();
            all.ID = 0;
            all.LibraryName = "Không chọn";
            libraryList.Insert(0, all);

            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName", Convert.ToInt64(pt.ExtentionProperty["LibraryID"]));
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PatientModel model)
        {
            var checkPatient = _patientDao.GetByColumnName("PhoneNumber", model.PhoneNumber).FirstOrDefault(p => p.PatientStatus != PatientStatus_vi_VN.Đã_xóa);

            if (checkPatient != null)
            {
                if (model.ID != checkPatient.ID)
                {
                    ErrorMessage = "Số điện thoại này đã được đăng ký bởi: " + checkPatient.PatientName;
                    return Redirect("/Patient");
                }
            }

            Patient patient = new Patient();
            patient.PatientName = model.PatientName;
            patient.PhoneNumber = model.PhoneNumber;
            patient.StartDate = new DateTime(1900, 01, 01);
            patient.Age = model.Age;
            patient.AccountManagerID = SAccount.ID;
            patient.AddictionLevel = model.AddictionLevel;
            patient.Environment = model.Environment;
            patient.ID = model.ID;
            if (_patientDao.Update(patient))
            {
                //PatientLibrary pl = new PatientLibrary();
                //pl.LibraryID = model.LibraryID;
                //pl.PatientID = model.ID;
                //if (_patientLibrary.Insert(pl))
                //{
                SuccessMessage = "Đã cập nhật thành công";
                //}
                //else
                //{
                //    ErrorMessage = "Cập nhật thất bại";
                //}
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            return Redirect("/Patient");
        }
        public ActionResult Delete(long pid)
        {
            if (_patientDao.UpdateStatus(pid, (int)PatientStatus_vi_VN.Đã_xóa) > 0)
            {
                SuccessMessage = "Đã xóa thành công";
            }
            else
            {
                ErrorMessage = "Xóa thất bại";
            }
            return Redirect("/Patient");
        }
        public ActionResult SetStartDate(long pid)
        {
            PatientModel model = new PatientModel();
            model.ID = pid;
            Patient patient = new Patient();
            patient = _patientDao.GetSingle(pid);
            ViewBag.Patient = patient;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateStartDate(PatientModel model)
        {
            if (_patientDao.UpdateStartDate(model.ID, model.StartDate) > 0)
            {
                SuccessMessage = "Đã cập nhật thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật thất bại";
            }
            return Redirect("/Patient");
        }

        public ActionResult ChangeLibrary(long pid)
        {
            PatientModel model = new PatientModel();
            model.LibraryID = _patientLibrary.GetLibraryOfPatient(pid);
            List<Library> libraryList = _libraryDao.GetList();
            /*
             * Load combobox library search box
            */
            //Library all = new Library();
            //all.ID = 0;
            //all.LibraryName = "Không chọn";
            //libraryList.Insert(0, all);
            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            model.ID = pid;
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeLibrary(PatientModel model)
        {
            if (_patientLibrary.ChangeLibrary(model.ID, model.LibraryID) > 0)
            {
                SuccessMessage = "Đã đổi thành công";
            }
            else
            {
                ErrorMessage = "Thư viện bạn vừa chọn chưa xác lập tin nhắn và giờ gửi";
            }
            return Redirect("/Patient");
        }

        public ActionResult ViewReport(PatientReportModel model)
        {
            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            DateTime newFromDate = model.FromDate;
            DateTime newToDate = model.ToDate;

            try
            {
                string[] arrFromDate = model.FromDate.ToString().Split(' ')[0].Split('/').ToArray();
                string[] arrToDate = model.ToDate.ToString().Split(' ')[0].Split('/').ToArray();
                newFromDate = new DateTime(arrFromDate[2].AsInt(), arrFromDate[1].AsInt(), arrFromDate[0].AsInt());
                newToDate = new DateTime(arrToDate[2].AsInt(), arrToDate[1].AsInt(), arrToDate[0].AsInt());
            }
            catch (Exception ex)
            { }

            model.NumEnvironmentUrban = _patientDao.CountPatientByEnvironment((int)Utility.Environment_vi_VN.Đô_thị, newFromDate, newToDate);
            model.NumEnvironmentSubUrban = _patientDao.CountPatientByEnvironment((int)Utility.Environment_vi_VN.Ven_đô, newFromDate, newToDate);
            model.NumEnvironmentCountrySide = _patientDao.CountPatientByEnvironment((int)Utility.Environment_vi_VN.Nông_thôn, newFromDate, newToDate);

            model.NumAddictionLevelHeavy = _patientDao.CountPatientByAddictionLevel((int)Utility.AddictionLevel_vi_VN.Nặng, newFromDate, newToDate);
            model.NumAddictionLevelMedium = _patientDao.CountPatientByAddictionLevel((int)Utility.AddictionLevel_vi_VN.Vừa, newFromDate, newToDate);
            model.NumAddictionLevelMild = _patientDao.CountPatientByAddictionLevel((int)Utility.AddictionLevel_vi_VN.Nhẹ, newFromDate, newToDate);

            model.FromDate = newFromDate;
            model.ToDate = newToDate;

            return View(model);
        }

        public ActionResult PatientImport()
        {
            List<Library> libraryList = _libraryDao.GetList();
            //Library all = new Library();
            //all.ID = 0;
            //all.LibraryName = "Chưa xác định";
            //libraryList.Insert(0, all);
            ViewBag.LibraryList = new SelectList(libraryList, "ID", "LibraryName");
            return View();
        }

        [HttpPost]
        public ActionResult Import(PatientImportViewModel model, HttpPostedFileBase fileImport = null)
        {
            string path = "/Userfile/FileImport";
            string returnPath = path + "/" + UploadFile(fileImport, path, null);
            ExecImport(model, returnPath);
            return Redirect("/Patient");
        }

        public void ExecImport(PatientImportViewModel model, string filePath)
        {
            try
            {
                List<Patient> patientFails = new List<Patient>();
                List<Patient> patientSuccess = new List<Patient>();
                List<Patient> patientWrongSyntax = new List<Patient>();
                var path = Path.Combine(Server.MapPath(filePath));
                HSSFWorkbook hssfworkbook;
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    hssfworkbook = new HSSFWorkbook(file);
                }
                ISheet sheet = hssfworkbook.GetSheetAt((int)model.ExcelSheetAt);
                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                bool isHeader = true;
                while (rows.MoveNext())
                {
                    try
                    {
                        if(!isHeader)
                        {
                            IRow row = (HSSFRow)rows.Current;
                            ICell cellPatientName = row.GetCell((int)model.ExcelColunmPatientName);
                            ICell cellPhoneNumber = row.GetCell((int)model.ExcelColunmPhoneNumber);
                            ICell cellAge = row.GetCell((int)model.ExcelColunmAge);
                            ICell cellEnvironment = row.GetCell((int)model.ExcelColunmEnvironment);
                            ICell cellAddictionLevel = row.GetCell((int)model.ExcelColunmAddictionLevel);
                            ICell cellStartDate = row.GetCell((int)model.ExcelColunmStartDate);

                            Patient patient = new Patient();
                            patient.AccountManagerID = SAccount.ID;
                            patient.PatientName = (cellPatientName != null) ? cellPatientName.StringCellValue : "";
                            patient.Age = (cellAge != null) ? (int)cellAge.NumericCellValue : 0;
                            patient.AddictionLevel = (cellAddictionLevel != null) ? (AddictionLevel_vi_VN)Enum.Parse(typeof(AddictionLevel_vi_VN), cellAddictionLevel.NumericCellValue.ToString()) : (byte)0;
                            
                            try
                            {
                                patient.PhoneNumber = (cellPhoneNumber != null) ? cellPhoneNumber.NumericCellValue.ToString() : "0";
                            }
                            catch (Exception ex)
                            {
                                patient.PhoneNumber = (cellPhoneNumber != null) ? cellPhoneNumber.StringCellValue.ToString() : "0";
                            }
                            patient.Environment = (cellEnvironment != null) ? (Environment_vi_VN)Enum.Parse(typeof(Environment_vi_VN), cellEnvironment.NumericCellValue.ToString()) : (byte)0;
                            patient.StartDate = (cellStartDate != null) ? cellStartDate.DateCellValue : new DateTime(1900,01,01);
                            patient.ShortCreatedDate = 0;

                            if (_patientDao.CheckPatientExists(patient.PhoneNumber) == 0)
                            {
                                if (patient.PhoneNumber.Length == 10 || patient.PhoneNumber.Length == 11)
                                {
                                    if (_patientDao.Insert(patient))
                                    {
                                        if( model.LibraryID > 0)
                                        {
                                            PatientLibrary pl = new PatientLibrary();
                                            pl.LibraryID = model.LibraryID;
                                            pl.PatientID = patient.ID;
                                            _patientLibrary.Insert(pl);
                                            /* Create CronJob for first message */
                                            _smsCronJobDao.AddCronJobForFirstMessage(pl.LibraryID,pl.PatientID);
                                        }
                                        patientSuccess.Add(patient);
                                    }
                                    else
                                    {
                                        if (patientFails.Where(m => m.PhoneNumber == patient.PhoneNumber).Count() <= 0)
                                        {
                                            patientFails.Add(patient);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (patientFails.Where(m => m.PhoneNumber == patient.PhoneNumber).Count() <= 0)
                                {
                                    patientFails.Add(patient);
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                       
                    }
                    isHeader = false;
                }
                if (patientFails.Count() > 0)
                {
                    Session["FailList"] = patientFails;
                    Session["DeplicatedMessage"] = "Tổng số bệnh nhận bị trùng : " + patientFails.Count();
                }

                SuccessMessage = "Tổng số bệnh nhận được nhập thành công : " + patientSuccess.Count();
                return;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Có lỗi phát sinh trong quá trình upload hoặc chọn data";
            }
        }

        MemoryStream GetExcelStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        void GenerateData(HSSFWorkbook hssfworkbook, List<Patient> PatientList = null)
        {
            ISheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
            sheet1.CreateRow(0).CreateCell(0).SetCellValue("Danh sách người cai nghiện");
            IRow rowheader = sheet1.CreateRow(1);
            rowheader.CreateCell(0).SetCellValue("Họ tên");
            rowheader.CreateCell(1).SetCellValue("Điện thoại");
            rowheader.CreateCell(2).SetCellValue("Tuổi");
            rowheader.CreateCell(3).SetCellValue("Môi trường");
            rowheader.CreateCell(4).SetCellValue("Mức độ");
            rowheader.CreateCell(5).SetCellValue("Trạng thái");

            int i = 2;
            if (PatientList == null)
                return;
            foreach (Patient patient in PatientList)
            {
                int j = 0;
                IRow row = sheet1.CreateRow(i);
                row.CreateCell(j).SetCellValue(patient.PatientName);
                j++;
                row.CreateCell(j).SetCellValue(patient.PhoneNumber);
                j++;
                row.CreateCell(j).SetCellValue(patient.Age);
                j++;
                row.CreateCell(j).SetCellValue(patient.Environment.ToString());
                j++;
                row.CreateCell(j).SetCellValue(patient.PatientStatus.ToString());
                j++;
                i++;
            }
        }

        void InitializeWorkbook(HSSFWorkbook hssfworkbook)
        {
            hssfworkbook = new HSSFWorkbook();

            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "NPOI Team";
            hssfworkbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "NPOI SDK Example";
            hssfworkbook.SummaryInformation = si;
        }

        public ActionResult ProcessExport(PatientModel model)
        {
            List<Patient> patientList = _patientDao.GetWithFilter(model.Page.PageSize, model.Page.CurrentPage, model.PatientName, model.PhoneNumber, model.Age,model.LibraryID, (int)model.Environment, (int)model.AddictionLevel, model.AccountManagerID, model.FromDate, model.ToDate, model.ShortCreatedDate);
            string fileName = "patientlist_" + Guid.NewGuid().ToString() + ".xls";
            ExportToExcel(patientList, fileName);
            return Redirect("/PinCode");
        }
        public ActionResult ContinueReceveSMS(long pid)
        {
            if (_patientDao.UpdateStatus(pid, (int)PatientStatus_vi_VN.Đang_cai_nghiện) > 0)
            {
                SuccessMessage = "Cập nhật dữ liệu thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật dữ liệu thất bại";
            }
            return Redirect("/Patient");
        }

        public ActionResult PauseReceveSMS(long pid)
        {
            if (_patientDao.UpdateStatus(pid, (int)PatientStatus_vi_VN.Tạm_dừng_nhận_tin) > 0)
            {
                SuccessMessage = "Cập nhật dữ liệu thành công";
            }
            else
            {
                ErrorMessage = "Cập nhật dữ liệu thất bại";
            }
            return Redirect("/Patient");
        }

        public void ExportToExcel(List<Patient> patientList = null, string fileName = "")
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            Response.Clear();
            InitializeWorkbook(hssfworkbook);
            GenerateData(hssfworkbook, patientList);
            GetExcelStream(hssfworkbook).WriteTo(Response.OutputStream);
            Response.End();
        }

        private ICell GetCell(IRow row, int column)
        {
            ICell cell = row.GetCell(column);
            if (cell == null)
                return row.CreateCell(column);
            return cell;
        }

        private bool SendWelcomeMessage(Patient patient,long libraryId)
        {
            List<SMS> firstListSend = _smsDao.GetFirstSMS(libraryId);
            if(firstListSend.Count() == 2)
            {
                SMS sms1 = firstListSend.Where(s => s.Message.Contains("Chao mung")).FirstOrDefault();
                SMS sms2 = firstListSend.Where(s => s.Message.Contains("BINHTHHUONG")).FirstOrDefault();
                int code1 = 200;//Tools.SendSMSToUser(patient.PhoneNumber, sms1.Message, Guid.NewGuid().ToString());
                Thread.Sleep(2000);
                int code2 = 200;//Tools.SendSMSToUser(patient.PhoneNumber, sms2.Message, Guid.NewGuid().ToString());
                if(code1 == 200 && code2 == 200)
                    return true;
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
