﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using CaiThuocLa.Models;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CaiThuocLa.Controllers
{
    public class QuestionAnswerLogController : BaseController
    {
        SqlQuestionAnswerLogDao _questionAnswerLog = new SqlQuestionAnswerLogDao();
        //
        // GET: /QuestionAnswerLog/

        public ActionResult Index(QuestionAnswerLogModel model)
        {
            if (model.CurrentPage != 0)
                model.Paging.CurrentPage = model.CurrentPage;
            else
                model.Paging.CurrentPage = 1;

            model.DataList = _questionAnswerLog.GetWithFilter(
                                (int)model.Paging.PageSize,
                                (int)model.Paging.CurrentPage,
                                model.Question,
                                model.AnswersContent,
                                model.PhoneNumber
                                );
            if (model.DataList != null && model.DataList.Count() > 0)
            {
                var firstOrDefault = model.DataList.FirstOrDefault();
                if (firstOrDefault != null)
                    model.Paging.TotalRecords = (model.DataList.Any())
                        ? Convert.ToInt64(firstOrDefault.ExtentionProperty["TotalRec"].ToString())
                        : 0;
                else
                    model.Paging.TotalRecords = 0;

            }

            ViewBag.SuccessMessage = (SuccessMessage != null) ? SuccessMessage : "";
            ViewBag.ErrorMessage = (ErrorMessage != null) ? ErrorMessage : "";
            SuccessMessage = "";
            ErrorMessage = "";
            return View(model);
        }
    }
}
