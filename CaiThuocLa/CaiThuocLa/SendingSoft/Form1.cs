﻿using CaiThuocLa.DAO;
using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SendingSoft
{
    public partial class Form1 : Form
    {
        public SqlSmsCronJobDao _smsCronJobDao = new SqlSmsCronJobDao();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            backgroundWorkerExec.RunWorkerAsync();
        }

        private void PrintLog(string message)
        {
            richTextBoxMessage.Invoke((Action)delegate
            {
                if (richTextBoxMessage.TextLength > 10000)
                    richTextBoxMessage.Clear();
                string now = DateTime.Now.ToString("[ dd/MM/yyyy hh:mm:ss tt ] ");
                richTextBoxMessage.AppendText(now + message + "\n");
            });
        }

        private void backgroundWorkerExec_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                List<SmsCronJob> sendingList = new List<SmsCronJob>();
                sendingList = _smsCronJobDao.GetSmsCronJobToSend();
                if (sendingList.Count() > 0)
                {
                    foreach(SmsCronJob cron in sendingList)
                    {
                        PrintLog(cron.ExtentionProperty["PhoneNumber"].ToString() + " --> " + cron.ExtentionProperty["Message"]);
                        SendSMSToUser(cron.ExtentionProperty["PhoneNumber"].ToString(), cron.ExtentionProperty["Message"].ToString(), Guid.NewGuid().ToString());
                        _smsCronJobDao.UpdateStatus(cron.ID);
                    }
                }
                else
                {
                    PrintLog("Không có tin để gửi. Chờ 30s");
                }
                Thread.Sleep(30000);
            }
        }

        public int SendSMSToUser(string clientPhoneNumber, string message, string guid)
        {
            int code = 500;
            try
            {
                string clientNo = System.Configuration.ConfigurationManager.AppSettings["ClientNo"];
                string clientPass = System.Configuration.ConfigurationManager.AppSettings["ClientPass"];
                string serviceType = System.Configuration.ConfigurationManager.AppSettings["ServiceTypeSmsHostingId"];
                SMSServiceReference.ServiceSoapClient client = new SMSServiceReference.ServiceSoapClient();
                string result = client.SendSMS(clientNo,clientPass,clientPhoneNumber,message,guid,Convert.ToInt32(serviceType));
                code = Convert.ToInt32(Regex.Match(result, "<Code>(.*?)</Code>").Groups[1].Value);
                //code = 200;
            }
            catch (Exception ex)
            {
                return 500;
            }
            return code;
        }

        
    }
}
