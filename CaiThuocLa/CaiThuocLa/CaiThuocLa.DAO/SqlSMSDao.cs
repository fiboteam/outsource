using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlSMSDao : SqlDaoBase<SMS>
    {
        public SqlSMSDao()
        {
            TableName = "tblSMS";
            EntityIDName = "SMSID";
            StoreProcedurePrefix = "spSMS_";
        }
        public SqlSMSDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SMS> GetWithFilter(
            long pagesize,
            long pagenum,
            string message,
            Int64 libraryID,
            Int16 smsStatus
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@message", message, 
                        "@libraryid", libraryID, 
                        "@smsstatus" ,smsStatus
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<SMS> GetSMSAll()
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetSMSAll";
                return DbAdapter1.ReadList(sql, Make, true);
            }
            catch(Exception)
            {
                return null;
            }
        }
        
        public bool AddUpdate(SMS sms)
        {
            try
            {
                string sql = StoreProcedurePrefix + "AddUpdate";
                object[] parms =
                    {
                        "@id", sms.ID, 
                        "@message", sms.Message, 
                        "@libraryid", sms.LibraryID, 
                        "@smsstatus", sms.SMSStatus, 
                        "@createdbyid" ,sms.CreatedByID,
                        "@updatebyid",sms.UpdatedByID
                    };
                var id = (Int64)DbAdapter1.ExcecuteScalar(sql, true, parms);

                if (id > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception)
            {
                return false;
            }
        }

        public List<SMS> GetListNotExistsInLibrary(long libraryId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetListNotExistsInLibrary";
                object[] parms =
                    {
                        "@libraryId", libraryId
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<SMS> GetFirstSMS(long libraryId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFirstSMS";
                object[] parms =
                    {
                        "@libraryId", libraryId
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int AddFirstSms(long libraryId,long createdById,string message1,string message2,string keyword1,string keyword2,string keyword3,string replyMessage1,string replyMessage2,string replyMessage3)
        {
            try
            {
                string sql = StoreProcedurePrefix + "AddFirstSms";
                object[] parms =
                    {
                        "@libraryId", libraryId, 
                        "@createdById", createdById, 
                        "@message1", message1, 
                        "@message2", message2, 
                        "@keyword1" ,keyword1,
                        "@keyword2", keyword2,
                        "@keyword3", keyword3,
                        "@replyMessage1",replyMessage1,
                        "@replyMessage2", replyMessage2,
                        "@replyMessage3", replyMessage3
                    };
               return (int)DbAdapter1.ExcecuteScalar(sql, true, parms);
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
