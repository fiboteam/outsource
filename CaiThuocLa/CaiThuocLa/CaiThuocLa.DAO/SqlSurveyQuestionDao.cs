using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlSurveyQuestionDao : SqlDaoBase<SurveyQuestion>
    {
        public SqlSurveyQuestionDao()
        {
            TableName = "tblSurveyQuestion";
            EntityIDName = "SurveyQuestionID";
            StoreProcedurePrefix = "spSurveyQuestion_";
        }
        public SqlSurveyQuestionDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SurveyQuestion> GetWithFilter(
            int pagesize,
            int pagenum,
            string question = "",
            string answersSearch = "",
            Int64 surveyID = -1,
            short status = 0
            )
        {

            try
            {

                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                {
                    "@pagesize", pagesize,
                    "@pagenum", pagenum,
                    "@question", question, 
                    "@answersSearch", answersSearch,
                    "@surveyID", surveyID, 
                    "@status", status
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SurveyQuestion> GetBySurveyID(Int64 id)
        {
            string sql = StoreProcedurePrefix + "GetBySurveyID";
            object[] parms =
            {
                "@surveyid", id
            };

            return DbAdapter1.ReadList(sql, Make, true, parms);
        }
    }
}
