using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlReceivedSMSDao : SqlDaoBase<ReceivedSMS>
    {
        public SqlReceivedSMSDao()
        {
            TableName = "tblReceivedSMS";
            EntityIDName = "ReceivedSMSID";
            StoreProcedurePrefix = "spReceivedSMS_";
        }
        public SqlReceivedSMSDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<ReceivedSMS> GetWithFilter(int pageSize, int pageNum, string message = "", string phoneNumber = "", string guidID = "")
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = 
                {
                    "@pagesize", pageSize,
                    "@pageNum", pageNum,
                    "@message", message,
                    "@phoneNumber", phoneNumber,
                    "@guidid", guidID
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
