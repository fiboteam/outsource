using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlLibraryDao : SqlDaoBase<Library>
    {
        public SqlLibraryDao()
        {
            TableName = "tblLibrary";
            EntityIDName = "LibraryID";
            StoreProcedurePrefix = "spLibrary_";
        }
        public SqlLibraryDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
    
        public List<Library> GetList()
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetList";
                object[] parms = {  };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Library> GetWithFilter(long pagesize,
                            long pagenum,
                            string libraryName,
                            int status,
                            int libraryType
         )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@libraryName" ,libraryName,
                        "@status", status,
                        "@libraryType", libraryType
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int UpdateStatus(long libraryId,int libraryStatus)
        {
            string sql = StoreProcedurePrefix + "UpdateStatus";
            object[] parms = { "@libraryId", libraryId, "@libraryStatus", libraryStatus };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }

        public int ActiveLibrary(long libraryId)
        {
            string sql = StoreProcedurePrefix + "ActiveLibrary";
            object[] parms = { "@libraryId", libraryId };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }
    }
}
