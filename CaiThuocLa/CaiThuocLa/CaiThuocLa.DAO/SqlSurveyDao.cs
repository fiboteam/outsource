using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlSurveyDao : SqlDaoBase<Survey>
    {
        public SqlSurveyDao()
        {
            TableName = "tblSurvey";
            EntityIDName = "SurveyID";
            StoreProcedurePrefix = "spSurvey_";
        }
        public SqlSurveyDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<Survey> GetWithFilter(int pageSize, int pageNum, string surveyName = "", string surveykeyword = "", short status = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = 
                {
                    "@pagesize", pageSize,
                    "@pageNum", pageNum,
                    "@surveykeyword", surveykeyword,
                    "@surveyname", surveyName,
                    "@status", status
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool AddUpdate(Survey survey)
        {
            string sql = StoreProcedurePrefix + "AddUpdate";
            object[] parms =
            {
                "@id", survey.ID, 
                "@surveyname", survey.SurveyName, 
                "@surveykeyword", survey.SurveyKeyword,
                "@status", survey.Status, 
                "@totalquestion", survey.TotalQuestion,
                "@createddate", survey.CreatedDate,
                "@updateddate", survey.UpdatedDate
            };

            Int64 id = (Int64)DbAdapter1.ExcecuteScalar(sql, true, parms);
            if (id > 0) return true;
            return false;
        }

        public string GetQuestion(Int64 surveyid = -1, string answers = "", string phoneNumber = "", short timeOut = 24)
        {
            string sql = StoreProcedurePrefix + "GetQuestion";
            object[] parms =
            {
                "@surveyid",surveyid,
                "@phonenumber",phoneNumber,
                "@answers", answers,
                "@timeout", timeOut
            };

            return DbAdapter1.ExcecuteScalar(sql, true, parms).ToString();
        }

        public string v2_CheckSurveyNew(long surveyID, string phoneNumber, int timeOut)
        {
            string sql = StoreProcedurePrefix + "v2_CheckSurveyNew";
            object[] parms =
            {
                "@SurveyID", surveyID,
                "@PhoneNumber", phoneNumber,
                "@TimeOut",timeOut
            };

            return DbAdapter1.ExcecuteScalar(sql, true, parms).ToString();
        }

        public string v2_AnswersQuestion(string phoneNumber, string answers, int timeOut)
        {
            string sql = StoreProcedurePrefix + "v2_AnswersQuestion";
            object[] parms =
            {
                "@PhoneNumber", phoneNumber,
                "@Answers", answers,
                "@TimeOut", timeOut
            };

            return DbAdapter1.ExcecuteScalar(sql, true, parms).ToString();
        }

        public string v2_GetContinueQuestion(long surveyID, string phoneNumber, bool isNew, long surveyQuestionID)
        {
            string sql = StoreProcedurePrefix + "v2_GetContinueQuestion";
            object[] parms =
            {
                "@SurveyID", surveyID,
                "@PhoneNumber", phoneNumber,
                "@IsNew", isNew,
                "@SurveyQuestionID", surveyQuestionID
            };

            return DbAdapter1.ExcecuteScalar(sql, true, parms).ToString();
        }
    }
}
