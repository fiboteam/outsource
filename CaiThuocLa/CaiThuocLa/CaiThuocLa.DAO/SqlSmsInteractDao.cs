using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlSmsInteractDao : SqlDaoBase<SmsInteract>
    {
        public SqlSmsInteractDao()
        {
            TableName = "tblSmsInteract";
            EntityIDName = "SmsInteractID";
            StoreProcedurePrefix = "spSmsInteract_";
        }
        public SqlSmsInteractDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SmsInteract> GetWithFilter(int pageSize, int pageNum, string keyword = "", string replyMessage = "", short status = 0, string contentsms = "")
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = 
                {
                    "@pagesize", pageSize,
                    "@pageNum", pageNum,
                    "@keyword", keyword,
                    "@replyMessage", replyMessage,
                    "@contentsms", contentsms,
                    "@status", status
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SmsInteract> GetSmsInteractID(string phoneNumber)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetSmsInteractID";
                object[] parms = 
                { 
                    "@phonenumber", phoneNumber
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
