using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlSendSMSDao : SqlDaoBase<SendSMS>
    {
        public SqlSendSMSDao()
        {
            TableName = "tblSendSMS";
            EntityIDName = "SendSMSID";
            StoreProcedurePrefix = "spSendSMS_";
        }
        public SqlSendSMSDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SendSMS> GetWithFilter(int pageSize, int pageNum, string message = "", string phoneNumber = "", string guidID = "", short status = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = 
                {
                    "@pagesize", pageSize,
                    "@pageNum", pageNum,
                    "@message", message,
                    "@phoneNumber", phoneNumber,
                    "@guidid", guidID,
                    "@status", status
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateStatus(short status, string guid)
        {
            try
            {
                string sql = StoreProcedurePrefix + "UpdateStatus";
                object[] parms = 
                {
                    "@smsguidid",guid,
                    "@status", (status != 1 && status != 2 ? 3 : status)
                };

                if (DbAdapter1.ExecuteNonQuery(sql, true, parms) > 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int CountAllSmsSent()
        {
            string sql = "select count(*) from tblSendSMS ";
            object[] parms = {  };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, false, parms));
        }
    }
}
