using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlQuestionAnswerLogDao : SqlDaoBase<QuestionAnswerLog>
    {
        public SqlQuestionAnswerLogDao()
        {
            TableName = "tblQuestionAnswerLog";
            EntityIDName = "QuestionAnswerLogID";
            StoreProcedurePrefix = "spQuestionAnswerLog_";
        }
        public SqlQuestionAnswerLogDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
        
        public List<QuestionAnswerLog> GetWithFilter(
            int pagesize,
            int pagenum,
            string question = "",
            string answersContent = "",
            string phoneNumber = ""
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                {
                    "@pagesize",pagesize,
                    "@pagenum",pagenum,
                    "@question",question,
                    "@answersContent",answersContent,
                    "@phoneNumber",phoneNumber
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<QuestionAnswerLog> GetAnswersComplete(long surveyID, string phoneNumber)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetAnswersComplete";
                object[] parms =
                {
                    "@SurveyID",surveyID,
                    "@PhoneNumber",phoneNumber,
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
