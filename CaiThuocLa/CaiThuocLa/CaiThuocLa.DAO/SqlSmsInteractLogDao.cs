using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;


namespace CaiThuocLa.DAO
{
    public class SqlSmsInteractLogDao : SqlDaoBase<SmsInteractLog>
    {
        public SqlSmsInteractLogDao()
        {
            TableName = "tblSmsInteractLog";
            EntityIDName = "SmsInteractLogID";
            StoreProcedurePrefix = "spSmsInteractLog_";
        }
        public SqlSmsInteractLogDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SmsInteractLog> GetWithFilter(int pageSize, int pageNum, string keyword = "", string phonenumber = "")
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms = 
                {
                    "@pagesize", pageSize,
                    "@pageNum", pageNum,
                    "@keyword", keyword,
                    "@phonenumber", phonenumber
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
