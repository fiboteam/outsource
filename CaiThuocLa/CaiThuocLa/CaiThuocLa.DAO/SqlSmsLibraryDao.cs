﻿using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaiThuocLa.DAO
{
    public class SqlSmsLibraryDao : SqlDaoBase<SmsLibrary>
    {
        public SqlSmsLibraryDao()
        {
            TableName = "tblSmsLibrary";
            EntityIDName = "SmsLibraryID";
            StoreProcedurePrefix = "spSmsLibrary_";
        }
        public SqlSmsLibraryDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
    
        public List<SmsLibrary> GetListInLibrary(long libraryid)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetListInLibrary";
                object[] parms =
                    {
                        "@libraryid", libraryid
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        
        public int DeleteSms(long smsLibraryId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "DeleteSms";
                object[] parms = 
                {
                    "@smsLibraryId", smsLibraryId
                };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch(Exception ex)
            {
                return -1;
            }
        }
    }
}
