using CaiThuocLa.DTO;
using CaiThuocLa.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaiThuocLa.DAO
{
    public class SqlAssortPatientDao : SqlDaoBase<AssortPatient>
    {
        public SqlAssortPatientDao()
        {
            TableName = "tblAssortPatient";
            EntityIDName = "AssortPatientID";
            StoreProcedurePrefix = "spAssortPatient_";
        }
        public SqlAssortPatientDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<AssortPatient> GetWithFilter(
            int pagesize,
            int pagenum,
            string phoneNumber = "",
            Int64 surveyID = -1,
            short assortType = 0,
            float score = 0,
            short Status = 0
            )
        {
            try
            {

                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                {
                    "@pagesize", pagesize,
                    "@pagenum", pagenum,
                    "@phoneNumber", phoneNumber, 
                    "@surveyID", surveyID, 
                    "@assortType", assortType,
                    "@score", score,
                    "@status", Status
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<AssortPatient> GetExport(DateTime? StartDate, DateTime? EndDate)
        {
            try
            {
                StartDate = (StartDate != null ? DateTime.Parse(StartDate.Value.ToString("yyyy-MM-dd 00:00:00")) : StartDate);
                EndDate = (EndDate != null ? DateTime.Parse(EndDate.Value.ToString("yyyy-MM-dd 23:59:59")) : EndDate);

                string sql = StoreProcedurePrefix + "GetExport";
                object[] parms =
                {
                    "@StartDate", StartDate,
                    "@EndDate", EndDate
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
