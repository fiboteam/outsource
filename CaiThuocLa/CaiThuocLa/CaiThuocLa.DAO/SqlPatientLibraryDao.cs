using CaiThuocLa.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CaiThuocLa.DAO
{
    public class SqlPatientLibraryDao : SqlDaoBase<PatientLibrary>
    {
        public SqlPatientLibraryDao()
        {
            TableName = "tblPatientLibrary";
            EntityIDName = "PatientLibraryID";
            StoreProcedurePrefix = "spPatientLibrary_";
        }
        public SqlPatientLibraryDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public long GetLibraryOfPatient(long patientId)
        {
            string sql = StoreProcedurePrefix + "GetLibraryOfPatient";
            object[] parms = { "@patientId", patientId };
            return Convert.ToInt64(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }

        public int ChangeLibrary(long patientId,long libraryid)
        {
            string sql = StoreProcedurePrefix + "ChangeLibrary";
            object[] parms = { "@patientId", patientId, "@libraryid", libraryid };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }
    }
}
