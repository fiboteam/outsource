using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlPatientDao : SqlDaoBase<Patient>
    {
        public SqlPatientDao()
        {
            TableName = "tblPatient";
            EntityIDName = "PatientID";
            StoreProcedurePrefix = "spPatient_";
        }
        public SqlPatientDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
       
        public List<Patient> GetWithFilter(long pagesize,
                            long pagenum,
                            string patientName,
                            string phoneNumber,
                            int age,
                            long libraryid,
                            int environment,
                            int addictionLevel,
                            long accountManagerId,
                            DateTime fromDate,
                            DateTime toDate,
                            int shortCreatedDate
                           )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@patientName" ,patientName,
                        "@phoneNumber", phoneNumber, 
                        "@age", age,
                        "@libraryid", libraryid,
                        "@environment" ,environment,
                        "@addictionLevel", addictionLevel, 
                        "@accountManagerId", accountManagerId,
                        "@fromDate" ,fromDate,
                        "@toDate", toDate, 
                        "@shortCreatedDate", shortCreatedDate
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Patient GetPatient(long patientId)
        {
            string sql = StoreProcedurePrefix + "GetPatient";
            object[] parms = { "@patientId", patientId };
            return DbAdapter1.Read(sql, Make, true, parms);
        }

        public Patient GetPatientByPhone(string phoneNumber)
        {
            string sql = StoreProcedurePrefix + "GetPatientByPhone";
            object[] parms = { "@PhoneNumber", phoneNumber };
            return DbAdapter1.Read(sql, Make, true, parms);
        }

        public int UpdateStatus(long patientId, int patientStatus)
        {
            string sql = StoreProcedurePrefix + "UpdateStatus";
            object[] parms = { "@patientId", patientId, "@patientStatus", patientStatus };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }

        public int UpdateStartDate(long patientId, DateTime startDate)
        {
            string sql = StoreProcedurePrefix + "UpdateStartDate";
            object[] parms = { "@patientId", patientId, "@startDate", startDate };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }

        public int CountPatientByAddictionLevel(int addictionLevel, DateTime fromDate, DateTime toDate)
        {
            string sql = StoreProcedurePrefix + "CountPatientByAddictionLevel";
            object[] parms = { "@addictionLevel", addictionLevel, "@fromDate", fromDate, "@toDate", toDate };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }

        public int CountPatientByEnvironment(int environment, DateTime fromDate, DateTime toDate)
        {
            string sql = StoreProcedurePrefix + "CountPatientByEnvironment";
            object[] parms = { "@environment", environment, "@fromDate", fromDate, "@toDate", toDate };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
        }

        public int CheckPatientExists(string phoneNumber)
        {
            string sql = "select * from tblPatient where PhoneNumber = @phonenumber AND PatientStatus <> 100";
            object[] parms = { "@phonenumber", phoneNumber };
            var result = DbAdapter1.Read(sql, Make, false, parms);

            if (result != null)
                return 1;
            return 0;
        }

        public int CountAllPatient()
        {
            string sql = "select count(*) from tblPatient where PatientStatus <> 100 ";
            object[] parms = { };
            return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, false, parms));
        }

        public int ChangeStatus(long patientId,int status)
        {
            string sql = String.Format("update tblPatient set PatientStatus = {0} where PatientID = {1}",status,patientId);
            object[] parms = { };
            return Convert.ToInt32(DbAdapter1.ExecuteNonQuery(sql, false, parms));
        }
    }
}
