using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CaiThuocLa.DTO;

namespace CaiThuocLa.DAO
{
    public class SqlSmsCronJobDao : SqlDaoBase<SmsCronJob>
    {
        public SqlSmsCronJobDao()
        {
            TableName = "tblSmsCronJob";
            EntityIDName = "SmsCronJobId";
            StoreProcedurePrefix = "spSmsCronJob_";
        }
        public SqlSmsCronJobDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<SmsCronJob> GetSmsCronJobToSend()
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetSmsCronJobToSend";
                object[] parms = { };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int UpdateStatus(long smsCronJobId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "UpdateStatus";
                object[] parms = { "@smsCronJobId", smsCronJobId };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch(Exception ex)
            {
                return -1;
            }
        }

       

        public int AddCronJobForFirstMessage(long libraryId, long patientId)
        {
            try
            {
                string sql = StoreProcedurePrefix + "AddCronJobForFirstMessage";
                object[] parms = { "@libraryId", libraryId, "@patientId", patientId };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch(Exception ex)
            {
                return -1;
            }
        }
        

        public List<SmsCronJob> GetWithFilter(
            long pagesize,
            long pagenum,
            Int64 libraryID,
            Int16 smsCronJobStatus,
            string message,
            string patientPhone
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum" ,pagenum,
                        "@libraryID", libraryID, 
                        "@smsCronJobStatus" ,smsCronJobStatus,
                        "@message", message, 
                        "@patientPhone", patientPhone
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch(Exception)
            {
                return null;
            }
        }


        public List<SmsCronJob> GetSmsByPatitent(string phoneNumber)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetSmsByPatitent";
                object[] parms =
                    {
                        "@phonenumber", phoneNumber
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
