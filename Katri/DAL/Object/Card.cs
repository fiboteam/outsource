﻿using Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DAL.Object
{
    public class Card: ObjectBase
    {
        public string CardNo { set; get; }
        public string CardName { set; get; }
        public string ServiceType { set; get; }
        public string Doctor { set; get; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Valid { set; get; }
        public string ToothPosition { set;get;}
        public int? ToothAmount { set; get; } 
        public long CreatedByUserId { set; get; }
        public long UpdatedByUserId { set; get; }
        public CardStatus CardStatus { set; get; }


        public Card()
        {
            ServiceType = "";
            ToothPosition = "";
            CardStatus = Utility.CardStatus.Active;
            ToothAmount = 0;
            CreatedByUserId = 0;
            UpdatedByUserId = 0;
        }
    }
}
