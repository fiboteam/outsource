﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Object
{
    public class User: ObjectBase
    {
        public string UserName { set; get; }
        public string Password { set; get; }
        public string Email { set; get; }
        public string PhoneNumber { set; get; }
        public string Address { set; get; }
        public string FullName { set; get; }
        public long ParentId { set; get; }
    }
}
