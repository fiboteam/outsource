﻿using DAL.Object;
using Utility;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity.Migrations;
using System.Text;
using System.Transactions;
using EntityFramework.MappingAPI;
using EntityFramework.BulkInsert.Extensions;

namespace DAL.EF
{
    public class EFBase<T> where T: ObjectBase
    {
        public readonly SystemDbContext _context = new SystemDbContext();

        public EFBase()
        {
            _context.Configuration.LazyLoadingEnabled = false;
        }

        internal DbSet<T> GetDbSet
        {
            get { return _context.Set<T>(); }
        }

        public IQueryable<T> GetAll()
        {
            try
            {
                return GetDbSet;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IQueryable<T> GetByFunc<TKey>(Expression<Func<T, bool>> funcWhere = null, Expression<Func<T, TKey>> funcOrderBy = null, string filterValue = "")
        {
            try
            {
                IQueryable<T> result = GetDbSet;

                if (funcWhere != null)
                    result = result.Where(funcWhere);
                if (funcOrderBy != null)
                    result = result.OrderBy(funcOrderBy);
                
                if(!String.IsNullOrEmpty(filterValue))
                {
                    //result.Except
                }

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool AddOrUpdate(T item)
        {
            try
            {
                GetDbSet.AddOrUpdate(item);
                _context.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public T FindByFunc(Expression<Func<T, bool>> funcWhere)
        {
            try
            {
                return GetDbSet.FirstOrDefault(funcWhere);
            }
            catch(Exception)
            {
                return null;
            }
        }

        public T FindById(long id)
        {
            try
            {
                return GetDbSet.FirstOrDefault<T>(p => p.Id == id);
            }
            catch(Exception)
            {
                return null;
            }
        }

        public bool AddOrUpdateRanger(List<T> listItem)
        {
            try
            {
                GetDbSet.AddOrUpdate(listItem.ToArray());
                _context.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public bool BulkInsert(List<T> listItem)
        {
            try
            {
                using (var transactionScope = new TransactionScope())
                {
                    _context.BulkInsert(listItem);
                    transactionScope.Complete();
                }

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public bool Delete(T item)
        {
            try
            {
                GetDbSet.Remove(item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(long id)
        {
            try
            {
                var item = GetDbSet.Find(id);
                GetDbSet.Remove(item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(List<long> arrId)
        {
            try
            {
                var items = GetDbSet.Where(p => arrId.Contains(p.Id));
                return Delete(items);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(IQueryable<T> items)
        {
            try
            {
                GetDbSet.RemoveRange(items);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ObjectPage<T> GetWithFilterBase(ObjectPage<T> objectPage, System.Linq.Expressions.Expression<Func<T, bool>> func)
        {
            objectPage.TotalRecord = GetDbSet.Where(func).Count();
            objectPage.Data = GetDbSet.Where(func).OrderBy(m => m.Id).Skip(objectPage.Record * (objectPage.CurrentPage - 1)).Take(objectPage.Record);
            return objectPage;
        }
    }
}
