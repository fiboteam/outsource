﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Routing;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc.Ajax;
using System.Dynamic;
using System.ComponentModel.DataAnnotations;
using System.Collections;

namespace Katri.App_Code
{
    public static class HtmlHelperExtension
    {
        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "", Value = "" } };

        public static MvcHtmlString HtmlString(this HtmlHelper html, string str = "")
        {
            return MvcHtmlString.Create(str);
        }


        //public static MvcHtmlString HtmlPager(this HtmlHelper html, Paging paging, bool isShowRefesh = false, string syntax = "currentpage",
        //    object htmlAttributes = null)
        //{
        //    if (paging == null || paging.ListPagers.Count <= 0)
        //        return null;

        //    HttpRequestBase request = html.ViewContext.HttpContext.Request;
        //    NameValueCollection urlParameters = HttpUtility.ParseQueryString(request.Url.Query);
        //    urlParameters.Remove(syntax);
        //    string url = request.Url.AbsolutePath + "?" + urlParameters.ToString();
        //    string strCurrentPage = "1";

        //    TagBuilder tag = new TagBuilder("ul");
        //    TagBuilder liTag;
        //    TagBuilder aTag;

        //    //tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));

        //    foreach (Pager pager in paging.ListPagers)
        //    {
        //        liTag = new TagBuilder("li");
        //        aTag = new TagBuilder("a");

        //        aTag.Attributes.Add("href", url + "&currentpage=" + pager.PageNum);
        //        aTag.Attributes.Add("title", pager.Title);
        //        if (pager.CurrentPage)
        //        {
        //            liTag.Attributes.Add("class", "currentpage");
        //            strCurrentPage = pager.PageNum;
        //        }
        //        aTag.SetInnerText(pager.Title);

        //        liTag.InnerHtml += aTag.ToString();
        //        tag.InnerHtml += liTag.ToString();
        //    }

        //    if (isShowRefesh)
        //    {
        //        TagBuilder imgTag = new TagBuilder("img");
        //        liTag = new TagBuilder("li");

        //        liTag.Attributes.Add("class", "sep");
        //        tag.InnerHtml += liTag.ToString();

        //        liTag = new TagBuilder("li");
        //        aTag = new TagBuilder("a");

        //        aTag.Attributes.Add("href", url + "&currentpage=" + strCurrentPage);
        //        imgTag.Attributes.Add("src", "~/../../Assets/Images/arrow_circle.png");
        //        imgTag.Attributes.Add("width", "16");
        //        imgTag.Attributes.Add("height", "16");

        //        aTag.InnerHtml += imgTag.ToString();
        //        liTag.InnerHtml += aTag.ToString();
        //        tag.InnerHtml += liTag.ToString();
        //    }

        //    return MvcHtmlString.Create(tag.ToString());
        //}

        public static RouteValueDictionary ToRouteDic(this NameValueCollection collection)
        {
            return collection.ToRouteDic(new RouteValueDictionary());
        }

        public static RouteValueDictionary ToRouteDic(this NameValueCollection collection,
            RouteValueDictionary routeDic)
        {
            foreach (string key in collection.Keys)
            {
                if (key != null && !routeDic.ContainsKey(key))
                    routeDic.Add(key, collection[key]);
            }
            return routeDic;
        }
        #region DisplayEnum
        public static MvcHtmlString EnumDisplay(this HtmlHelper htmlHelper, string name, string typeName, object enumValue, string lang = "")
        {
            string strDisplay = "";
            TagBuilder tagSpan = new TagBuilder("span");
            string color = "";
            try
            {
                Type type = GetTypeLang(name, lang);
                var enums = new List<SelectListItem>();
                foreach (var value in Enum.GetValues(type))
                {
                    //

                    DisplayAttribute[] attr = (DisplayAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DisplayAttribute), true);
                    if (attr == null || attr.Length == 0 || attr[0].Description != null)
                    {
                        color = attr != null && attr.Length > 0 && !string.IsNullOrWhiteSpace(attr[0].Description) ? attr[0].Description : "";
                    }

                    var item = new SelectListItem();
                    item.Value = value.ToString();
                    if ((byte)value == (byte)enumValue)
                    {
                        strDisplay = Enum.GetName(type, value).Replace("_", " ");
                        break;
                    }
                }
                tagSpan.InnerHtml = strDisplay.ToString().Replace("_", " ");
            }
            catch
            {
                tagSpan.InnerHtml = strDisplay.ToString().Replace("_", " ");
            }
            tagSpan.MergeAttribute("style", "color:" + color);
            return new MvcHtmlString(strDisplay.ToString());
        }
        #endregion
        #region DisplayEnumFor

        public static MvcHtmlString EnumDisplayFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string lang = "") where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);
            TProperty val = GetValue(htmlHelper, expression);
            string strDisplay = "";
            TagBuilder tagSpan = new TagBuilder("span");
            string color = "";
            try
            {
                Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
                IEnumerable<TProperty> values = Enum.GetValues(enumType).Cast<TProperty>();

                var enumLangs = new List<SelectListItem>();
                string valueEnum = "";

                foreach (var value in Enum.GetValues(enumTypeLang))
                {
                    DisplayAttribute[] attr = (DisplayAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DisplayAttribute), true);
                    if (attr == null || attr.Length == 0 || attr[0].Description != null)
                    {
                        color = attr != null && attr.Length > 0 && !string.IsNullOrWhiteSpace(attr[0].Description) ? attr[0].Description : "";
                    }
                    string str = Enum.GetName(enumType, value);

                    if (val.ToString() == str)
                    {
                        valueEnum = value.ToString();
                        strDisplay = Enum.GetName(enumTypeLang, value);
                        break;
                    }
                }

                tagSpan.InnerHtml = strDisplay.ToString().Replace("_", " ");
            }
            catch
            {
                tagSpan.InnerHtml = val.ToString().Replace("_", " ");
            }
            tagSpan.MergeAttribute("style", "color:" + color);
            return new MvcHtmlString(tagSpan.ToString());
        }

        #endregion

        public static Type GetTypeLang(string fullName, string lang)
        {
            if (lang == "en-US")
            {
                lang = "";
            }
            lang = lang.Replace("-", "_");
            lang = lang != "" ? "_" + lang : "";

            /*
             * 1 : Lấy giá trị enum tropng file defince với ngôn ngữ tương ứng
             * 2 : Nếu không có định nghĩa trong file defile tiếp tục tìm trong file controller
             * 3 : Nếu cả trong file define và controller không có giá trị. Lấy giá trị default trong file define
             * 4 : Nếu trong file define không có thì tìm trong file controller.
             */
            Type type = Type.GetType(fullName + lang + ",Utility");
            if (type == null)
                type = Type.GetType(fullName + lang + ",CaiThuocLa");
            if (type == null)
                type = Type.GetType(fullName + ",CaiThuocLa.Utility");
            if (type == null)
                type = Type.GetType(fullName + ",CaiThuocLa");
            return type;
        }

        #region DropDownListFor muti selected
        public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, SelectList selectListItem, List<string> listSelected = null, object htmlAttributes = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            TagBuilder select = new TagBuilder("select");
            //TagBuilder option = new TagBuilder("option");
            select.MergeAttribute("name", field);
            select.MergeAttribute("multiple", "");
            select.AddCssClass("span12 select-two");

            //class="select2-select-00 span12" multiple="" size="5"
            foreach (SelectListItem selecteditem in selectListItem)
            {
                TagBuilder option = new TagBuilder("option");
                option.InnerHtml = selecteditem.Text.Trim();
                option.MergeAttribute("Value", selecteditem.Value.Trim());
                if (listSelected != null && listSelected.Find(item => item.ToString().Trim().Equals(selecteditem.Value.ToString().Trim())) != null)
                {
                    option.MergeAttribute("Selected", "Selected");
                }
                select.InnerHtml += option.ToString();
            }
            return HtmlString(htmlHelper, select.ToString());
            // return System.Web.Mvc.Html.SelectExtensions.DropDownListFor(htmlHelper, expression, selectListItem, htmlAttributes);
        }

        public static MvcHtmlString DropDownListServerIpFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, SelectList selectListItem, List<string> listSelected = null, object htmlAttributes = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            TagBuilder select = new TagBuilder("select");
            //TagBuilder option = new TagBuilder("option");
            select.MergeAttribute("name", field);
            select.MergeAttribute("multiple", "");
            select.AddCssClass("span12 select-two");

            //class="select2-select-00 span12" multiple="" size="5"
            foreach (SelectListItem selecteditem in selectListItem)
            {
                TagBuilder option = new TagBuilder("option");
                option.InnerHtml = selecteditem.Text.Trim();
                option.MergeAttribute("Value", selecteditem.Text.Trim());
                if (listSelected != null && listSelected.Find(item => item.ToString().Trim().Equals(selecteditem.Text.ToString().Trim())) != null)
                {
                    option.MergeAttribute("Selected", "Selected");
                }
                select.InnerHtml += option.ToString();
            }
            return HtmlString(htmlHelper, select.ToString());
            // return System.Web.Mvc.Html.SelectExtensions.DropDownListFor(htmlHelper, expression, selectListItem, htmlAttributes);
        }

        public static MvcHtmlString DropDownListIP(this HtmlHelper html, string inputName, SelectList selectListItem, List<object> listSelected = null, object htmlAttributes = null)
        {

            TagBuilder select = new TagBuilder("select");
            //TagBuilder option = new TagBuilder("option");
            select.MergeAttribute("name", inputName);
            select.MergeAttribute("multiple", "");
            select.AddCssClass("span12 select-two");

            //class="select2-select-00 span12" multiple="" size="5"
			if (selectListItem != null)
			{
				foreach (SelectListItem selecteditem in selectListItem)
				{
					TagBuilder option = new TagBuilder("option");
					option.InnerHtml = selecteditem.Text.Trim();
					option.MergeAttribute("Value", selecteditem.Value);
					if (listSelected != null && listSelected.Find(item => item.ToString().ToLower().Trim().Equals(selecteditem.Value.ToString().Trim().ToLower())) != null)
					{
						option.MergeAttribute("Selected", "Selected");
					}
					select.InnerHtml += option.ToString();
				}
			}
            return new MvcHtmlString(select.ToString());
        }

        #endregion

        #region EnumDropDownList
        //<%= Html.EnumDropDownList("Person.FavoriteColor", Model.Person.FavoriteColor) %>
        public static MvcHtmlString EnumDropDownList(this HtmlHelper htmlHelper, string name, string typeName, object selected,
            IDictionary<string, string> enumExtension = null, string lang = "", object htmlAttributes = null)
        {
            var enums = new List<SelectListItem>();
            try
            {
                Type type = GetTypeLang(typeName, lang);
                Type typeEN = GetTypeLang(typeName, "");
                int index = 0;
                if (enumExtension != null)
                {
                    foreach (KeyValuePair<string, string> value in enumExtension)
                    {
                        var item = new SelectListItem();
                        item.Value = value.Value;
                        item.Text = value.Key.Replace("_", " ");
                        enums.Add(item);
                        index++;
                    }
                }

                foreach (var value in Enum.GetValues(type))
                {
                    var item = new SelectListItem();
                    item.Value = value.ToString();
                    item.Text = Enum.GetName(type, value).Replace("_", " ");

                    if (selected != null)
                        item.Selected = Convert.ToInt16(selected) == (byte)value;
                    enums.Add(item);

                }

                /*
                 * Chuyen key sang ngon ngu tieng anh 
                 */
                var items = new List<SelectListItem>();
                foreach (var value in Enum.GetValues(typeEN))
                {
                    var itemEN = new SelectListItem();
                    itemEN.Value = value.ToString();
                    itemEN.Text = Enum.GetName(typeEN, value).Replace("_", " ");
                    items.Add(itemEN);
                }
                if (lang != "")
                {
                    for (int i = index; i < items.Count(); i++)
                    {
                        enums[i].Value = items[i].Value;
                        //enums[i].Selected = items[i].Selected;
                    }
                }

            }
            catch
            {
                enums = new List<SelectListItem>();
                SelectListItem seItem = new SelectListItem();
                seItem.Text = "Error. Not loaded enum";
                seItem.Value = 0.ToString();
                enums.Add(seItem);
            }

            //string test = System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlHelper, name, enums.AsEnumerable(), htmlAttributes).ToHtmlString();
            return System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlHelper, name, enums.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString EnumDropDownList(this HtmlHelper htmlHelper, string inputName, string typeName, object selected,
            IDictionary<string, string> enumExtension = null, string lang = "", IDictionary<string, object> selectHtmlAttributes = null)
        {
            Type type = GetTypeLang(typeName, lang);
            Type typeEN = GetTypeLang(typeName, "");

            var enums = new List<SelectListItem>();
            foreach (var value in Enum.GetValues(type))
            {
                var item = new SelectListItem();
                item.Value = value.ToString();
                item.Text = Enum.GetName(type, value).Replace("_", " ");

                if (selected != null)
                    item.Selected = Convert.ToInt16(selected) == (byte)value;
                enums.Add(item);

            }

            var enumsEN = new List<SelectListItem>();
            foreach (var value in Enum.GetValues(typeEN))
            {
                var item = new SelectListItem();
                item.Value = value.ToString();
                item.Text = Enum.GetName(type, value).Replace("_", " ");

                if (selected != null)
                    item.Selected = Convert.ToInt16(selected) == (byte)value;
                enumsEN.Add(item);

            }

            TagBuilder selectTag = new TagBuilder("select");
            TagBuilder optionTag;
            
            selectTag.Attributes.Add("name", inputName);
			
			selectTag.MergeAttributes(selectHtmlAttributes);
			string clss = " span12";
			if(selectTag.Attributes.ContainsKey("class"))
			{
				clss = selectTag.Attributes["class"] + " span12";
				selectTag.Attributes.Remove("class");
			}
			selectTag.Attributes.Add("class", clss);
            string strOption = "";
            for (int i = 0; i < enumsEN.Count(); i++)
            {
                optionTag = new TagBuilder("option");
                optionTag.Attributes.Add("value", enumsEN[i].Value);
                if (enumsEN[i].Selected)
                    optionTag.Attributes.Add("selected", "selected");
                optionTag.InnerHtml = enumsEN[i].Text;
                strOption += optionTag.ToString();
            }
            selectTag.InnerHtml = strOption;

            return new MvcHtmlString(selectTag.ToString());
        }

        //Không coment về hàm này. Đã thống nhất rồi.
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IDictionary<string, string> enumExtension = null, string lang = "", object htmlAttributes = null, IList<string> exceptedEnums = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
                IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();
                // lấy giá trị của enum ngôn ngữ tương ứng
                var enumLangs = new List<SelectListItem>();
                foreach (int value in Enum.GetValues(enumTypeLang))
                {
                    var item = new SelectListItem();
                    item.Value = value.ToString();
                    item.Text = Enum.GetName(enumTypeLang, value).Replace("_", " ");
                    enumLangs.Add(item);
                }

                items = values.Select(value => new SelectListItem
                {
                    Text = value.ToString().Replace("_", " "),
                    Value = value.ToString(),
                    //Nếu  để kiểu byte không nhân được selected
                    //Value = ((byte)Enum.Parse(enumType, value.ToString())).ToString(),
                    Selected = value.Equals(metadata.Model)
                }).ToList();

                // thêm extensions
                if (enumExtension != null)
                {
                    foreach (KeyValuePair<string, string> value in enumExtension)
                    {
                        var item = new SelectListItem();
                        item.Value = value.Value;
                        item.Text = value.Key;
                        items.Insert(0, item);
                        enumLangs.Insert(0, item);
                    }
                }
                // đổi tên hiển thị thành tên của ngôn ngữ tương ứng
                if (lang != "")
                {
                    for (int i = 0; i < items.Count(); i++)
                    {
                        items[i].Text = enumLangs[i].Text;
                    }
                }

                if (metadata.IsNullableValueType)
                {
                    items = SingleEmptyItem.Concat(items).ToList();
                }

                //begin remove exceptedList
                //add date: 2013-05-14 by phong.nd
                if (exceptedEnums != null && exceptedEnums.Count > 0)
                {
                    foreach (var excepted in exceptedEnums)
                    {
                        var remove = items.Where(item => item.Value == excepted).FirstOrDefault();
                        if (remove != null)
                            items.Remove(remove);
                    }
                }
                
                //end remove excepted
                //var deleteObj = items.Where(item => Convert.ToInt32(item.Value) == 100).FirstOrDefault();
                //if (deleteObj != null)
                //    items.Remove(deleteObj);
            }
            catch
            {
                items = new List<SelectListItem>();
                SelectListItem seItem = new SelectListItem();
                seItem.Text = "Error. Not loaded enum";
                seItem.Value = 0.ToString();
                items.Add(seItem);
            }
            return System.Web.Mvc.Html.SelectExtensions.DropDownListFor(htmlHelper, expression, items, htmlAttributes);
        }

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }


        #endregion EnumDropDownList

        #region EnumRadioButtonList
        /*
         *  Load from defince enum
        */
        public static MvcHtmlString EnumRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string lang = "", IDictionary<string, object> labelHtmlAttributes = null, IDictionary<string, object> inputHtmlAttributes = null, List<object> viewList = null, IList<object> exceptedEnums = null, int selectedVal = 0) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);

            Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputName = fieldname;
            //Get keys enum
            TProperty val = GetValue(htmlHelper, expression);
            //foreach enum default
            int valueDefault = 0;
            foreach (var item in Enum.GetValues(enumType))
            {
                if (val.Equals(item))
                {
                    valueDefault = Convert.ToInt32(item);
                    break;
                }
            }
            //get value
            string str = "";
            int index = 0;
            int k = 0;
            foreach (var item in Enum.GetValues(enumTypeLang))
            {
                string name = item.ToString();
                var itemval = item;
                bool selected = false;

                //begin remove exceptedList
                //add date: 2013-05-17 by phong.nd
                if (exceptedEnums != null && exceptedEnums.Count > 0)
                {
                    var excepted = exceptedEnums.Where(ex => Convert.ToInt32(ex.ToString()) == Convert.ToInt32(itemval)).FirstOrDefault();
                    if (excepted != null)
                        continue;
                }
                //end remove excepted

                if (valueDefault == Convert.ToInt32(item))
                {
                    selected = true;
                }
                if (index == 0)
                {
                    selected = true;
                    index++;
                }
                //Tri edit
                if (selectedVal == Convert.ToInt32(item))
                {
                    selected = true;
                }
                if (viewList == null)
                {
                    //Tao label long radio button
                    var labelBuilder = new TagBuilder("label");
                    //labelBuilder.MergeAttribute("class", "radio");
                    labelBuilder.MergeAttributes(labelHtmlAttributes);

                    labelBuilder.InnerHtml = RadioButton(htmlHelper, inputName, new SelectListItem { Text = name, Value = Convert.ToInt32(itemval).ToString(), Selected = selected }, inputHtmlAttributes);
                    labelBuilder.InnerHtml += name.Replace("_", " ");
                    str += labelBuilder.ToString();
                }
                else
                {

                    if (viewList != null && viewList.Find(ob => (long)ob == ((long)Convert.ToInt32(item))) != null)
                    {
                        if (k == 0)
                        {
                            selected = true;
                            k++;
                        }
                        //Tao label long radio button
                        var labelBuilder = new TagBuilder("label");
                        //labelBuilder.MergeAttribute("class", "radio");
                        labelBuilder.MergeAttributes(labelHtmlAttributes);

                        labelBuilder.InnerHtml = RadioButton(htmlHelper, inputName, new SelectListItem { Text = name, Value = Convert.ToInt32(itemval).ToString(), Selected = selected }, inputHtmlAttributes);
                        labelBuilder.InnerHtml += name.Replace("_", " ");
                        str += labelBuilder.ToString();
                    }
                }

            }

            return new MvcHtmlString(str);
        }

        public static MvcHtmlString EnumRadioButtonForNoneSelected<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string lang = "", IDictionary<string, object> labelHtmlAttributes = null, IDictionary<string, object> inputHtmlAttributes = null, List<object> viewList = null, IList<object> exceptedEnums = null, int selectedVal = 0) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);

            Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputName = fieldname;
            //Get keys enum
            TProperty val = GetValue(htmlHelper, expression);
            //foreach enum default
            int valueDefault = 0;
            foreach (var item in Enum.GetValues(enumType))
            {
                if (val.Equals(item))
                {
                    valueDefault = Convert.ToInt32(item);
                    break;
                }
            }
            //get value
            string str = "";
            int index = 0;
            int k = 0;
            foreach (var item in Enum.GetValues(enumTypeLang))
            {
                string name = item.ToString();
                var itemval = item;
                bool selected = false;

                //begin remove exceptedList
                //add date: 2013-05-17 by phong.nd
                if (exceptedEnums != null && exceptedEnums.Count > 0)
                {
                    var excepted = exceptedEnums.Where(ex => Convert.ToInt32(ex.ToString()) == Convert.ToInt32(itemval)).FirstOrDefault();
                    if (excepted != null)
                        continue;
                }
                //end remove excepted

                if (valueDefault == Convert.ToInt32(item))
                {
                    selected = true;
                }
                if (index == 0)
                {
                    selected = true;
                    index++;
                }
                //Tri edit
                if (selectedVal == Convert.ToInt32(item))
                {
                    selected = true;
                }
                if (viewList == null)
                {
                    //Tao label long radio button
                    var labelBuilder = new TagBuilder("label");
                    //labelBuilder.MergeAttribute("class", "radio");
                    labelBuilder.MergeAttributes(labelHtmlAttributes);

                    labelBuilder.InnerHtml = RadioButton(htmlHelper, inputName, new SelectListItem { Text = name, Value = Convert.ToInt32(itemval).ToString() }, inputHtmlAttributes);
                    labelBuilder.InnerHtml += name.Replace("_", " ");
                    str += labelBuilder.ToString();
                }
            }
            return new MvcHtmlString(str);
        }

        /*
         *  Hàm tao radio từ dữ liệu được load từ define
         */

        public static MvcHtmlString EnumRadioButton(this HtmlHelper htmlHelper, string name, string typeName, string lang = "", IDictionary<string, object> labelHtmlAttributes = null, IDictionary<string, object> inputHtmlAttributes = null, List<object> viewList = null)
        {
            Type type = GetTypeLang(typeName, lang);
            var enums = new List<SelectListItem>();
            string str = "";
            int index = 0;
            foreach (byte value in Enum.GetValues(type))
            {
                bool selected = false;
                if (index == 0)
                {
                    index++;
                    selected = true;
                }
                var item = new SelectListItem();
                item.Value = value.ToString();
                item.Text = Enum.GetName(type, value).Replace("_", " ");
                //Tao label long radio button
                var labelBuilder = new TagBuilder("label");
                string classCss = "";
                try
                {
                    classCss = labelHtmlAttributes["class"].ToString();
                }
                catch
                {
                    classCss = "";
                }
                labelBuilder.AddCssClass(classCss);
                labelBuilder.MergeAttributes(labelHtmlAttributes);

                labelBuilder.InnerHtml = RadioButton(htmlHelper, name, new SelectListItem { Text = item.Text, Value = item.Value.ToString(), Selected = selected }, inputHtmlAttributes);
                labelBuilder.InnerHtml += item.Text.Replace("_", " ");
                str += labelBuilder.ToString();
            }
            return new MvcHtmlString(str);
        }

        
        /*
         * Hàm tao radio từ dữ liệu được load từ database
         */
        public static MvcHtmlString FiboRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, List<SelectListItem> listItem, IDictionary<string, object> htmlAttributes = null) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);

            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputName = fieldname;
            TProperty val = GetValue(htmlHelper, expression);

            string str = "";
            foreach (SelectListItem item in listItem)
            {
                //Tao label long radio button
                var labelBuilder = new TagBuilder("label");
                labelBuilder.MergeAttribute("class", "radio");
                labelBuilder.MergeAttributes(htmlAttributes);
                if (val.ToString().ToUpper() == item.Value.ToUpper())
                    item.Selected = true;
                labelBuilder.InnerHtml = RadioButton(htmlHelper, inputName, item, htmlAttributes);
                labelBuilder.InnerHtml += item.Text;
                str += labelBuilder.ToString();
            }
            return new MvcHtmlString(str);
        }

        public static string RadioButton(this HtmlHelper htmlHelper, string name, SelectListItem listItem,
                             IDictionary<string, object> htmlAttributes)
        {
            var inputIdSb = new StringBuilder();
            inputIdSb.Append(name)
                .Append("_")
                .Append(listItem.Value);

            var sb = new StringBuilder();

            var builder = new TagBuilder("input");
            if (listItem.Selected) builder.MergeAttribute("checked", "checked");
            builder.MergeAttribute("type", "radio");
            builder.MergeAttribute("value", listItem.Value);
            builder.MergeAttribute("id", inputIdSb.ToString());
            builder.MergeAttribute("name", name);
            builder.MergeAttribute("class", "uniform");
            string classCss = "";
            try
            {
                classCss = htmlAttributes["class"].ToString();
            }
            catch
            {
                classCss = "";
            }
            builder.AddCssClass(classCss);
            builder.MergeAttributes(htmlAttributes);
            sb.Append(builder.ToString(TagRenderMode.SelfClosing));

            return sb.ToString();
        }

        public static string RadioButtonLabel(string inputId, string displayText,
                                     IDictionary<string, object> htmlAttributes)
        {
            var labelBuilder = new TagBuilder("label");
            labelBuilder.MergeAttribute("for", inputId);
            labelBuilder.MergeAttributes(htmlAttributes);
            labelBuilder.InnerHtml = displayText;

            return labelBuilder.ToString(TagRenderMode.Normal);
        }


        public static TProperty GetValue<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) where TModel : class
        {
            TModel model = htmlHelper.ViewData.Model;
            if (model == null)
            {
                return default(TProperty);
            }
            Func<TModel, TProperty> func = expression.Compile();
            return func(model);
        }

        #endregion

        #region EnumCheckboxListFor
        public static MvcHtmlString EnumCheckBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, byte selectedSingle = 0, string lang = "", IDictionary<string, object> labelHtmlAttributes = null, IDictionary<string, object> inputHtmlAttributes = null) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);

            Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputName = fieldname;
            //TProperty val = GetValue(htmlHelper, expression);

            string str = "";
            foreach (var item in Enum.GetValues(enumTypeLang))
            {
                if ((byte)item == selectedSingle)
                {
                    //Tao label long radio button
                    var labelBuilder = new TagBuilder("label");
                    //labelBuilder.MergeAttribute("class", "checkbox");
                    labelBuilder.MergeAttributes(labelHtmlAttributes);
                    //labelBuilder.MergeAttributes(htmlAttributes);
                    labelBuilder.InnerHtml = CheckBox(htmlHelper, inputName, new SelectListItem { Text = item.ToString(), Value = Convert.ToInt32(item).ToString(), Selected = true }, inputHtmlAttributes);
                    labelBuilder.InnerHtml += item.ToString().Replace("_", " ");
                    str += labelBuilder.ToString();
                }
            }
            return new MvcHtmlString(str);
        }

        public static MvcHtmlString EnumCheckBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, List<int> selectedValues = null, string lang = "", IDictionary<string, object> labelHtmlAttributes = null, IDictionary<string, object> inputHtmlAttributes = null, bool isPrivate = false) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);

            Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputName = fieldname;
            //TProperty val = GetValue(htmlHelper, expression);
            string str = "";

            foreach (var item in Enum.GetValues(enumTypeLang))
            {
                var itemval = item;
                bool val = false;
                if (selectedValues != null)
                {
                    foreach (int selected in selectedValues)
                    {
                        if (selected == (byte)item)
                        {
                            val = true;
                        }
                    }
                }

                //Tao label long radio button
                var labelBuilder = new TagBuilder("label");
                //labelBuilder.MergeAttribute("class", "checkbox");
                labelBuilder.MergeAttributes(labelHtmlAttributes);

                //labelBuilder.MergeAttributes(htmlAttributes);
                labelBuilder.InnerHtml = CheckBox(htmlHelper, inputName, new SelectListItem { Text = item.ToString(), Value = Convert.ToInt32(itemval).ToString(), Selected = val }, inputHtmlAttributes);
                labelBuilder.InnerHtml += item.ToString().Replace("_", " ");
                str += labelBuilder.ToString();
            }

            return new MvcHtmlString(str);
        }

        public static string CheckBox(this HtmlHelper htmlHelper, string name, SelectListItem listItem,
                             IDictionary<string, object> inputHtmlAttributes)
        {
            var inputIdSb = new StringBuilder();
            inputIdSb.Append(name)
                .Append("_")
                .Append(listItem.Value);

            var sb = new StringBuilder();

            var builder = new TagBuilder("input");
            if (listItem.Selected) builder.MergeAttribute("checked", "checked");
            builder.MergeAttribute("type", "checkbox");
            builder.MergeAttribute("value", listItem.Value);
            builder.MergeAttribute("id", inputIdSb.ToString());
            builder.MergeAttribute("name", name);
            builder.MergeAttribute("class", "uniform");
            builder.MergeAttributes(inputHtmlAttributes);
            sb.Append(builder.ToString(TagRenderMode.SelfClosing));
            //sb.Append(RadioButtonLabel(inputIdSb.ToString(), listItem.Text, htmlAttributes));

            return sb.ToString();
        }


        #region CheckBox have span folow

        public static MvcHtmlString EnumCheckBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string lang = "", List<int> selectedValues = null, IDictionary<string, object> labelHtmlAttributes = null, IDictionary<string, object> inputHtmlAttributes = null, IDictionary<string, object> spanHtmlAttributes = null, bool isPermission = false) where TModel : class
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Type enumType = GetNonNullableModelType(metadata);

            Type enumTypeLang = GetTypeLang(enumType.FullName, lang);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputName = fieldname;
            //TProperty val = GetValue(htmlHelper, expression);
            string str = "";

            foreach (var item in Enum.GetValues(enumTypeLang))
            {
                var itemval = item;
                bool val = false;
                if (selectedValues != null)
                {
                    foreach (int selected in selectedValues)
                    {
                        if (selected == (byte)item)
                        {
                            val = true;
                        }
                    }
                }
                //Tao label long radio button
                var labelBuilder = new TagBuilder("label");
                //labelBuilder.MergeAttribute("class", "checkbox");
                labelBuilder.MergeAttributes(labelHtmlAttributes);
                labelBuilder.MergeAttribute("class", "text-bissenessfield-" + Convert.ToInt32(itemval));
                labelBuilder.MergeAttribute("style", "width:100%;float:left;margin-bottom:0px");

                //labelBuilder.MergeAttributes(htmlAttributes);
                var spanbuilder = new TagBuilder("span");
                spanbuilder.AddCssClass(spanHtmlAttributes["class"].ToString() + "-" + Convert.ToInt32(itemval));
                if (isPermission)
                {
                    spanbuilder.InnerHtml = "<a href = '#select-sales' class = 'select-sale' style = 'color:green;margin-left:20px;' data-target = '#anim-modal-show-popup-member-list' data-toggle = 'modal' classId = '" + Convert.ToInt32(itemval) + "' > Chọn sales </a>";
                }
                else
                {
                    spanbuilder.InnerHtml = "";
                }
                spanbuilder.MergeAttribute("style", "float:left;");
                labelBuilder.InnerHtml = CheckBox(htmlHelper, inputName, new SelectListItem { Text = item.ToString(), Value = Convert.ToInt32(itemval).ToString(), Selected = val }, inputHtmlAttributes, spanHtmlAttributes);
                labelBuilder.InnerHtml += item.ToString().Replace("_", " ");
                str += labelBuilder.ToString();
                str += spanbuilder.ToString();
            }
            return new MvcHtmlString(str);
        }

        public static string CheckBox(this HtmlHelper htmlHelper, string name, SelectListItem listItem,
                             IDictionary<string, object> inputHtmlAttributes, IDictionary<string, object> spanHtmlAttributes)
        {
            var inputIdSb = new StringBuilder();
            inputIdSb.Append(name)
                .Append("_")
                .Append(listItem.Value);

            var sb = new StringBuilder();

            var builder = new TagBuilder("input");
            if (listItem.Selected) builder.MergeAttribute("checked", "checked");
            builder.MergeAttribute("type", "checkbox");
            builder.MergeAttribute("value", listItem.Value);
            builder.MergeAttribute("id", inputIdSb.ToString());
            builder.MergeAttribute("name", name);
            builder.MergeAttribute("class", "uniform check-box-businessfield-" + listItem.Value);
            builder.AddCssClass((inputHtmlAttributes["class"] != null) ? inputHtmlAttributes["class"].ToString() : "");
            builder.MergeAttributes(inputHtmlAttributes);

            sb.Append(builder.ToString(TagRenderMode.SelfClosing));

            //sb.Append(RadioButtonLabel(inputIdSb.ToString(), listItem.Text, htmlAttributes));
            return sb.ToString();
        }

        #endregion


        #endregion

        #region InputFileHTML

        public static MvcHtmlString InputFileFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            String field = ExpressionHelper.GetExpressionText(expression);
            String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);
            var inputTag = new TagBuilder("input");
            inputTag.MergeAttribute("type", "file");

            inputTag.MergeAttribute("name", fieldname);
            inputTag.MergeAttribute("data-provide", "fileinput");
            inputTag.MergeAttributes(htmlAttributes);


            string str = inputTag.ToString();

            return new MvcHtmlString(str);
        }

        #endregion

        #region Truncate

        public static MvcHtmlString Truncate(this HtmlHelper htmlHelper, string sourceText, string ellipsis = "...", int min = 0, int max = 50)
        {
            //If text is shorter than preview length
            if (sourceText.Length <= max)
            {
                return MvcHtmlString.Create(sourceText); //@RETURN break out early if too short
            }

            //Grab the char at the last position allowed
            char cutOffChar = sourceText[max];

            int lastPosition = max;

            //While the last char isn't a space, cut back until we hit a space or minimum
            while (cutOffChar != ' ' && lastPosition > min)
            {
                lastPosition--;
                cutOffChar = sourceText[lastPosition];
            }

            //Crop text and add some dots
            string outText = sourceText.Substring(0, lastPosition);
            outText += " " + ellipsis;

            return MvcHtmlString.Create(outText);
        }

        #endregion End Truncate

        //public static MvcHtmlString ActionLinkFibo(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues, string htmlAttributes = "", string linkDisplay = "")
        //{
        //    //TagBuilder aTag = new TagBuilder("a"); aTag.Attributes.Add("href", url + "&currentpage=" + pager.PageNum); aTag.Attributes.Add("title", pager.Title);
        //    //return MvcHtmlString.Create(tag.ToString());
        //    string encurl = QueryStringModule.Encrypt(string.Format("{0}/{1}/?{2}", controllerName, actionName, routeValues.ToString().Replace("{", "").Replace("}", "").Replace(",", "&").Replace(" ", "")));
        //    MvcHtmlString url = new MvcHtmlString(string.Format("<a href='/{0}'{2}>{3}{1}</a>", encurl, linkText, htmlAttributes, linkDisplay));
        //    return url;
        //}
        //public static MvcHtmlString ActionIconFibo(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues, string htmlAttributes = "", string linkDisplay = "")
        //{
        //    //TagBuilder aTag = new TagBuilder("a"); aTag.Attributes.Add("href", url + "&currentpage=" + pager.PageNum); aTag.Attributes.Add("title", pager.Title);
        //    //return MvcHtmlString.Create(tag.ToString());
        //    string encurl = QueryStringModule.Encrypt(string.Format("{0}/{1}/?{2}", controllerName, actionName, routeValues.ToString().Replace("{", "").Replace("}", "").Replace(",", "&").Replace(" ", "")));
        //    MvcHtmlString url = new MvcHtmlString(string.Format("<a href=\"/{0}\" title='{1}' {2}>{3}</a>", encurl, linkText, htmlAttributes, linkDisplay));
        //    return url;
        //}

        public static MvcHtmlString ActionSubmitFibo(this HtmlHelper htmlHelper, string linkText = "#", string eVentJavaScript = "", string htmlAttributes = "", string linkDisplay = "")
        {
            //TagBuilder aTag = new TagBuilder("a"); aTag.Attributes.Add("href", url + "&currentpage=" + pager.PageNum); aTag.Attributes.Add("title", pager.Title);
            //return MvcHtmlString.Create(tag.ToString());
            //string encurl = QueryStringModule.Encrypt(string.Format("{0}/{1}/?{2}", controllerName, actionName, routeValues.ToString().Replace("{", "").Replace("}", "").Replace(",", "&").Replace(" ", "")));
            MvcHtmlString url = new MvcHtmlString(string.Format("<a href='#{0}' {1} {2} >{3}</a>", linkText, eVentJavaScript, htmlAttributes, linkDisplay));
            return url;
        }

        //public static MvcHtmlString ActionSortLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string fieldSort = null, string htmlAttributes = "", string iCon = "")
        //{
        //    HttpRequestBase request = htmlHelper.ViewContext.HttpContext.Request;
        //    NameValueCollection urlParameters = HttpUtility.ParseQueryString(request.Url.Query);
        //    string field = (urlParameters["fieldSort"] != null) ? urlParameters["fieldSort"] : "";
        //    urlParameters["fieldSort"] = fieldSort;
        //    string typeSort = (urlParameters["typeSort"] != null) ? urlParameters["typeSort"] : "desc";
        //    string cssClass = "";
        //    if (field == fieldSort)
        //    {
        //        if (typeSort != null && typeSort == "desc")
        //        {
        //            cssClass = "sorting_asc";
        //            typeSort = "asc";
        //        }
        //        else
        //        {
        //            if (typeSort != null && typeSort == "asc")
        //            {
        //                cssClass = "sorting_desc";
        //                typeSort = "desc";
        //            }
        //        }
        //    }
        //    urlParameters["typeSort"] = typeSort;
        //    //string encurl = QueryStringModule.Encrypt(string.Format("{0}/{1}/?{3}", controllerName, actionName, "", urlParameters.ToString()));

        //    //MvcHtmlString url = new MvcHtmlString(string.Format("<a href='/{0}' class='{3}' >{2}{1}</a>", encurl, linkText, iCon, cssClass));

        //    return url;
        //}

		public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
		{
			//ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
			//DateTime dt = new DateTime();
			//string value = "";
			//if (metadata.Model != null)
			//{
			//	dt = DateTime.Parse(metadata.Model.ToString());
			//	value = dt.Year + "-" + dt.Month + "-" + dt.Day;
			//}
			//String field = ExpressionHelper.GetExpressionText(expression);
			//String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);

			//string str = GenerateHtmlPicker(fieldname, value, "span12 date-picker", htmlAttributes);
			//return new MvcHtmlString(str);

			Dictionary<string, object> attributeHtml = new Dictionary<string, object>();
			attributeHtml.Add("data-val", "false");
			attributeHtml.Add("readonly", "readonly");
			attributeHtml.Add("class", "span12 date-picker");

			return System.Web.Mvc.Html.InputExtensions.TextBoxFor(htmlHelper, expression, "{0:dd-MM-yyyy}", attributeHtml);
		}

		public static MvcHtmlString BirthdayPickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
		{
			//ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
			//DateTime dt = new DateTime();
			//string value = "";
			//if (metadata.Model != null)
			//{
			//	dt = DateTime.Parse(metadata.Model.ToString());
			//	value = dt.Year + "-" + dt.Month + "-" + dt.Day;
			//}
			//String field = ExpressionHelper.GetExpressionText(expression);
			//String fieldname = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(field);

			//string str = GenerateHtmlPicker(fieldname, value, "span12 birthday-picker", htmlAttributes);
			//return new MvcHtmlString(str);

            Dictionary<string, object> attributeHtml = new Dictionary<string, object>();
            attributeHtml.Add("data-val", "false");
            attributeHtml.Add("readonly", "readonly");
            attributeHtml.Add("class", "span12 birthday-picker");

			return System.Web.Mvc.Html.InputExtensions.TextBoxFor(htmlHelper, expression, "{0:dd-MM-yyyy}", attributeHtml);
          
		}

		public static string GenerateHtmlPicker(string fieldName, string value, string classInput, IDictionary<string, object> htmlAttributes = null)
		{
		
			var inputTag = new TagBuilder("input");
			inputTag.MergeAttribute("type", "text");
			inputTag.MergeAttribute("value", value);
			inputTag.MergeAttribute("name", fieldName);
			inputTag.MergeAttribute("id", fieldName);
			inputTag.MergeAttribute("data-val", "false");
			inputTag.MergeAttribute("class", classInput);
			inputTag.MergeAttributes(htmlAttributes);
			inputTag.MergeAttribute("readonly", "readonly");

			return inputTag.ToString();
		}
    }
}