﻿using DAL.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Katri.Models
{
    public class UserModel: ObjectBase
    {
        public string UserName { set; get; }

        public UserModel()
        {
            UserName = "";
        }
    }
}