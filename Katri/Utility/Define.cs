﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public enum CardStatus : int
    {
        Active = 1,
        DeActive = 2,
        Delete = 100
    }
}
