﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class ObjectPage<T>
    {
        public int Record { set; get; }
        public int CurrentPage { set; get; }
        public long TotalRecord { set; get; }
        public IQueryable<T> Data { set; get; }

        public ObjectPage()
        {
            Record = 10;
            CurrentPage = 1;
            TotalRecord = 0;
        }

    }
}
