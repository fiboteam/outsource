﻿using Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Xml.Linq;

namespace SMSGateway.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Received(string message = "", string phone = "", string service = "", string port = "", string main = "", string sub = "", string guid = "")
        {
            try
            {
                int serviceNo = -1;
                int.TryParse(WebConfigurationManager.AppSettings["ServiceNo"], out serviceNo);

                //Kiểm tra IP
                string ip = Request.ServerVariables["REMOTE_ADDR"];
                if (!Tool.CheckIpReceived(ip))
                {
                    return Content(CreateXmlResponse("", "Lỗi IP kết nối không hợp lệ"), "text/xml");
                }

                if (String.IsNullOrEmpty(phone))
                {
                    return Content(CreateXmlResponse("", "Số điện thoại rỗng"), "text/xml");
                }

                if (!Tool.CheckPhoneNumber(phone))
                {
                    return Content(CreateXmlResponse("", "Số điện không hợp lệ. SĐT: " + phone), "text/xml");
                }

                //Chuyển số điện thoại thành số 0 đầu
                phone = phone.Replace(" ", "");
                if (phone.StartsWith("84"))
                    phone = phone.Remove(0, 2).Insert(0, "0");
                else if (phone.StartsWith("+84"))
                    phone = phone.Remove(0, 3).Insert(0, "0");

                Dictionary<string, string> dicMessageResult = Tool.GetDicFromFile("~/App_Data/MessageResult.txt");
                if (dicMessageResult == null || dicMessageResult.Count <= 0)
                {
                    return Content(CreateXmlResponse("", "Lỗi không tìm thấy tin nhắn trả về"), "text/xml");
                }

                //Kiểm tra nội dung
                string[] parms = message.ToLower().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (
                    String.IsNullOrEmpty(message)
                    || parms.Length != 3 
                    || !parms[0].Equals(Tool.GetParamAppConfig("Keyword").ToLower())
                    )
                {
                    return Content(CreateXmlResponse(phone, dicMessageResult["MessageErrorSyntax"], serviceNo), "text/xml");
                }

                string charStartEmployee = Tool.GetParamAppConfig("CharStartEmployee").ToLower();
                string charStartVote = Tool.GetParamAppConfig("CharStartVote").ToLower();

                //Kiểm tra độ dài mã nhân viên
                if (parms[1].Length != int.Parse(Tool.GetParamAppConfig("LengthtEmployee")) || !parms[1].StartsWith(charStartEmployee))
                {
                    return Content(CreateXmlResponse(phone, dicMessageResult["ErrorEmployee"], serviceNo), "text/xml");
                }
                else
                {
                    string strNumber = parms[1].Remove(0, charStartEmployee.Length);
                    if(!Tool.IsNumber(strNumber))
                        return Content(CreateXmlResponse(phone, dicMessageResult["ErrorEmployee"], serviceNo), "text/xml");
                }

                //Kiểm tra độ dài mã bình chọn
                if (parms[2].Length != int.Parse(Tool.GetParamAppConfig("LengthVote")) || !parms[2].StartsWith(charStartVote))
                {
                    return Content(CreateXmlResponse(phone, dicMessageResult["ErrorVote"], serviceNo), "text/xml");
                }
                else
                {
                    string strNumber = parms[2].Remove(0, charStartVote.Length);
                    if (!Tool.IsNumber(strNumber))
                        return Content(CreateXmlResponse(phone, dicMessageResult["ErrorVote"], serviceNo), "text/xml");
                }

                //Thành công
                string resultXml = CreateXmlResponse(phone, dicMessageResult["MessageSuccess"], serviceNo);

                return Content(resultXml, "text/xml");
            }
            catch (Exception ex)
            {
                return Content(CreateXmlResponse("", "Đã có lỗi xảy ra trên hệ thông. Message: " + ex.Message), "text/xml");
            }
        }

        public static string CreateXmlResponse(string phone, string message, int serviceNo = -1, int contentType = 0)
        {
            XElement xml = new XElement("ClientResponse");

            List<string> listMessage = new List<string>();
            int countMessage = (int)Math.Ceiling((double)message.Length / 160);
            for (int i = 1; i <= countMessage; i++)
            {
                try
                {
                    listMessage.Add(message.Substring((i - 1) * 160, 160));
                }
                catch
                {
                    listMessage.Add(message.Substring((i - 1) * 160));
                }
            }

            foreach (var i in listMessage)
            {
                xml.Add(
                    new XElement("Message",
                        new XElement("PhoneNumber", phone),
                        new XElement("Message", i),
                        new XElement("SMSID", Guid.NewGuid().ToString()),
                        new XElement("ServiceNo", serviceNo),
                        new XElement("ContentType", contentType)
                    )
                );
            }

            return xml.ToString();
        }
    }
}
