﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Xml.Linq;
using System.IO;
using System.Web.Configuration;
using System.Configuration;
using System.Web;

namespace Utility
{
    public class Tool
    {
        public static bool CheckIpReceived(string ip)
        {
            string[] listIpAccess = WebConfigurationManager.AppSettings["IpCheck"].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            if (listIpAccess.Contains(ip)) return true;
            return false;
        }

        public static bool ChangeParamAppConfig(string key, string value, string parent = "appSettings")
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("/");
                config.AppSettings.Settings[key].Value = value;
                config.Save(ConfigurationSaveMode.Modified);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckPhoneNumber(string phoneNumber)
        {
            try
            {
                phoneNumber = phoneNumber.Replace(" ", "");
                if (phoneNumber.StartsWith("84"))
                    phoneNumber = phoneNumber.Remove(0, 2).Insert(0, "0");
                else if (phoneNumber.StartsWith("+84"))
                    phoneNumber = phoneNumber.Remove(0, 3).Insert(0, "0");

                if (phoneNumber.Length < 10)
                    return false;

                double phoneTmp = -1;
                if (!double.TryParse(phoneNumber, out phoneTmp))
                    return false;

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static string GetParamAppConfig(string key)
        { 
            try
            {
                return WebConfigurationManager.AppSettings[key];
            }
            catch(Exception)
            {
                return String.Empty;
            }
        }

        public static List<string> GetListByReadLine(string filePath)
        {
            filePath = HttpContext.Current.Server.MapPath(filePath);
            List<string> list = new List<string>();

            try
            {
                using (TextReader reader = File.OpenText(filePath))
                {
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        line = line.Replace("\\n", "\n");
                        line = line.Replace("\\r", "\r");
                        list.Add(line);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return list;
        }

        public static bool WriteTextByWriteLine(List<string> listText, string filePath, bool deleteOld = true)
        {
            try
            {
                filePath = HttpContext.Current.Server.MapPath(filePath);
                string source = String.Empty;

                if (!deleteOld)
                {
                    try { source = File.ReadAllText(filePath); }
                    catch { }

                    if (!String.IsNullOrEmpty(source))
                    {
                        StringReader reader = new StringReader(source);
                        string line = String.Empty;
                        while ((line = reader.ReadLine()) != null)
                        {
                            listText.Add(line);
                        }
                    }
                }

                StringBuilder sWrite = new StringBuilder();
                foreach (var i in listText)
                {
                    sWrite.AppendLine(i);
                }

                File.WriteAllText(filePath, sWrite.ToString());
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Dictionary<string, string> GetDicFromFile(string path)
        {
            try
            {
                List<string> list = GetListByReadLine(path);
                Dictionary<string, string> dic = new Dictionary<string, string>();

                foreach(var i in list)
                {
                    string[] parms = i.Split(new string[] { "|||" }, StringSplitOptions.None);
                    dic.Add(parms[0], parms[1]);
                }

                return dic;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static bool IsNumber(string stringNumber, bool isMinus = false)
        {
            if (String.IsNullOrEmpty(stringNumber))
                return false;

            foreach (char i in stringNumber)
            {
                long numberTmp = 0;
                if (!long.TryParse(i.ToString(), out numberTmp))
                    return false;
            }
            return true;
        }
    }
}
